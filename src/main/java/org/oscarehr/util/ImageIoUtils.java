/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.log4j.Logger;

public class ImageIoUtils
{
	private static Logger logger = MiscUtils.getLogger();

	public static final float GENERAL_GOOD_COMPRESSION = .92f;

	static
	{
		ImageIO.setUseCache(false);
	}

	public static BufferedImage cropSquareThenScaleSmallerProportionally(byte[] inputImage, int maxWidth, int maxHeight) throws IOException
	{
		ByteArrayInputStream bais = new ByteArrayInputStream(inputImage);
		BufferedImage image = ImageIO.read(bais);

		int imageWidth = image.getWidth();
		int imageHeight = image.getHeight();
		int desiredDimension = Math.min(imageWidth, imageHeight);

		image = cropCentre(image, desiredDimension, desiredDimension);
		image = scaleJpgSmallerProportionally(image, maxWidth, maxHeight);

		return(image);
	}

	public static byte[] cropSquareThenScaleJpgSmallerProportionally(byte[] inputImage, int maxWidth, int maxHeight, float quality)
	{
		try
		{
			BufferedImage image = cropSquareThenScaleSmallerProportionally(inputImage, maxWidth, maxHeight);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			writeJpg(baos, quality, image);

			return(baos.toByteArray());
		}
		catch (Exception e)
		{
			logger.error("Error scaling image.", e);
			return(null);
		}
	}

	public static BufferedImage cropCentre(BufferedImage image, int desiredWidth, int desiredHeight)
	{
		int imageWidth = image.getWidth();
		int imageHeight = image.getHeight();

		// scale if necessary
		if (desiredWidth < imageWidth || desiredHeight < imageHeight)
		{
			int newWidth = Math.min(imageWidth, desiredWidth);
			int newHeight = Math.min(imageHeight, desiredHeight);
			int newX = (imageWidth - newWidth) / 2;
			int newY = (imageHeight - newHeight) / 2;

			image = image.getSubimage(newX, newY, newWidth, newHeight);
		}

		return(image);
	}

	public static byte[] scaleJpgSmallerProportionally(byte[] inputImage, int maxWidth, int maxHeight, float quality)
	{
		try
		{
			ByteArrayInputStream bais = new ByteArrayInputStream(inputImage);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			scaleJpgSmallerProportionally(bais, baos, maxWidth, maxHeight, quality);

			return(baos.toByteArray());
		}
		catch (Exception e)
		{
			logger.error("Error scaling image.", e);
			return(null);
		}
	}

	/**
	 * This method will strip exif data
	 * @param quality is only applicable for compression algorithms like jpg and should be 0-1.0
	 * @throws IOException 
	 */
	public static void scaleJpgSmallerProportionally(InputStream inputStream, OutputStream outputStream, int maxWidth, int maxHeight, float quality) throws IOException
	{
		BufferedImage image = ImageIO.read(inputStream);
		image = scaleJpgSmallerProportionally(image, maxWidth, maxHeight);
		writeJpg(outputStream, quality, image);
	}

	public static BufferedImage scaleJpgSmallerProportionally(BufferedImage image, int maxWidth, int maxHeight)
	{
		int imageWidth = image.getWidth();
		int imageHeight = image.getHeight();

		// scale if necessary
		if (maxWidth < imageWidth || maxHeight < imageHeight)
		{
			float shrinkRatio = Math.min((float) maxHeight / imageHeight, (float) maxWidth / imageWidth);
			int newWidth = (int) (imageWidth * shrinkRatio);
			int newHeight = (int) (imageHeight * shrinkRatio);

			image = toBufferedImage(image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH));
		}

		return(image);
	}

	public static void writeJpg(OutputStream outputStream, float quality, BufferedImage image) throws IOException
	{
		// write image 
		ImageWriter jpgImageWriter = getJpgImageWriter();
		try
		{
			// set quality
			ImageWriteParam imageWriteParam = jpgImageWriter.getDefaultWriteParam();
			imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			imageWriteParam.setCompressionQuality(quality);

			try (ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(outputStream))
			{
				jpgImageWriter.setOutput(imageOutputStream);
				IIOImage iioImage = new IIOImage(image, null, null);
				jpgImageWriter.write(null, iioImage, imageWriteParam);
			}
		}
		finally
		{
			jpgImageWriter.dispose();
		}
	}

	/**
	 * You better remember to dispose of this image writer...
	 */
	public static ImageWriter getJpgImageWriter()
	{
		Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("jpg");
		if (writers.hasNext())
		{
			return(writers.next());
		}
		else
		{
			throw(new IllegalStateException("Missing jpg Image Writer"));
		}
	}

	public static void writePng(OutputStream outputStream, BufferedImage image) throws IOException
	{
		// write image 
		ImageWriter pngImageWriter = getPngImageWriter();
		try
		{
			try (ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(outputStream))
			{
				pngImageWriter.setOutput(imageOutputStream);
				IIOImage iioImage = new IIOImage(image, null, null);
				pngImageWriter.write(null, iioImage, null);
			}
		}
		finally
		{
			pngImageWriter.dispose();
		}
	}

	/**
	 * You better remember to dispose of this image writer...
	 */
	public static ImageWriter getPngImageWriter()
	{
		Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("png");
		if (writers.hasNext())
		{
			return(writers.next());
		}
		else
		{
			throw(new IllegalStateException("Missing png Image Writer"));
		}
	}

	public static BufferedImage toBufferedImage(Image image)
	{
		BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = bufferedImage.createGraphics();
		try
		{
			g2d.drawImage(image, 0, 0, null);
		}
		finally
		{
			g2d.dispose();
		}

		return(bufferedImage);
	}

}
