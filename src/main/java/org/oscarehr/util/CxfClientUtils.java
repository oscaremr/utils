/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.common.gzip.GZIPInInterceptor;
import org.apache.cxf.transport.common.gzip.GZIPOutInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.ConnectionType;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.log4j.Logger;

/**
 * This class uses ConfigXmlUtils to controls some configuration parameters, they are all defaulted if not specified.
 * category=misc, property=ws_client_connection_timeout_ms
 * category=misc, property=ws_client_receive_timeout_ms
 * category=misc, property=ws_client_use_gzip
 * category=misc, property=ws_client_gzip_threshold_bytes
 * category=misc, property=allow_all_ssl_certificates
 */
public class CxfClientUtils
{
	private static Logger logger = MiscUtils.getLogger();

	public static final long connectionTimeout = ConfigXmlUtils.getPropertyLong("misc", "ws_client_connection_timeout_ms", 1500L);
	public static final long receiveTimeout = ConfigXmlUtils.getPropertyLong("misc", "ws_client_receive_timeout_ms", 10000L);
	public static final boolean useGZip = ConfigXmlUtils.getPropertyBoolean("misc", "ws_client_use_gzip", true);
	public static final int gZipThreshold = ConfigXmlUtils.getPropertyInt("misc", "ws_client_gzip_threshold_bytes", 0);
	public static final String sslProtocol = ConfigXmlUtils.getPropertyString("misc", "ws_client_ssl_protocol", "TLS");
	public static final boolean allowAllSsl = ConfigXmlUtils.getPropertyBoolean("misc", "allow_all_ssl_certificates", false);

	static
	{
		if (allowAllSsl)
		{
			try
			{
				MiscUtils.setJvmDefaultSSLSocketFactoryAllowAllCertificates();
			}
			catch (Exception e)
			{
				logger.error("Unexpected error", e);
			}
		}

		logger.info("CxfClientUtils using : connectionTimeout=" + connectionTimeout + ", receiveTimeout=" + receiveTimeout + ", useGZip=" + useGZip + ", gZipThreshold=" + gZipThreshold + ", allowAllSsl=" + allowAllSsl+", sslProtocol="+sslProtocol);
	}

	public static void initSslFromConfig()
	{
		// believe it or not this method is actually useful, it will cause the class to load and run the static block which inits the ssl
		// by using the static block we ensure it is initialised but by having this method we can control the order of loading.
	}

	public static class TrustAllManager implements X509TrustManager
	{
		@Override
		public X509Certificate[] getAcceptedIssuers()
		{
			return new X509Certificate[0];
		}

		@Override
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
		{
			// allow local self made
		}

		@Override
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
		{
			// allow local self made
		}
	}

	/**
	 * This method will configure both the ssl and time out for you
	 * @param wsPort
	 */
	public static void configureClientConnection(Object wsPort)
	{
		configureClientConnection(wsPort, connectionTimeout, receiveTimeout);
	}
	
	public static void configureClientConnection(Object wsPort, long receiveTimeout)
	{
		configureClientConnection(wsPort, connectionTimeout, receiveTimeout);
	}

	public static void configureClientConnection(Object wsPort, long connectionTimeout, long receiveTimeout)
	{
		Client cxfClient = ClientProxy.getClient(wsPort);
		HTTPConduit httpConduit = (HTTPConduit) cxfClient.getConduit();

		configureSsl(httpConduit);
		configureTimeout(httpConduit, connectionTimeout, receiveTimeout);

		if (useGZip) configureGzip(cxfClient);
	}

	public static void configureGzip(Client cxfClient)
	{
		cxfClient.getInInterceptors().add(new GZIPInInterceptor());
		cxfClient.getOutInterceptors().add(new GZIPOutInterceptor(gZipThreshold));
	}

	public static void configureTimeout(HTTPConduit httpConduit)
	{
		configureTimeout(httpConduit, connectionTimeout, receiveTimeout);
	}

	public static void configureTimeout(HTTPConduit httpConduit, long connectionTimeout, long receiveTimeout)
	{
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();

		httpClientPolicy.setConnection(ConnectionType.KEEP_ALIVE);
		httpClientPolicy.setConnectionTimeout(connectionTimeout);
		httpClientPolicy.setAllowChunking(false);
		httpClientPolicy.setReceiveTimeout(receiveTimeout);

		httpConduit.setClient(httpClientPolicy);
	}

	public static void configureSsl(HTTPConduit httpConduit)
	{
		TLSClientParameters tslClientParameters = httpConduit.getTlsClientParameters();
		if (tslClientParameters == null) tslClientParameters = new TLSClientParameters();
		tslClientParameters.setDisableCNCheck(true);
		TrustAllManager[] tam = { new TrustAllManager() };
		tslClientParameters.setTrustManagers(tam);
		tslClientParameters.setSecureSocketProtocol(sslProtocol);
		httpConduit.setTlsClientParameters(tslClientParameters);
	}

	/**
	 * For CXF 2.7.x / wss4j 1.6.x
	 * @param user can be userId or userName, all depends on what the received requires
	 * @param password can be password or securityToken, all depends on what the received requires
	 */
	public static void addWSS4JAuthentication(Object user, String password, Object wsPort)
	{
		Client cxfClient = ClientProxy.getClient(wsPort);
		cxfClient.getOutInterceptors().add(new AuthenticationOutWSS4JInterceptor(user, password));
	}

	/**
	 * For CXF 3.0.x / wss4j 2.0.x
	 * @param user can be userId or userName, all depends on what the received requires
	 * @param password can be password or securityToken, all depends on what the received requires
	 */
	public static void addWSS4JAuthentication3(Object user, String password, Object wsPort)
	{
		Client cxfClient = ClientProxy.getClient(wsPort);
		cxfClient.getOutInterceptors().add(new AuthenticationOutWSS4JInterceptor3(user, password));
	}

}
