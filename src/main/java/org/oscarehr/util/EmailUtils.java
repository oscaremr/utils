/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.util.MiscUtils;

/**
 * Example of how to use this is as follows
 * 
 * // Create the email message
 * HtmlEmail email=getHtmlEmail("127.0.0.1", null, null, null); // or just getHtmlEmail();
 * email.addTo("root@[127.0.0.1]", "da root user");
 * email.setFrom("tleung@[127.0.0.1]", "Moi");
 * email.setSubject("test subject");
 * 
 * // set the html message
 * email.setHtmlMsg("<html>html contents</html>");
 * 
 * // set the alternative message
 * email.setTextMsg("text contents");
 * 
 * // send the email
 * email.send();
 * 
 * If the recipient_override parameter is not null, it will assume the value is an email address and use that and replace to/cc/bcc addresses.
 * The print_instead_of_send value should be true or false, if true it will skip the last send step and log it at info level instead.
 * 
 * This class uses ConfigXmlUtils to controls some configuration parameters, they are all defaulted if not specified.
 * category=smtp, property=host, defaulted to smtp.gmail.com
 * category=smtp, property=ssl_port
 * category=smtp, property=user
 * category=smtp, property=password
 * category=smtp, property=recipient_override
 * category=smtp, property=print_instead_of_send
 * category=smtp, property=smtp_timeout
 */
public final class EmailUtils
{
	private static Logger logger = MiscUtils.getLogger();

	private static final String CATEGORY = "smtp";

	public static String smtpHost = ConfigXmlUtils.getPropertyString(CATEGORY, "host", "smtp.gmail.com");
	public static int smtpPort = ConfigXmlUtils.getPropertyInt(CATEGORY, "smtpPort", 587);
	public static boolean smtpTlsRequired = ConfigXmlUtils.getPropertyBoolean(CATEGORY, "tls_required", true);
	public static String smtpUser = ConfigXmlUtils.getPropertyString(CATEGORY, "user");
	public static String smtpPassword = ConfigXmlUtils.getPropertyString(CATEGORY, "password");
	public static String smtpTimeout = ConfigXmlUtils.getPropertyString(CATEGORY, "smtpTimeout", "10000");
	public static String recipientOverride = ConfigXmlUtils.getPropertyString(CATEGORY, "recipient_override");
	public static boolean printInsteadOfSend = ConfigXmlUtils.getPropertyBoolean(CATEGORY, "print_instead_of_send", false);

	public static String getSmtpUser()
	{
		return(smtpUser);
	}

	private static class HtmlEmailWrapper extends HtmlEmail
	{
		@Override
		public String sendMimeMessage()
		{
			try
			{
				if (recipientOverride != null)
				{
					message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipientOverride));
					message.setRecipients(Message.RecipientType.CC, new InternetAddress[0]);
					message.setRecipients(Message.RecipientType.BCC, new InternetAddress[0]);
				}

				if (printInsteadOfSend)
				{
					logger.info(getAsString(message));
				}
				else
				{
					logger.debug("Sending Email to " + Arrays.toString(message.getAllRecipients()));
					return(super.sendMimeMessage());
				}
			}
			catch (Exception e)
			{
				logger.error("Error", e);
			}

			return(null);
		}
	}

	/**
	 * This method will return an HtmlEmail object populated with 
	 * the values passed in, ignoring the parameters in the configuration file.
	 */
	public static HtmlEmail getHtmlEmail(String smtpServer, int smtpPort, boolean smtpTlsRequired, String smtpUser, String smtpPassword) throws EmailException
	{
		logger.debug("smtpServer=" + smtpServer + ", smtpPort=" + smtpPort + ", smtpUser=" + smtpUser + ", smtpPassword=" + smtpPassword);

		HtmlEmail email = null;

		if (recipientOverride != null || printInsteadOfSend) email = new HtmlEmailWrapper();
		else email = new HtmlEmail();

		email.setHostName(smtpServer);

		if (smtpUser != null && smtpPassword != null) email.setAuthentication(smtpUser, smtpPassword);

		email.setSmtpPort(smtpPort);
		email.setTLS(smtpTlsRequired);

		Session session = email.getMailSession();
		Properties properties = session.getProperties();
		properties.setProperty("mail.smtp.connectiontimeout", smtpTimeout);
		properties.setProperty("mail.smtp.timeout", smtpTimeout);

		return(email);
	}

	/**
	 * This method will return an HtmlEmail object populated with 
	 * the smtpServer/smtpUser/smtpPassword from the config xml file.
	 * @throws EmailException 
	 */
	public static HtmlEmail getHtmlEmail() throws EmailException
	{
		return(getHtmlEmail(smtpHost, smtpPort, smtpTlsRequired, smtpUser, smtpPassword));
	}

	/**
	 * This is a convenience method for sending and email to 1 recipient using the configuration file settings.
	 * @throws EmailException 
	 */
	public static String sendEmail(String toEmailAddress, String toName, String fromEmailAddress, String fromName, String subject, String textContents, String htmlContents) throws EmailException
	{
		HtmlEmail htmlEmail = getHtmlEmail();

		htmlEmail.addTo(toEmailAddress, toName);
		if (fromEmailAddress == null) fromEmailAddress = smtpUser;
		htmlEmail.setFrom(fromEmailAddress, fromName);

		htmlEmail.setSubject(subject);
		if (textContents != null) htmlEmail.setTextMsg(textContents);
		if (htmlContents != null) htmlEmail.setHtmlMsg(htmlContents);

		return(htmlEmail.send());
	}

	/**
	 * This method is like a toString for Email objects.
	 */
	public static String getAsString(Email email) throws IOException, MessagingException, EmailException
	{
		email.buildMimeMessage();
		return(getAsString(email.getMimeMessage()));
	}

	/**
	 * This method is like a toString for Email objects.
	 */
	public static String getAsString(MimeMessage mimeMessage) throws IOException, MessagingException
	{
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		mimeMessage.writeTo(os);
		return(os.toString());
	}

	/**
	 * This main method is useful when debugging smtp configuration problems.
	 */
	public static void main(String... argv) throws EmailException
	{
		// gmail : smtp.gmail.com:587		

		String fromEmailAddress = "no-reply-notifications@myoscar.org";
		String toEmailAddress = "tedleungoscar@gmail.com";
		String smtpServer = "smtp.gmail.com";

		int smtpPort = 587;
		boolean smtpTlsRequired=true;
		String smtpUser = "no-reply-notifications@myoscar.org";
		String smtpPassword = "";

		HtmlEmail htmlEmail = EmailUtils.getHtmlEmail(smtpServer, smtpPort, smtpTlsRequired, smtpUser, smtpPassword);

		htmlEmail.addTo(toEmailAddress, toEmailAddress);
		htmlEmail.setFrom(fromEmailAddress, fromEmailAddress);

		htmlEmail.setSubject("test subject");
		htmlEmail.setTextMsg("test contents " + (new java.util.Date()) +" port="+smtpPort+", tls="+smtpTlsRequired);
		
		htmlEmail.send();
	}
}