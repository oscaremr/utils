/**
 * Copyright (c) 2001-2015. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.IOException;
import java.io.InputStream;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.sun.management.UnixOperatingSystemMXBean;

public class VmStat2
{
	private static Logger logger = MiscUtils.getLogger();

	private static final String NATIVE_DRIVE_INFO_COMMAND = "df";
	private static final String NATIVE_MEM_INFO_COMMAND = "free";

	private static OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
	private static ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
	private static List<MemoryPoolMXBean> memoryPools = ManagementFactory.getMemoryPoolMXBeans();
	private static List<GarbageCollectorMXBean> garbageCollectorMXBeans = ManagementFactory.getGarbageCollectorMXBeans();

	
	public static class FileHandleInfo
	{
		public long count;
		public long max;

		@Override
		public String toString()
		{
			return ("count=" + count + ", max=" + max);
		}
	}

	public static class GcInfo
	{
		public String name;
		public String pools;
		public long count;
		public long totalTime;

		@Override
		public String toString()
		{
			return ("name=" + name + ", count=" + count + ", totalTime=" + totalTime + ", pools=" + pools);
		}
	}

	public static class MemInfo
	{
		public String name;
		public long totalSize;
		public long usage;
		public long free;
		public String collectors;

		@Override
		public String toString()
		{
			return ("name=" + name + ", totalSize=" + totalSize + ", usage=" + usage + ", free=" + free + ", collectors=" + collectors);
		}
	}

	public static class ThreadCounts
	{
		public int totalThreads;
		public int deamonThreads;
		public int nonDeamonThreads;
		public int peakThreads;

		@Override
		public String toString()
		{
			return ("totalThreads=" + totalThreads + ", deamonThreads=" + deamonThreads + ", nonDeamonThreads=" + nonDeamonThreads + ", peakThreads=" + peakThreads);
		}
	}

	public static class VmInfo
	{
		public int cpuUsage;
		public ThreadCounts threadCount;
		public FileHandleInfo fileHandleInfo;
		public List<String> osMemInfo;
		public List<MemInfo> memoryUsage;
		public List<GcInfo> gcCounts;
		public List<String> driveInfo;

		@Override
		public String toString()
		{
			StringBuilder sb = new StringBuilder();

			sb.append("cpuUsage:" + cpuUsage + '%');
			sb.append('\n');

			sb.append("thread:" + threadCount);
			sb.append('\n');

			sb.append("fileHandleInfo:" + fileHandleInfo);
			sb.append('\n');

			for (String s : osMemInfo)
			{
				sb.append("osMem:" + s);
				sb.append('\n');
			}

			for (MemInfo memDriveInfo : memoryUsage)
			{
				sb.append("mem:" + memDriveInfo);
				sb.append('\n');
			}

			for (GcInfo gcInfo : gcCounts)
			{
				sb.append("gc:" + gcInfo);
				sb.append('\n');
			}

			for (String s : driveInfo)
			{
				sb.append("drive:" + s);
				sb.append('\n');
			}

			return (sb.toString());
		}
	}

	public static VmInfo getVmInfo()
	{
		VmInfo vmInfo = new VmInfo();

		vmInfo.cpuUsage = getCpuUsagePercent();
		vmInfo.threadCount = getThreadCount();
		vmInfo.fileHandleInfo = getFileHandleInfo();
		vmInfo.osMemInfo = runOsCommand(NATIVE_MEM_INFO_COMMAND);
		vmInfo.memoryUsage = getMemoryUsage();
		vmInfo.gcCounts = getGcCount();
		vmInfo.driveInfo = runOsCommand(NATIVE_DRIVE_INFO_COMMAND);

		return (vmInfo);
	}

	public static FileHandleInfo getFileHandleInfo()
	{
		if (operatingSystemMXBean instanceof UnixOperatingSystemMXBean)
		{
			FileHandleInfo fileHandleInfo=new FileHandleInfo();
			
			UnixOperatingSystemMXBean ubean=(UnixOperatingSystemMXBean)operatingSystemMXBean;
			fileHandleInfo.count=ubean.getOpenFileDescriptorCount();
			fileHandleInfo.max=ubean.getMaxFileDescriptorCount();
			
			return(fileHandleInfo);
		}

		return null;
	}

	public static ThreadCounts getThreadCount()
	{
		ThreadCounts threadCounts = new ThreadCounts();

		threadCounts.totalThreads = threadMXBean.getThreadCount();
		threadCounts.deamonThreads = threadMXBean.getDaemonThreadCount();
		threadCounts.peakThreads = threadMXBean.getPeakThreadCount();
		threadCounts.nonDeamonThreads = threadCounts.totalThreads - threadCounts.deamonThreads;

		return (threadCounts);
	}

	public static ArrayList<MemInfo> getMemoryUsage()
	{
		ArrayList<MemInfo> results = new ArrayList<MemInfo>();

		for (MemoryPoolMXBean memoryPool : memoryPools)
		{
			MemInfo memInfo = new MemInfo();
			MemoryUsage memoryUsage = memoryPool.getUsage();

			memInfo.name = memoryPool.getName();
			memInfo.totalSize = memoryUsage.getMax();
			memInfo.usage = memoryUsage.getUsed();
			memInfo.free = memInfo.totalSize - memInfo.usage;

			memInfo.collectors = Arrays.toString(memoryPool.getMemoryManagerNames());

			results.add(memInfo);
		}

		return (results);
	}

	public static List<GcInfo> getGcCount()
	{
		ArrayList<GcInfo> results = new ArrayList<GcInfo>();

		for (GarbageCollectorMXBean garbageCollectorMXBean : garbageCollectorMXBeans)
		{
			GcInfo gcInfo = new GcInfo();

			gcInfo.name = garbageCollectorMXBean.getName();
			gcInfo.pools = Arrays.toString(garbageCollectorMXBean.getMemoryPoolNames());
			gcInfo.count = garbageCollectorMXBean.getCollectionCount();
			gcInfo.totalTime = garbageCollectorMXBean.getCollectionTime();

			results.add(gcInfo);
		}

		return (results);
	}

	/**
	 * @return the CPU usage as a percentage for the last minute. i.e. 50 is returned for 50%.
	 * The usage may exceed 100% if it's heavily queued up, i.e. 150 means 100% utilisation with 50% back log
	 */
	public static int getCpuUsagePercent()
	{
		return (int) (operatingSystemMXBean.getSystemLoadAverage() * 100 / operatingSystemMXBean.getAvailableProcessors());
	}

	public static List<String> runOsCommand(String s)
	{
		try
		{
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec(s);
			try (InputStream is = process.getInputStream())
			{
				return (IOUtils.readLines(is));
			}
		}
		catch (IOException e)
		{
			logger.error("unexpected error", e);
			return (null);
		}
	}

	public static void main(String... argv) throws Exception
	{
		System.err.println(getVmInfo());
	}
}
