/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/**
 * The purpose of this class is to help track performance of a server. Specifically the inspiration is external TCP traffic 
 * and internal data traffic, i.e. HTTP and either filesystem or db requests.
 * <br /><br />
 * In the final reports we want to show the following : <br />
 * 1) the action (http://foo.com/myContext/locallySensitiveHashServlet) <br />
 * 2) the number of hits (544 requests) <br />
 * 3) the min/max/avg time for the action (min=123ms, avg=340ms, max=780ms) <br />
 * 4) in groups, a) ordered by highest number of hits, b) ordered by largest max time, c) ordered by highest avg time, d) ordered by highest total time
 * 5) the time distribution should be of daily granularity, the time groups would then be a) monthly, b) daily, c) by server restart times (i.e. time frame starts when server starts, ends when server is restarted)
 * <br /><br />
 * To achieve the above, we can track access in daily buckets where the bucket contains (action,requestCount,minTime,maxTime,totalTime)
 * From that the reports should be able to generate what we want.
 * <br />
 * <hr />
 * <br />
 * Implementation details : <br />
 * Write to the local file system for efficiency purposes. Tree structure is as follows :
 * 	data_root
 *		├── server_start_log.xml
 *		├── db_access
 *		└── http_access
 *			└── 2010
 *		  		├── tracking_2010-04-06.xml
 *		  		└── tracking_2010-04-07.xml
 * <br /><br />
 * data_root : should be configurable in a system wide manner.
 * <br /><br />
 * server_start_log.xml keeps track of when the server starts up shutdown is not important as it's either when the next start occurred or "current_time". Entries should be in the ISO date_time format (my example here might not be accurate as I'm typing it in off memory). Entries are unsorted, you can sort them after you read them for processing.
 *		<ServerStartLog>
 *			<StartTime>2010-04-06T19:56:33 PST</StartTime>
 *		</ServerStartLog>
 * <br /><br />
 * db_access and http_access would be 2 example categories, every time you add a category it should make a new dir. A directory listing in the 
 * data_root should give you all categories. 
 * <br /><br />
 * tracking_2010-04-06.xml is a sample data file, in this case for http_access, the contents are data stored in xml format in an unsorted fashion
 *		<PerformanceTracker date="2010-04-06">
 *			<Action requestCount="544" maxTimeMs="780" totalTimeMs="563452" maxDataSize="123" totalDataSize="4567">/myContext/locallySensitiveHashServlet</action>
 *			<Action requestCount="111" maxTimeMs="333" totalTimeMs="444444" maxDataSize="123" totalDataSize="4567">/myContext/foo.jsp</action>
 *		</PerformanceTracker>
 * <br /><br />
 * We'll start with a synchronisedMap and we can optimise this in the future if we find it's taking too much time. The idea is 
 * that this is to track coarse grain requests on the magnitude of milliseconds or even seconds so a few nano seconds should be fine.
 * <br /><br />
 * This works hand-in-hand with PerformanceReporter so make sure you look at the javadocs for that class. 
 * <hr />
 * Expected usage is : <br />
 * 1) call PerformanceTrackerUtils.addServerStartLogEntry() in your contextStartupListener. <br />
 * 2) make a class with singleton instances of PerformanceTracker i.e. <br />
 *      public static final PerformanceTracker httpAccess=new PerformanceTracker("/data", "http_access") <br />
 *      public static final PerformanceTracker dbAccess=new PerformanceTracker("/data", "db_access") <br />
 * 3) in your code, like an httpServletFilter, time the call and upon completion call httpAccess.addEntry(url, time);
 */
public final class PerformanceTracker
{
	private static Logger logger = LogManager.getLogger(PerformanceTracker.class);

	private static final Timer timer = new Timer(PerformanceTracker.class.getName(), true);

	public class LogRollerTimerTask extends TimerTask
	{
		@Override
		public void run()
		{
			try
			{
				writeDataAndRollLogFileIfRequired();
			}
			catch (Exception e)
			{
				logger.error("Error", e);
			}
		}
	}

	private String dataRoot;
	private String categoryName;
	private HashMap<String, PerformanceTrackerEntry> entries;
	private GregorianCalendar currentLogFileTime;
	private LogRollerTimerTask timerTask;

	public PerformanceTracker(String dataRoot, String categoryName) throws ParserConfigurationException, SAXException, IOException
	{
		this.dataRoot = dataRoot;
		this.categoryName = categoryName;
		currentLogFileTime = new GregorianCalendar();
		entries = PerformanceTrackerUtils.readTrackingData(dataRoot, categoryName, currentLogFileTime);

		timerTask = new LogRollerTimerTask();
		timer.schedule(timerTask, 60000, 60000);
	}

	public void addEntry(String action, int timeForAction, long dataSize)
	{
		PerformanceTrackerEntry entry = null;

		synchronized (entries)
		{
			entry = entries.get(action);
		}

		if (entry == null)
		{
			entry = new PerformanceTrackerEntry(action, timeForAction, dataSize);
			synchronized (entries)
			{
				entries.put(action, entry);
			}
		}
		else
		{
			entry.addRequest(timeForAction, dataSize);
		}
	}

	public void stopPerformanceTracker() throws ClassCastException, ParserConfigurationException, SAXException, IOException, InterruptedException, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		timerTask.cancel();
		writeDataAndRollLogFileIfRequired();
	}

	private void writeDataAndRollLogFileIfRequired() throws ParserConfigurationException, SAXException, IOException, InterruptedException, ClassCastException, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		GregorianCalendar currentTime = new GregorianCalendar();
		if (!DateUtils.isSameDay(currentLogFileTime, currentTime))
		{
			GregorianCalendar previousDate = currentLogFileTime;
			HashMap<String, PerformanceTrackerEntry> previousEntries = entries;

			currentLogFileTime = currentTime;
			entries = PerformanceTrackerUtils.readTrackingData(dataRoot, categoryName, currentLogFileTime);

			// timing issue, make sure other threads have had time to call their increments.
			Thread.sleep(2000);
			PerformanceTrackerUtils.writeTrackingData(dataRoot, categoryName, previousDate, previousEntries);
		}
		else
		{
			synchronized (entries)
			{
				PerformanceTrackerUtils.writeTrackingData(dataRoot, categoryName, currentLogFileTime, entries);
			}
		}
	}
}
