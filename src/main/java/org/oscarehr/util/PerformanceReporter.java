/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.time.DateUtils;
import org.xml.sax.SAXException;

/**
 * The purpose of this class is to make sense of the data tracked in PerformanceTracker.
 * <br /><br />
 * As noted in PerformanceTracker, we would like to report the most performed action, most time consuming action, and the longest running time action.
 * <br /><br />  
 * The initial inspiration for the reports output is as follows :<br />
 * 1) a report that shows the top N highest average time requests for a given month (manually open multiple reports to compare different months). This will show how the system is generally tracking over a year in terms of performance.<br />
 * 2) a report that shows the top N highest average time requests for a given server uptime timeframe. This will allow you to compare the previous release with the current release and see if things got better or worst.<br />
 * The above could then be also sorted by highest max time to see which operations are the worst on the single point of view.<br />
 * The above could also be sorted by highest count to see which operations to possibly streamline in a workflow.<br />
 * <br />
 * <hr />
 * <br />
 * Implementation details : <br />
 * First implementation will be unoptimised, lets hope no one complains as it's technical system reports anyways. In the future, we can materialise the results and write them back to disk if a "month has finished" or something to prevent recalculations of the same thing. 
 * <br /><br />
 * Note the reports are always just by a given time frame. We could just allow input of start and end timeframes. 
 * There are 2 special time frames which we talk about often Monthly and Server UpTime but they are in reality just a start and end time (and we have accepted granularity of the day).
 * For the server uptime reports, there's the server_start_log.xml which we can just calculate the start and end times from.
 */
public final class PerformanceReporter
{
	/**
	 * Start and end times have granularity of 1 day, the endTime is exclusive.
	 * 
	 * There is no particular sort order, sorting can be done on the returned values using the comparators provided in PerformanceTrackerEntry.
	 */
	public static HashMap<String, PerformanceTrackerEntry> getConsolodatedResults(String dataRoot, String categoryName, Calendar startTime, Calendar endTime) throws ParserConfigurationException, SAXException, IOException
	{
		startTime = DateUtils.truncate(startTime, Calendar.DAY_OF_MONTH);
		endTime = DateUtils.truncate(endTime, Calendar.DAY_OF_MONTH);

		if (!startTime.before(endTime)) throw(new IllegalArgumentException("startTime is not before endTime after hour/minute/second truncation"));

		Calendar timePointer = (Calendar) startTime.clone();
		HashMap<String, PerformanceTrackerEntry> results = new HashMap<String, PerformanceTrackerEntry>();

		while (timePointer.before(endTime))
		{
			HashMap<String, PerformanceTrackerEntry> tempEntries = PerformanceTrackerUtils.readTrackingData(dataRoot, categoryName, timePointer);
			for (PerformanceTrackerEntry tempEntry : tempEntries.values())
			{
				merge(results, tempEntry);
			}

			timePointer.add(Calendar.DAY_OF_YEAR, 1);
			timePointer.getTimeInMillis();
		}

		return(results);
	}

	private static void merge(HashMap<String, PerformanceTrackerEntry> results, PerformanceTrackerEntry tempEntry)
	{
		PerformanceTrackerEntry existingEntry = results.get(tempEntry.getAction());

		if (existingEntry == null)
		{
			results.put(tempEntry.getAction(), tempEntry);
		}
		else
		{
			existingEntry.mergeEntries(tempEntry);
		}
	}
}
