/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * This cache uses disk instead of memory for the storage location. The purpose is for large items that need caching, as an example if you're doing image manipulation, the image files are large and you want to cache the manipulation results as manipulation
 * takes a long time.
 * 
 * Requirements : 
 * - we need a place to store the files - we need a max item limit 
 * - we need a max duration limit, probably coarse grain like per hour at shortest 
 * - We need pools to flush, so if max time=24 hours, and pools = 4, then each pool is 6 hours
 * worth of cache data. If we do currentTimeMs/poolDuration then we should get some decent pool keys.
 * 
 * Flush algorithm : 
 * We need a flush timer task which checks if we should flush a pool of items based on time. 
 * Upon flush we should wipe the entire cache directory for everything except valid cache pool keys, this will clean up after reconfiguration and long server outtages where cache pool keys maybe unexpected. 
 * Valid poolKeys are current poolKey minus 1 though maxPools.
 * Upon flush we should also count the number of items in existing valid pools so we have a counter for how many items to allow in the  current pool before a flush pool is called. If there's only 1 pool left we will be forced to flush ourselves and start a clean cache. This only happens if all pools were empty and except the latest one is filled to maxItems, this is very odd so we should log a warning as you'd probably need a better configuration.
 * 
 * Add algorithm is : 
 * - pool = currentTimeMs/poolDuration 
 * - key must be unique file-name-allowable key.
 * 
 * Get algorithm is : 
 * - pool = currentTimeMs/poolDuration 
 * - read item from pool 
 * - if not found pool=pool-1 and try again, for all valid pools.
 */
public final class DiskCache<K extends DiskCacheKey>
{
	private static Logger logger = LogManager.getLogger(DiskCache.class);
	private static Timer timer = new Timer(DiskCache.class.getName(), true);
	private static final int MAX_MERGE_REQUEST_PAUSE_CYCLES=20; 

	private class FlushTimerTask extends TimerTask
	{
		@Override
		public void run()
		{
			try
			{
				promptShiftAndFlushPools();
			}
			catch (Exception e)
			{
				logger.error("Error", e);
			}
		}
	}

	private DiskCacheDataRetrieverInterface<K> diskCacheDataRetrieverInterface;
	private long mergeRequestPausePeriod=5;
	private String cacheDirectory;
	private int maxItemsToCache;
	private int maxCachePools;
	private long maxPoolCacheTimeMs;
	
	private HashMap<K, Boolean> uncachedRequestLockManager=new HashMap<K, Boolean>();

	/**
	 * @param diskCacheDataRetrieverInterface can be null if you want to manually populate the cache, this is not recommended though as you will need to deal with simultaneous cache misses and simultaneous disk writes.
	 * @param maxMergeRequestBlockTimeMs is only used if diskCacheDataRetrieverInterface is not null, this number is the max time simultanious requests to the same uncached items will block before returning null assuming and error has occurred. This number should be larger than your expected processing time, i.e. if requesting a scaled image genmerally takes 1 second, then you should probably set this to 5000 ms.  
	 * @param cacheDirectory is where this cache will put data
	 * @param maxItemsToCache max items in cache is only a rough guide, the actual items may exceed this between flush checking intervals.
	 * @param maxCacheTimeMs this is an upper bound on the cache time, not a lower bound
	 * @param maxCachePools This controls how the cache is flushed, it is not flushed one item at a time but in blocks, so as an example if you cache items for 1 day and have 4 pools, then we will evict 6 hours of items at a time. You want to keep the pool count reasonable, like 4 or so, too few and you'll flush a lot every time, too many and it'll take too long to lookup.
	 * @param flushCheckingPeriodMs this should be set reasonably low if your maxCacheItems is sensitive. This is fairly expensive but it's in another thread so values between 5 minutes to 1 hour maybe appropriate.  
	 */
	public DiskCache(DiskCacheDataRetrieverInterface<K> diskCacheDataRetrieverInterface, long maxMergeRequestBlockTimeMs, String cacheDirectory, int maxItemsToCache, long maxCacheTimeMs, int maxCachePools, long flushCheckingPeriodMs)
	{
		this.diskCacheDataRetrieverInterface = diskCacheDataRetrieverInterface;
		this.mergeRequestPausePeriod=Math.max(mergeRequestPausePeriod, (int) (maxMergeRequestBlockTimeMs/MAX_MERGE_REQUEST_PAUSE_CYCLES));
		this.cacheDirectory = cacheDirectory;
		this.maxItemsToCache = maxItemsToCache;
		this.maxCachePools = maxCachePools;
		maxPoolCacheTimeMs = maxCacheTimeMs / maxCachePools;

		File cacheDirFile = new File(cacheDirectory);
		cacheDirFile.mkdirs();

		timer.schedule(new FlushTimerTask(), 10000, flushCheckingPeriodMs);
	}

	public void put(K cacheKey, byte[] cacheData) throws IOException
	{
		long poolKey = System.currentTimeMillis() / maxPoolCacheTimeMs;
		File file = new File(cacheDirectory + '/' + poolKey + '/' + cacheKey.getFilename());
		FileUtils.writeByteArrayToFile(file, cacheData);
	}

	public byte[] get(K cacheKey) throws IOException, InterruptedException
	{
		byte[] b=readFromDisk(cacheKey);
		if (b!=null) return(b);
		
		b = getUncachedResultsAndAddToCacheMergeSimultaneousRequests(cacheKey);
		return (b);
	}

	private byte[] readFromDisk(K cacheKey) throws IOException
	{
		long poolKey = System.currentTimeMillis() / maxPoolCacheTimeMs;

		for (int i = 0; i < maxCachePools; i++)
		{
			File file = new File(cacheDirectory + '/' + (poolKey - i) + '/' + cacheKey.getFilename());
			if (file.exists())
			{
				byte[] b = FileUtils.readFileToByteArray(file);
				return (b);
			}
		}
		
		return(null);
	}
	
	/**
	 * This method will get the results from the data retriever.
	 * If a result is returned it will add it to the cache.
	 * If multiple simultaneous requests are made, only one will proceed,
	 * the other requests will block waiting on that requests. When results 
	 * are returned from that request, the results will be read from the cache 
	 * instead.
	 * 
	 * There is a maxBlock time, after the maxBlock time the other requests will return null
	 * assuming an error has occurred (and we don't want to request more errors). 
	 */
	private byte[] getUncachedResultsAndAddToCacheMergeSimultaneousRequests(K cacheKey) throws IOException, InterruptedException
	{
		if (diskCacheDataRetrieverInterface==null) return(null);
		
		boolean obtainedLock=obtainUncachedRequestLock(cacheKey);
		if (obtainedLock)
		{
			try
			{
				byte[] b=diskCacheDataRetrieverInterface.getUncachedResult(cacheKey);
				if (b==null) return(null);
				
				put(cacheKey, b);
				return(b);
			}
			finally
			{
				releaseUncachedRequestLock(cacheKey);
			}
		}
		else
		{
			for (int i=0; i<MAX_MERGE_REQUEST_PAUSE_CYCLES; i++)
			{
				if (logger.isDebugEnabled()) logger.debug("blocking merge on "+cacheKey);
				
				Thread.sleep(mergeRequestPausePeriod);
				boolean isStillLocked=checkIsLocked(cacheKey);
				if (!isStillLocked)
				{
					byte[] b=readFromDisk(cacheKey);
					return(b);
				}
			}
		}
		
		return(null);
	}
	
	/**
	 * @return true if able to lock, false if some one else has lock
	 */
	private boolean obtainUncachedRequestLock(K cacheKey)
	{
		synchronized (uncachedRequestLockManager)
		{
			Boolean result=uncachedRequestLockManager.get(cacheKey);
			if (result==null)
			{
				uncachedRequestLockManager.put(cacheKey, Boolean.TRUE);
				return(true);
			}
			else
			{
				return(false);
			}
		}
	}

	/**
	 * You should only call this method if you previously obtained a lock. You should not unlock some one elses lock.
	 */
	private void releaseUncachedRequestLock(K cacheKey)
	{
		synchronized (uncachedRequestLockManager)
		{
			uncachedRequestLockManager.remove(cacheKey);
		}
	}
	
	/**
	 * @return true if locked, false if not locked.
	 */
	private boolean checkIsLocked(K cacheKey)
	{
		synchronized (uncachedRequestLockManager)
		{
			return(uncachedRequestLockManager.containsKey(cacheKey));
		}
	}
	
	public void remove(K cacheKey)
	{
		ArrayList<String> validPoolKeyNames = getValidPoolKeyNames();

		for (String poolKey : validPoolKeyNames)
		{
			File file = new File(cacheDirectory + '/' + poolKey + '/' + cacheKey.getFilename());
			if (file.exists()) file.delete();
		}
	}

	private void promptShiftAndFlushPools() throws IOException
	{
		ArrayList<String> validPoolKeyNames = getValidPoolKeyNames();
		removeExtraDirectoriesAndPools(validPoolKeyNames);
		ensureMaxItemLimit(validPoolKeyNames);
	}

	private void ensureMaxItemLimit(ArrayList<String> validPoolKeyNames) throws IOException
	{
		for (int i = maxCachePools; i >= 0; i--)
		{
			int countTemp = countAllItems(validPoolKeyNames);
			if (countTemp > maxItemsToCache)
			{
				removeOldestPool(validPoolKeyNames);
			}
		}
	}

	private void removeOldestPool(ArrayList<String> validPoolKeyNames) throws IOException
	{
		for (int i = validPoolKeyNames.size() - 1; i >= 0; i--)
		{
			String poolKey = validPoolKeyNames.get(i);
			File file = new File(cacheDirectory + '/' + poolKey);

			if (file.exists())
			{
				FileUtils.deleteDirectory(file);
				return;
			}
		}
	}

	private int countAllItems(ArrayList<String> validPoolKeyNames)
	{
		int totalCount = 0;

		for (String poolKey : validPoolKeyNames)
		{
			File file = new File(cacheDirectory + '/' + poolKey);
			if (file.exists())
			{
				String[] result = file.list();
				if (result != null) totalCount = totalCount + result.length;
			}
		}

		return (totalCount);
	}

	private void removeExtraDirectoriesAndPools(ArrayList<String> validPoolKeyNames) throws IOException
	{
		File dir = new File(cacheDirectory);
		for (File f : dir.listFiles())
		{
			if (f.isDirectory())
			{
				if (!validPoolKeyNames.contains(f.getName()))
				{
					FileUtils.deleteDirectory(f);
				}
			}
		}
	}

	private ArrayList<String> getValidPoolKeyNames()
	{
		long currentPoolKey = System.currentTimeMillis() / maxPoolCacheTimeMs;
		ArrayList<String> validPoolKeys = new ArrayList<String>();

		for (int i = 0; i < maxCachePools; i++)
		{
			validPoolKeys.add(String.valueOf(currentPoolKey - i));
		}

		return (validPoolKeys);
	}
}
