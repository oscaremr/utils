/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

import javax.faces.FactoryFinder;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

@ManagedBean
@ApplicationScoped
public final class JsfUtils
{
	private static Logger logger = MiscUtils.getLogger();

	public static final String REDIRECT_QUERY_STRING_KEY_VALUE_PAIR = "faces-redirect=true";

	public static HashSet<String> supportedLanguages = null;

	public static String appendRedirectQueryString(String s)
	{
		if (s.indexOf('?') == -1) return(s + '?' + REDIRECT_QUERY_STRING_KEY_VALUE_PAIR);
		else return(s + '&' + REDIRECT_QUERY_STRING_KEY_VALUE_PAIR);
	}

	public static <T> T getJsfBean(String beanName, Class<T> beanClass)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		return(getJsfBean(context, beanName, beanClass));
	}

	public static <T> T getJsfBean(FacesContext context, String beanName, Class<T> beanClass)
	{
		T bean = context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", beanClass);
		return(bean);
	}

	public static String getRequestParameterAttribute(String key)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
		String result = externalContext.getRequestParameterMap().get(key);
		result = StringUtils.trimToNull(result);

		if (result == null)
		{
			result = (String) context.getAttributes().get(key);
		}

		return(result);
	}

	public static void setRequestAttribute(String key, String value)
	{
		FacesContext context = FacesContext.getCurrentInstance();
		context.getAttributes().put(key, value);
	}

	public static HttpServletRequest getHttpServletRequest()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		return(getHttpServletRequest(context));
	}

	public static HttpServletRequest getHttpServletRequest(FacesContext context)
	{
		ExternalContext externalContext = context.getExternalContext();
		return((HttpServletRequest) externalContext.getRequest());
	}

	public static HttpSession getSession()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		return(getSession(context));
	}

	public static HttpSession getSession(FacesContext context)
	{
		ExternalContext externalContext = context.getExternalContext();
		return((HttpSession) externalContext.getSession(true));
	}

	public static String getContextPath()
	{
		return(getContextPath(FacesContext.getCurrentInstance()));
	}

	public static String getContextPath(FacesContext context)
	{
		return(context.getExternalContext().getRequestContextPath());
	}

	public static void addLocalisedInfoMessage(String messageKey)
	{
		WebUtils.addLocalisedInfoMessage(getHttpServletRequest(), messageKey);
	}

	public static void addLocalisedInfoMessage(String messageKey, String additionalText)
	{
		WebUtils.addLocalisedInfoMessage(getHttpServletRequest(), messageKey, additionalText);
	}

	public static void addLocalisedErrorMessage(String messageKey)
	{
		WebUtils.addLocalisedErrorMessage(getHttpServletRequest(), messageKey);
	}

	public static void addLocalisedErrorMessage(String messageKey, String additionalText)
	{
		WebUtils.addLocalisedErrorMessage(getHttpServletRequest(), messageKey, additionalText);
	}

	public static String getLocalisedString(String key)
	{
		return(LocaleUtils.getMessage(getHttpServletRequest(), key));
	}

	public static Locale getLocale()
	{
		return(getHttpServletRequest().getLocale());
	}

	/**
	 * @return the langauge of choice if supported like "en" "fr", if their language is not supported return "en".
	 */
	public static String getLanguageString()
	{
		// sort out language string
		Locale local = JsfUtils.getLocale();
		String languageString = local.getLanguage();

		// make sure we have their language, or default to english.
		if (!isSupportedLanguage(languageString))
		{
			languageString = Locale.ENGLISH.getLanguage();
		}

		return(languageString);
	}

	public static boolean isSupportedLanguage(String language)
	{
		if (supportedLanguages == null)
		{
			supportedLanguages = new HashSet<String>();
			FacesContext context = FacesContext.getCurrentInstance();
			Iterator<Locale> it = context.getApplication().getSupportedLocales();
			while (it.hasNext())
			{
				Locale locale = it.next();
				supportedLanguages.add(locale.getLanguage());
			}
		}

		return(supportedLanguages.contains(language));
	}

	public static void removeBeanInstance(Object o)
	{
		HttpSession session = getSession();
		if (session == null) return;

		Enumeration<String> e = session.getAttributeNames();
		while (e.hasMoreElements())
		{
			String name = e.nextElement();
			Object sessionObject = session.getAttribute(name);
			if (o == sessionObject)
			{
				session.removeAttribute(name);
				return;
			}
		}
	}

	public static void removeJsfBeans(HttpSession session, Class<?> theClass)
	{
		logger.debug("Removing jsf beans of type : " + theClass.getSimpleName());

		if (session == null) return;

		Enumeration<String> e = session.getAttributeNames();
		while (e.hasMoreElements())
		{
			String key = e.nextElement();
			Object value = session.getAttribute(key);
			if (theClass.isInstance(value))
			{
				logger.debug("removing jsf bean : " + key);
				session.removeAttribute(key);
			}
		}
	}

	/**
	 * suitable for accessing the context/beans but not rendering.
	 */
	public static FacesContext getFacesContext(HttpServletRequest request, HttpServletResponse response)
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();

		if (facesContext == null)
		{
			FacesContextFactory contextFactory = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
			LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
			Lifecycle lifecycle = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);

			facesContext = contextFactory.getFacesContext(request.getSession().getServletContext(), request, response, lifecycle);
		}

		return facesContext;
	}

	public static String getJsIsoDateFormat()
	{
		return(DateUtils.JS_ISO_DATE_FORMAT);
	}
}