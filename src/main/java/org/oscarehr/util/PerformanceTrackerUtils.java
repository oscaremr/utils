/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public final class PerformanceTrackerUtils
{
	public static final String SERVER_START_LOG_FILENAME = "server_start_log.xml";
	public static final String START_TIME_XML_TAG_NAME = "StartTime";
	public static final String TRACKING_FILE_NAME_PREFIX = "tracking_";
	public static final String ACTION_XML_TAG_NAME = "Action";

	private static void ensureDataRootExists(String dataRoot)
	{
		File dataRootDir = new File(dataRoot);
		dataRootDir.mkdirs();
	}

	/**
	 * add entry to start log, will create new file if none exists.
	 */
	public static void addServerStartLogEntry(String dataRoot, Calendar startDate) throws ParserConfigurationException, IOException, ClassCastException, ClassNotFoundException, InstantiationException, IllegalAccessException, SAXException
	{
		ensureDataRootExists(dataRoot);

		File file = new File(dataRoot + '/' + SERVER_START_LOG_FILENAME);
		Document doc = null;

		if (file.exists())
		{
			doc = getServerStartLogXml(file);
		}
		else
		{
			doc = XmlUtils.newDocument("ServerStartLog");
		}

		XmlUtils.appendChildToRoot(doc, START_TIME_XML_TAG_NAME, DateFormatUtils.ISO_DATETIME_FORMAT.format(startDate));

		byte[] data = XmlUtils.toBytes(doc, true);
		FileUtils.writeByteArrayToFile(file, data);
	}

	/**
	 * TreeSet representing all the server startups, will be empty list if the file doesn't exist. 
	 * Defaults to ascending order, so you might have to reverse the list to get descending.
	 */
	public static TreeSet<Calendar> getServerStartLog(String dataRoot) throws ClassCastException, IOException, ParserConfigurationException, SAXException, ParseException
	{
		TreeSet<Calendar> results = new TreeSet<Calendar>();

		File file = new File(dataRoot + '/' + SERVER_START_LOG_FILENAME);
		if (file.exists())
		{
			Document doc = getServerStartLogXml(file);

			Node rootNode = doc.getFirstChild();
			ArrayList<String> startTimes = XmlUtils.getChildNodesTextContents(rootNode, START_TIME_XML_TAG_NAME);

			// convert entries into calendars
			for (String startTime : startTimes)
			{
				GregorianCalendar cal = parseIsoDateTimeToCalendar(startTime);
				results.add(cal);
			}
		}

		return(results);
	}

	public static GregorianCalendar parseIsoDateTimeToCalendar(String isoDate) throws ParseException
	{
		String pattern = DateFormatUtils.ISO_DATETIME_FORMAT.getPattern();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		Date date = simpleDateFormat.parse(isoDate);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.getTimeInMillis();

		return(cal);
	}

	public static GregorianCalendar parseIsoDateToCalendar(String isoDate) throws ParseException
	{
		String pattern = DateFormatUtils.ISO_DATE_FORMAT.getPattern();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		Date date = simpleDateFormat.parse(isoDate);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.getTimeInMillis();

		return(cal);
	}

	/**
	 * gets data file as xml
	 */
	private static Document getServerStartLogXml(File file) throws IOException, ParserConfigurationException, SAXException, ClassCastException
	{
		try (FileInputStream fileInputStream = new FileInputStream(file))
		{
			Document doc = XmlUtils.toDocument(fileInputStream);
			return(doc);
		}
	}

	public static TreeSet<String> getAllCategories(String dataRoot)
	{
		File dataRootDir = new File(dataRoot);

		File[] entries = dataRootDir.listFiles();
		TreeSet<String> results = new TreeSet<String>();

		for (File file : entries)
		{
			if (file.isDirectory()) results.add(file.getName());
		}

		return(results);
	}

	private static String getTrackingFullDirectoryPath(String dataRoot, String categoryName, Calendar date)
	{
		return(dataRoot + '/' + categoryName + '/' + date.get(Calendar.YEAR));
	}

	private static String getTrackingFullFilePath(String dataRoot, String categoryName, Calendar date)
	{
		String fileName = TRACKING_FILE_NAME_PREFIX + DateFormatUtils.ISO_DATE_FORMAT.format(date) + ".xml";
		String fullDirectoryPath = getTrackingFullDirectoryPath(dataRoot, categoryName, date);
		return(fullDirectoryPath + '/' + fileName);
	}

	/**
	 * This method will create the category and year directories if absent, will do nothing if already exists
	 * @param categoryName should be something normal that can be used as a directory name
	 */
	private static void ensureCategoryAndYearDirectories(String dataRoot, String categoryName, Calendar date)
	{
		String fullDirectoryPath = getTrackingFullDirectoryPath(dataRoot, categoryName, date);
		File file = new File(fullDirectoryPath);
		file.mkdirs();
	}

	public static void writeTrackingData(String dataRoot, String categoryName, Calendar date, HashMap<String, PerformanceTrackerEntry> entries) throws ParserConfigurationException, ClassCastException, ClassNotFoundException, InstantiationException, IllegalAccessException, IOException
	{
		ensureCategoryAndYearDirectories(dataRoot, categoryName, date);

		Document doc = XmlUtils.newDocument("PerformanceTracker");
		Element rootNode = (Element) doc.getFirstChild();
		rootNode.setAttribute("date", DateFormatUtils.ISO_DATE_FORMAT.format(date));

		for (PerformanceTrackerEntry entry : entries.values())
		{
			Element element = entry.toXmlElement(doc);
			rootNode.appendChild(element);
		}

		String fullFilePath = getTrackingFullFilePath(dataRoot, categoryName, date);
		File file = new File(fullFilePath);

		byte[] data = XmlUtils.toBytes(doc, true);
		FileUtils.writeByteArrayToFile(file, data);
	}

	/**
	 * Key is the action. Guaranteed to return a Map, the map will be empty if the file didn't exist. 
	 */
	public static HashMap<String, PerformanceTrackerEntry> readTrackingData(String dataRoot, String categoryName, Calendar date) throws ParserConfigurationException, SAXException, IOException
	{
		HashMap<String, PerformanceTrackerEntry> results = new HashMap<String, PerformanceTrackerEntry>();

		String fullFilePath = getTrackingFullFilePath(dataRoot, categoryName, date);
		File file = new File(fullFilePath);
		if (file.exists())
		{
			try (FileInputStream fileInputStream = new FileInputStream(file))
			{
				Document doc = XmlUtils.toDocument(fileInputStream);
				Node rootNode = doc.getFirstChild();
				ArrayList<Node> nodes = XmlUtils.getChildNodes(rootNode, ACTION_XML_TAG_NAME);
				for (Node node : nodes)
				{
					PerformanceTrackerEntry performanceTrackerEntry = new PerformanceTrackerEntry(node);
					results.put(performanceTrackerEntry.getAction(), performanceTrackerEntry);
				}
			}
		}

		return(results);
	}
}
