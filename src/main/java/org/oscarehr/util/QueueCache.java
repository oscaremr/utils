/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.WeakHashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * This is a hash map which limits the number of items with in. The intended use is for a LRU cache.
 * Implementation will be hashmaps behind the scene, when one fills the next is used, when all fills 
 * an entire pool is emptied.  
 * 
 * There are reasonable limits, i.e. there's no point in having 10 pools and max items of 10....
 * like wise even 10 pools and 100 items is fairly silly. In general the pool sizes should be large, 
 * and the number of pools should be small. Something like 1000 items or more in 4 pools is good. 
 * you should never really want more than 10 pools and no less than 3.
 * 
 * As of 2012-12-12, the newer ehcache seems to have pretty good programmatic support now, we should consider
 * deprecating this and using ehcache instead in the future. The benchmark class shows the performance difference
 * as well as how to programmatically. Although the performance of ehcache is the worst, keep in mind those numbers are in
 * nano seconds and the caching is meant for long operations i.e. > 1ms style operations. Sooner or later JSR107 will finalise too
 * and we can have some standard java cache... so maybe well skip ehcache entierly and wait for a JSR107 cache
 */
public final class QueueCache<K, V>
{
	private static WeakHashMap<String, QueueCache<?,?>> cacheTracker=null;
	
	private static Logger logger = LogManager.getLogger(QueueCache.class);
	private static Timer timer = new Timer(QueueCache.class.getName(), true);

	private class ShiftTimerTask extends TimerTask
	{
		@Override
		public void run()
		{
			try
			{
				shiftPools();
			}
			catch (Exception e)
			{
				logger.error("Error", e);
			}
		}
	}

	private HashMap<K, V>[] data;
	private int maxPoolSize;
	private QueueCacheValueCloner<V> cloner = null;

	public QueueCache(String name, int pools, int objectsToCache, long maxTimeToCache, QueueCacheValueCloner<V> cloner)
	{
		this(name, pools, objectsToCache, cloner);

		timer.schedule(new ShiftTimerTask(), maxTimeToCache / pools, maxTimeToCache / pools);
	}

	/**
	 * @param cloner can be null to share data objects in cache (becareful sharing, make sure you know what you're doing).
	 */
	@SuppressWarnings("unchecked")
	public QueueCache(String name, int pools, int objectsToCache, QueueCacheValueCloner<V> cloner)
	{
		this.cloner = cloner;
		data = new HashMap[pools];

		maxPoolSize = Math.max(10, objectsToCache / pools);

		for (int i = 0; i < pools; i++)
		{
			data[i] = new HashMap<K, V>((int) (maxPoolSize * 1.5));
		}
		
		if (cacheTracker!=null)
		{
			cacheTracker.put(name, this);
		}
	}

	public void flushCache()
	{
		for (int i=0; i<data.length; i++)
		{
			shiftPools();
		}
	}
	
	public void put(K key, V value)
	{
		Map<K, V> pool = data[0];

		if (cloner == null)
		{
			synchronized (pool)
			{
				pool.put(key, value);
			}
		}
		else
		{
			V duplicate = cloner.cloneBean(value);
			synchronized (pool)
			{
				pool.put(key, duplicate);
			}
		}

		// shuffle pools if required.
		int poolSize;
		synchronized (pool)
		{
			poolSize = pool.size();
		}
		if (poolSize > maxPoolSize) shiftPools();
	}

	private void shiftPools()
	{
		synchronized (data)
		{
			for (int i = data.length - 1; i > 0; i--)
			{
				data[i] = data[i - 1];
			}

			data[0] = new HashMap<K, V>((int) (maxPoolSize * 1.5));
		}
	}

	public int[] getPoolSizes()
	{
		int[] sizes = new int[data.length];

		synchronized (data)
		{
			for (int i = 0; i < data.length; i++)
			{
				sizes[i] = data[i].size();
			}
		}

		return(sizes);
	}

	public void remove(K key)
	{
		for (int i = 0; i < data.length; i++)
		{
			Map<K, V> pool = data[i];
			synchronized (pool)
			{
				pool.remove(key);
			}
		}
	}

	public V get(K key)
	{
		V result;

		for (int i = 0; i < data.length; i++)
		{
			Map<K, V> pool = data[i];
			synchronized (pool)
			{
				result = pool.get(key);
				if (result != null)
				{
					if (cloner == null)
					{
						return(result);
					}
					else
					{
						V duplicate = cloner.cloneBean(result);
						return(duplicate);
					}
				}
			}
		}

		return(null);
	}

	public int size()
	{
		int count = 0;

		for (int i = 0; i < data.length; i++)
		{
			Map<K, V> pool = data[i];
			synchronized (pool)
			{
				count = count + pool.size();
			}
		}

		return(count);
	}
	
	public static void enableCacheTracker(long printPeriod)
	{
		cacheTracker=new WeakHashMap<String, QueueCache<?,?>>();
		
		TimerTask tt=new TimerTask()
		{
			@Override
			public void run()
			{
				printCacheStats();
			}
		};
		
		timer.schedule(tt, printPeriod, printPeriod);
	}
	
	public static void printCacheStats()
	{
		for (Map.Entry<String, QueueCache<?,?>> entry : cacheTracker.entrySet())
		{
			logger.info("QueueCache : " + entry.getKey()+" : "+entry.getValue().size());
		}
	}
}