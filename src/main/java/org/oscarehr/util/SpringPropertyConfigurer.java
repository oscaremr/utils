/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.w3c.dom.Node;

/**
 * This class takes a config.xml file and makes a hashmap of category.property=value
 * and sets it as a spring PropertyPlaceholderConfigurer so all the config.xml properties
 * are available in your spring.xml files as long as you do :
 * <bean class="org.oscarehr.util.SpringPropertyConfigurer" />
 */
public class SpringPropertyConfigurer extends PropertyPlaceholderConfigurer
{
	private static Logger logger = MiscUtils.getLogger();

	public SpringPropertyConfigurer()
	{
		HashMap<String, HashMap<String, Object>> configMap = ConfigXmlUtils.getConfig();

		Properties p = new Properties();

		for (String category : configMap.keySet())
		{
			HashMap<String, Object> categoryMap = configMap.get(category);

			for (String key : categoryMap.keySet())
			{
				Object value = categoryMap.get(key);

				// single entry adverse to list
				if (value instanceof Node)
				{
					String categoryKey = category + '.' + key;
					String valueString = ((Node) value).getTextContent();
					p.put(categoryKey, valueString);
					logger.debug("Setting " + categoryKey + '=' + valueString);
				}
			}
		}

		setProperties(p);
	}
}