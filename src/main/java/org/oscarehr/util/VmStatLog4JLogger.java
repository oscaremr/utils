/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.util.HashMap;

import org.apache.log4j.Logger;

public final class VmStatLog4JLogger implements VmStatDataLoggerInterface
{
	private static Logger logger = MiscUtils.getLogger();

	private HashMap<String, Long> previousCollectionCount = new HashMap<String, Long>();
	private HashMap<String, Long> previousCollectionTime = new HashMap<String, Long>();

	@Override
	public void logThreadCounts(int threadCount, int deamonThreadCount, int peakThreadCount)
	{
		logger.info("Threads : count=" + threadCount + ", deamonThreadCount=" + deamonThreadCount + ", peakThreadCount=" + peakThreadCount);
	}

	@Override
	public void logCpuUsage(int cpus, double loadAverage)
	{
		double queued = loadAverage - cpus;
		String status = (queued >= 0 ? "BLOCKED" : "UNUSED");
		logger.info("CPU : count=" + cpus + ", loadAverage=" + loadAverage + ", status=" + status + ':' + queued);
	}

	@Override
	public void logMemInfo(String poolName, long max, long used)
	{
		logger.info("Memory : pool=" + poolName + ", max=" + max + ", used=" + used + ", % used =" + (used / max));
	}

	@Override
	public void logGcInfo(String collectorName, long totalCollectionCount, long totalCollectionTimeMs)
	{
		Long prevCount = previousCollectionCount.get(collectorName);
		if (prevCount == null) prevCount = 0L;

		Long prevTime = previousCollectionTime.get(collectorName);
		if (prevTime == null) prevTime = 0L;

		long countDiff = totalCollectionCount - prevCount;
		long timeDiff = totalCollectionTimeMs - prevTime;

		logger.info("GC : collectorName=" + collectorName + ", totalCollectionCount=" + totalCollectionCount + ", totalCollectionTimeMs=" + totalCollectionTimeMs + ", totalAvgTimePerCollection=" + (totalCollectionTimeMs / Math.max(1, totalCollectionCount)));
		logger.info("GC : collectorName=" + collectorName + ", countDiff=" + countDiff + ", timeDiff=" + timeDiff + ", diffAvgTimerPerCollection=" + (timeDiff / Math.max(1, countDiff)));

		previousCollectionCount.put(collectorName, totalCollectionCount);
		previousCollectionTime.put(collectorName, totalCollectionTimeMs);
	}
}
