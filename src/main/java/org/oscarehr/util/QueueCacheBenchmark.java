/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.WeakHashMap;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;
import net.sf.ehcache.config.PersistenceConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration.Strategy;

/**
 * This class is meant to show some gauge of how the overhead of using the QueueCache compares to 
 * using a HashMap or WeakHashMap. Note this only measures the insertion and removal of entries, it does not
 * take into account the cache hit/miss ratio. Generally speaking it would appear HashMap is always quickest
 * followed by WeakHashMap then this QueueCache. The QueueCache however has cache guarantees like size and time.
 * 
 * Note the results displayed here are in _nano_ seconds so even if you see a large number it's probably really really small.
 */
public class QueueCacheBenchmark
{
	public static class QueueCacheTestValue implements QueueCacheValueCloner<QueueCacheTestValue>
	{
		private String name;
		private Integer value;

		public QueueCacheTestValue()
		{
			// allow null constructor
		}

		public QueueCacheTestValue(Integer value)
		{
			this.name = "name" + value;
			this.value = value;
		}

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public Integer getValue()
		{
			return(value);
		}

		public void setValue(Integer value)
		{
			this.value = value;
		}

		@Override
		public QueueCacheTestValue cloneBean(QueueCacheTestValue original)
		{
			return(new QueueCacheTestValue(original.getValue()));
		}
	}

	public static class TestThreadMap extends Thread
	{
		private Map<Integer, QueueCacheTestValue> map;
		private int sampleSize;
		private int distribution;

		public TestThreadMap(Map<Integer, QueueCacheTestValue> map, int distribution, int sampleSize)
		{
			this.map = map;
			this.sampleSize = sampleSize;
			this.distribution = distribution;
		}

		@Override
		public void run()
		{
			Random rand = new Random();

			for (int i = 0; i < sampleSize; i++)
			{
				Integer randomNumber = rand.nextInt(distribution);
				QueueCacheTestValue value = map.get(randomNumber);

				if (value == null) map.put(randomNumber, new QueueCacheTestValue(randomNumber));
			}
		}
	}

	public static class TestThreadQueue extends Thread
	{
		private QueueCache<Integer, QueueCacheTestValue> queueCache;
		private int sampleSize;
		private int distribution;

		public TestThreadQueue(QueueCache<Integer, QueueCacheTestValue> queueCache, int distribution, int sampleSize)
		{
			this.queueCache = queueCache;
			this.sampleSize = sampleSize;
			this.distribution = distribution;
		}

		@Override
		public void run()
		{
			Random rand = new Random();

			for (int i = 0; i < sampleSize; i++)
			{
				Integer randomNumber = rand.nextInt(distribution);
				QueueCacheTestValue value = queueCache.get(randomNumber);

				if (value == null) queueCache.put(randomNumber, new QueueCacheTestValue(randomNumber));
			}
		}
	}

	public static class TestThreadEhcache extends Thread
	{
		private Cache ehCache;
		private int sampleSize;
		private int distribution;

		public TestThreadEhcache(Cache ehCache, int distribution, int sampleSize)
		{
			this.ehCache = ehCache;
			this.sampleSize = sampleSize;
			this.distribution = distribution;
		}

		@Override
		public void run()
		{
			Random rand = new Random();

			for (int i = 0; i < sampleSize; i++)
			{
				Integer randomNumber = rand.nextInt(distribution);

				Element value = ehCache.get(randomNumber);

				if (value == null)
				{
					value = new Element(randomNumber, new QueueCacheTestValue(randomNumber));
					ehCache.put(value);
				}
			}
		}
	}

	public static void main(String... argv) throws InterruptedException
	{
		final int THREADS = 32;
		final int SAMPLE_SIZE = 100000;
		final int DISTRIBUTION = 8000;
		final int MAX_SIZE = 2000;
		final int QUEUE_POOLS = 4;
		final int MAX_TIME = 10000;

		Map<Integer, QueueCacheTestValue> weakHashMap = Collections.synchronizedMap(new WeakHashMap<Integer, QueueCacheTestValue>());
		Map<Integer, QueueCacheTestValue> hashMap = Collections.synchronizedMap(new HashMap<Integer, QueueCacheTestValue>());
		QueueCache<Integer, QueueCacheTestValue> queueCacheShared = new QueueCache<Integer, QueueCacheTestValue>("test1", QUEUE_POOLS, MAX_SIZE, MAX_TIME, null);
		QueueCache<Integer, QueueCacheTestValue> queueCacheCloned = new QueueCache<Integer, QueueCacheTestValue>("test2", QUEUE_POOLS, MAX_SIZE, MAX_TIME, new QueueCacheTestValue());

		//--- ehcache ---
		Configuration defaultManagerConfig = ConfigurationFactory.parseConfiguration();
		CacheConfiguration defaultCacheConfig = defaultManagerConfig.getDefaultCacheConfiguration();
		defaultCacheConfig.setEternal(false);
		defaultCacheConfig.setMaxEntriesLocalHeap(MAX_SIZE);
		defaultCacheConfig.setTimeToLiveSeconds(MAX_TIME);

		PersistenceConfiguration persistenceConfiguration = new PersistenceConfiguration();
		persistenceConfiguration.strategy(Strategy.NONE);
		defaultCacheConfig.persistence(persistenceConfiguration);

		CacheManager ehcacheManager = CacheManager.create(defaultManagerConfig);
		CacheConfiguration cacheConfiguration = new CacheConfiguration("test ehcache", MAX_SIZE);
		cacheConfiguration.setEternal(false);
		cacheConfiguration.setMaxEntriesLocalHeap(MAX_SIZE);
		cacheConfiguration.setTimeToLiveSeconds(MAX_TIME);
		Cache ehcache = new Cache(cacheConfiguration);

		ehcacheManager.addCache(ehcache);

		// warm up
		spawnMapThreads(true, THREADS, DISTRIBUTION, 1000, weakHashMap);
		spawnMapThreads(true, THREADS, DISTRIBUTION, 1000, hashMap);
		spawnQueueThreads(true, THREADS, DISTRIBUTION, 1000, queueCacheShared);
		spawnQueueThreads(true, THREADS, DISTRIBUTION, 1000, queueCacheCloned);
		spawnEhcacheThreads(true, THREADS, DISTRIBUTION, 1000, ehcache);

		// real
		spawnMapThreads(false, THREADS, DISTRIBUTION, SAMPLE_SIZE, weakHashMap);
		spawnMapThreads(false, THREADS, DISTRIBUTION, SAMPLE_SIZE, hashMap);
		spawnQueueThreads(false, THREADS, DISTRIBUTION, SAMPLE_SIZE, queueCacheShared);
		spawnQueueThreads(false, THREADS, DISTRIBUTION, SAMPLE_SIZE, queueCacheCloned);
		spawnEhcacheThreads(false, THREADS, DISTRIBUTION, SAMPLE_SIZE, ehcache);

		// see queue cleanup
		final int PRINT_PERIOD = MAX_TIME / QUEUE_POOLS / 2;
		for (int i = 0; i < (MAX_TIME / PRINT_PERIOD) + 1; i++)
		{
			System.err.println("shrinking queueCacheShared.size=" + queueCacheShared.size() + " : " + Arrays.toString(queueCacheShared.getPoolSizes()));
			System.err.println("shrinking queueCacheCloned.size=" + queueCacheCloned.size() + " : " + Arrays.toString(queueCacheCloned.getPoolSizes()));
			System.err.println("shrinking ehcache.size=" + ehcache.getSize());
			Thread.sleep(PRINT_PERIOD);
		}

	}

	private static void spawnMapThreads(boolean warmup, int threadCount, int distribution, int sampleSize, Map<Integer, QueueCacheTestValue> map) throws InterruptedException
	{
		ArrayList<Thread> threads = new ArrayList<Thread>();

		long startTime = System.nanoTime();

		for (int i = 0; i < threadCount; i++)
		{
			Thread t = new TestThreadMap(map, distribution, sampleSize);
			t.start();
			threads.add(t);
		}

		for (Thread thread : threads)
		{
			thread.join();
		}

		long totalTime = System.nanoTime() - startTime;
		double avgTime = ((double) totalTime / sampleSize);

		if (!warmup) System.err.println("Map Test Time avg : " + avgTime + "ns");
	}

	private static void spawnQueueThreads(boolean warmup, int threadCount, int distribution, int sampleSize, QueueCache<Integer, QueueCacheTestValue> queueCache) throws InterruptedException
	{
		ArrayList<Thread> threads = new ArrayList<Thread>();

		long startTime = System.nanoTime();

		for (int i = 0; i < threadCount; i++)
		{
			Thread t = new TestThreadQueue(queueCache, distribution, sampleSize);
			t.start();
			threads.add(t);
		}

		for (Thread thread : threads)
		{
			thread.join();
		}

		long totalTime = System.nanoTime() - startTime;
		double avgTime = ((double) totalTime / sampleSize);

		if (!warmup) System.err.println("QueueCache Test Time avg : " + avgTime + "ns");
	}

	private static void spawnEhcacheThreads(boolean warmup, int threadCount, int distribution, int sampleSize, Cache ehcache) throws InterruptedException
	{
		ArrayList<Thread> threads = new ArrayList<Thread>();

		long startTime = System.nanoTime();

		for (int i = 0; i < threadCount; i++)
		{
			Thread t = new TestThreadEhcache(ehcache, distribution, sampleSize);
			t.start();
			threads.add(t);
		}

		for (Thread thread : threads)
		{
			thread.join();
		}

		long totalTime = System.nanoTime() - startTime;
		double avgTime = ((double) totalTime / sampleSize);

		if (!warmup) System.err.println("Ehcache Test Time avg : " + avgTime + "ns");
	}
}
