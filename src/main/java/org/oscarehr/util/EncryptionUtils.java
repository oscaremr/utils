/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * This utility class is a convenience class for 1 off encryption / decryptions of data.
 * If you're doing bulk encryption/decryption you should do it manually as this is not as efficient because it'll create
 * new objects for every method call.
 */
public final class EncryptionUtils
{
	public static class RsaKeyData
	{
		public BigInteger modulus;
		public BigInteger exponent;
	}
	
	/**
	 * @param hashType = MD5 | SHA-1 | SHA-256
	 */
	public static byte[] getSha(String hashType, byte[] data) throws NoSuchAlgorithmException
	{
		MessageDigest messageDigest = MessageDigest.getInstance(hashType);
		return(messageDigest.digest(data));
	}

	public static byte[] getSha(String hashType, String data) throws UnsupportedEncodingException, NoSuchAlgorithmException
	{
		byte[] b = null;
		if (data != null) b = data.getBytes(MiscUtils.DEFAULT_UTF8_ENCODING);
		return(getSha(hashType, b));
	}

	/**
	 * @param bits = 128 only in default java, if you have added security packages you might be able to do 192 | 256
	 */
	public static SecretKey generateAesEncryptionKey(int bits) throws NoSuchAlgorithmException
	{
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(bits);

		SecretKey secretKey = keyGenerator.generateKey();
		return(secretKey);
	}

	/**
	 * @param bits = 128 only in default java, if you have added security packages you might be able to do 192 | 256
	 * @param hashType see getSha()
	 */
	public static SecretKeySpec generateAesEncryptionKey(String seed, String hashType, int bits) throws UnsupportedEncodingException, NoSuchAlgorithmException
	{
		byte[] sha1 = getSha(hashType, seed);

		SecretKeySpec secretKey = new SecretKeySpec(sha1, 0, bits / 8, "AES");
		return(secretKey);
	}

	/**
	 * This method will re-materialise a SecretKeySpec from encodingBytes.
	 * To get the encoding bytes / serialise an AES key just call originalKey.getEncoded();
	 * This is suitable for serialising / deserialising AES keys for storage like to files or databases etc.
	 */
	public static SecretKeySpec deserialiseAesEncryptionKey(byte[] encodedBytes)
	{
		SecretKeySpec deserialisedKey = new SecretKeySpec(encodedBytes, "AES");
		return(deserialisedKey);
	}
	
	/**
	 * @param bits = 2048, you can also use 1024 but really... why
	 */
	public static KeyPair generateRsaKeyPair(int bits) throws NoSuchAlgorithmException
	{
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(bits);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		return(keyPair);
	}

	/**
	 * @return object containing the RSA key data so you can serialise the key to a file / database etc.
	 */
	public static RsaKeyData serialiseRsaPublicKeyData(RSAPublicKey rsaKey)
	{
		RsaKeyData rsaKeyData=new RsaKeyData();
		rsaKeyData.modulus=rsaKey.getModulus();
		rsaKeyData.exponent=rsaKey.getPublicExponent();
		return(rsaKeyData);
	}

	/**
	 * @return Rsa key from serialised data
	 */
	public static RSAPublicKey deserialiseRsaPublicKey(RsaKeyData rsaKeyData) throws InvalidKeySpecException, NoSuchAlgorithmException
	{
		RSAPublicKeySpec deserialisedPublicKeySpec = new RSAPublicKeySpec(rsaKeyData.modulus, rsaKeyData.exponent);
		RSAPublicKey deserialisedPublicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(deserialisedPublicKeySpec);
		return(deserialisedPublicKey);
	}
	
	
	/**
	 * @return object containing the RSA key data so you can serialise the key to a file / database etc.
	 */
	public static RsaKeyData serialiseRsaPrivateKeyData(RSAPrivateKey rsaKey)
	{
		RsaKeyData rsaKeyData=new RsaKeyData();
		rsaKeyData.modulus=rsaKey.getModulus();
		rsaKeyData.exponent=rsaKey.getPrivateExponent();
		return(rsaKeyData);
	}

	/**
	 * @return Rsa key from serialised data
	 */
	public static RSAPrivateKey deserialiseRsaPrivateKey(RsaKeyData rsaKeyData) throws InvalidKeySpecException, NoSuchAlgorithmException
	{
		RSAPrivateKeySpec deserialisedPrivateKeySpec = new RSAPrivateKeySpec(rsaKeyData.modulus, rsaKeyData.exponent);
		RSAPrivateKey deserialisedPrivateKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(deserialisedPrivateKeySpec);
		return(deserialisedPrivateKey);
	}

	/**
	 * @param algorithm = AES | RSA
	 * @param cipherMode = Cipher.ENCRYPT_MODE | Cipher.DECRYPT_MODE
	 * @param key = AES SecretKey | RSA PublicKey/PrivateKey, if null is passed in then the original data is returned, i.e. no-op.
	 * @param data = the unencrypted data or encrypted data
	 */
	public static byte[] encryptDecrypt(String algorithm, int cipherMode, Key key, byte[] data) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException
	{
		if (key == null) return(data);

		Cipher cipher = Cipher.getInstance(algorithm);
		cipher.init(cipherMode, key);
		byte[] results = cipher.doFinal(data);
		return(results);
	}

	public static byte[] encryptAes(SecretKey secretKey, byte[] plainData) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
	{
		return(encryptDecrypt("AES", Cipher.ENCRYPT_MODE, secretKey, plainData));
	}

	public static byte[] encryptAes(SecretKey secretKey, String plainData) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException
	{
		byte[] b = null;
		if (plainData != null) b = plainData.getBytes(MiscUtils.DEFAULT_UTF8_ENCODING);
		return(encryptAes(secretKey, b));
	}

	public static byte[] decryptAes(SecretKey secretKey, byte[] encryptedData) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{
		return(encryptDecrypt("AES", Cipher.DECRYPT_MODE, secretKey, encryptedData));
	}

	public static String decryptAesToString(SecretKey secretKey, byte[] encryptedData) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException
	{
		byte[] b = decryptAes(secretKey, encryptedData);
		return(new String(b, MiscUtils.DEFAULT_UTF8_ENCODING));
	}

	public static byte[] encryptRsa(PublicKey publicKey, byte[] plainData) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException
	{
		return(encryptDecrypt("RSA", Cipher.ENCRYPT_MODE, publicKey, plainData));
	}

	public static byte[] encryptRsa(PublicKey publicKey, String plainData) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException
	{
		byte[] b = null;
		if (plainData != null) b = plainData.getBytes(MiscUtils.DEFAULT_UTF8_ENCODING);
		return(encryptRsa(publicKey, b));
	}

	public static byte[] decryptRsa(PrivateKey privateKey, byte[] encryptedData) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
	{
		return(encryptDecrypt("RSA", Cipher.DECRYPT_MODE, privateKey, encryptedData));
	}

	public static String decryptRsaToString(PrivateKey privateKey, byte[] encryptedData) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException
	{
		byte[] b = decryptRsa(privateKey, encryptedData);
		return(new String(b, MiscUtils.DEFAULT_UTF8_ENCODING));
	}
	
	public static void main(String... argv) throws Exception
	{
		String originalString = "The brown cow jumped over the halfpipe.";

		//--- sample sha ---
		byte[] sha1Data = getSha("SHA-1", originalString);
		System.err.println("sha-1 : " + new String(sha1Data));

		byte[] sha256Data = getSha("SHA-256", originalString);
		System.err.println("sha-256 : " + new String(sha256Data));

		//--- sample AES ---
		SecretKey aesKey = generateAesEncryptionKey(128);
		byte[] aesEncryptedData = encryptAes(aesKey, originalString);
		System.err.println("aes : " + new String(aesEncryptedData));

		String aesDecryptedData = decryptAesToString(aesKey, aesEncryptedData);
		System.err.println("aes : " + aesDecryptedData);

		//--- sample RSA ---
		KeyPair keyPair = generateRsaKeyPair(2048);
		byte[] rsaEncryptedData = encryptRsa(keyPair.getPublic(), originalString);
		System.err.println("rsa : " + new String(rsaEncryptedData));

		String rsaDecryptedData = decryptRsaToString(keyPair.getPrivate(), rsaEncryptedData);
		System.err.println("rsa : " + rsaDecryptedData);
	}
}
