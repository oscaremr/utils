/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
/*
* Copyright (c) 2001-2002. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
* This software is published under the GPL GNU General Public License. 
* This program is free software; you can redistribute it and/or 
* modify it under the terms of the GNU General Public License 
* as published by the Free Software Foundation; either version 2 
* of the License, or (at your option) any later version. * 
* This program is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License 
* along with this program; if not, write to the Free Software 
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
* 
* <OSCAR TEAM>
* 
* This software was written for 
* Centre for Research on Inner City Health, St. Michael's Hospital, 
* Toronto, Ontario, Canada 
*/

package org.oscarehr.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public final class XmlUtils
{
	private static Logger logger = LogManager.getLogger(XmlUtils.class);

	public static void setLsSeriliserToFormatted(LSSerializer lsSerializer)
	{
		lsSerializer.getDomConfig().setParameter("format-pretty-print", true);
	}

	public static void writeNode(Node node, OutputStream os, boolean formatted) throws ClassCastException, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		DOMImplementationRegistry domImplementationRegistry = DOMImplementationRegistry.newInstance();
		DOMImplementationLS domImplementationLS = (DOMImplementationLS) domImplementationRegistry.getDOMImplementation("LS");

		LSOutput lsOutput = domImplementationLS.createLSOutput();
		lsOutput.setEncoding(MiscUtils.DEFAULT_UTF8_ENCODING);
		lsOutput.setByteStream(os);

		LSSerializer lsSerializer = domImplementationLS.createLSSerializer();
		if (formatted) setLsSeriliserToFormatted(lsSerializer);
		lsSerializer.write(node, lsOutput);
	}

	public static byte[] toBytes(Node node, boolean formatted) throws ClassCastException, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		writeNode(node, baos, formatted);
		return (baos.toByteArray());
	}

	public static String toString(Node node, boolean formatted) throws ClassCastException, ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		writeNode(node, baos, formatted);
		return (baos.toString());
	}

	public static Document toDocumentFromFile(String url) throws ParserConfigurationException, SAXException, IOException
	{
		try (InputStream is = XmlUtils.class.getResourceAsStream(url))
		{
			if (is != null) return (XmlUtils.toDocument(is));
		}

		try (FileInputStream fis = new FileInputStream(url))
		{
			return (XmlUtils.toDocument(fis));
		}
	}

	public static Document toDocument(String s) throws IOException, org.xml.sax.SAXException, ParserConfigurationException
	{
		return (toDocument(s.getBytes(MiscUtils.DEFAULT_UTF8_ENCODING)));
	}

	public static Document toDocument(byte[] x) throws IOException, org.xml.sax.SAXException, ParserConfigurationException
	{
		ByteArrayInputStream is = new ByteArrayInputStream(x, 0, x.length);
		return (toDocument(is));
	}

	public static Document toDocument(InputStream is) throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(is);
		return (document);
	}

	public static Document newDocument(String rootName) throws ParserConfigurationException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();

		doc.appendChild(doc.createElement(rootName));

		return (doc);
	}

	public static void appendChildToRoot(Document doc, String childName, byte[] childContents)
	{
		appendChild(doc, doc.getFirstChild(), childName, new String(Base64.encodeBase64(childContents)));
	}

	public static void appendChildToRoot(Document doc, String childName, String childContents)
	{
		appendChild(doc, doc.getFirstChild(), childName, childContents);
	}

	public static void appendChildToRootIgnoreNull(Document doc, String childName, String childContents)
	{
		if (childContents == null) return;
		appendChildToRoot(doc, childName, childContents);
	}

	public static void appendChild(Document doc, Node parentNode, String childName, String childContents)
	{
		if (childContents == null) throw (new NullPointerException("ChildNode is null."));

		Element child = doc.createElement(childName);
		child.setTextContent(childContents);
		parentNode.appendChild(child);
	}

	public static void replaceChild(Document doc, Node parentNode, String childName, String childContents)
	{
		Node node = getChildNode(parentNode, childName);

		// if some one passes in null
		if (childContents == null)
		{
			// remove existing node
			if (node != null) parentNode.removeChild(node);

			return;
		}

		// this means there's at least contents pass in,
		if (node == null)
		{
			// no existing node, so we just add one
			appendChild(doc, parentNode, childName, childContents);
		}
		else
		{
			// existing node so we update it instead.
			node.setTextContent(childContents);
		}
	}

	/**
	 * only returns the first instance of this child node
	 */
	public static Node getChildNode(Node node, String name)
	{
		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			Node temp = nodeList.item(i);
			if (temp.getNodeType() != Node.ELEMENT_NODE) continue;
			if (name.equals(temp.getLocalName()) || name.equals(temp.getNodeName())) return (temp);
		}

		return (null);
	}

	/**
	 * @return a list of all child nodes with this name
	 */
	public static ArrayList<Node> getChildNodes(Node node, String name)
	{
		ArrayList<Node> results = new ArrayList<Node>();

		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			Node temp = nodeList.item(i);
			if (temp.getNodeType() != Node.ELEMENT_NODE) continue;
			if (name.equals(temp.getLocalName()) || name.equals(temp.getNodeName()))
			{
				results.add(temp);
			}
		}

		return (results);
	}

	public static String getChildNodeTextContents(Node node, String name)
	{
		Node tempNode = getChildNode(node, name);
		if (tempNode != null) return (tempNode.getTextContent());
		else return (null);
	}

	public static Long getChildNodeLongContents(Node node, String name)
	{
		String s = getChildNodeTextContents(node, name);
		if (s != null) return (new Long(s));
		else return (null);
	}

	public static Integer getChildNodeIntegerContents(Node node, String name)
	{
		String s = getChildNodeTextContents(node, name);
		if (s != null) return (new Integer(s));
		else return (null);
	}

	public static Boolean getChildNodeBooleanContents(Node node, String name)
	{
		String s = getChildNodeTextContents(node, name);
		if (s != null) return (Boolean.valueOf(s));
		else return (null);
	}

	/**
	 * @return a list of all child node text contents with this name
	 */
	public static ArrayList<String> getChildNodesTextContents(Node node, String name)
	{
		ArrayList<String> results = new ArrayList<String>();

		NodeList nodeList = node.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			Node temp = nodeList.item(i);
			if (temp.getNodeType() != Node.ELEMENT_NODE) continue;
			if (name.equals(temp.getLocalName()) || name.equals(temp.getNodeName()))
			{
				results.add(temp.getTextContent());
			}
		}

		return (results);
	}

	/**
	 * removes any immediate child nodes with the given name.
	 */
	public static void removeAllChildNodes(Node node, String name)
	{
		// this must be done as a 2 pass algorithm. 
		// attempt at doing it at single pass fails because the list is 
		// being altered as we read it which leads to errors.
		ArrayList<Node> removeList = getChildNodes(node, name);

		for (Node temp : removeList)
		{
			node.removeChild(temp);
		}
	}

	/**
	 * @return the attribute value or null
	 */
	public static String getAttributeValue(Node node, String attributeName)
	{
		NamedNodeMap attributes = node.getAttributes();
		if (attributes == null) return (null);

		Node tempNode = attributes.getNamedItem(attributeName);
		if (tempNode == null) return (null);

		return (tempNode.getNodeValue());
	}

	public static <V extends Object> Document toXml(Map<String, V> map) throws ParserConfigurationException
	{
		Document doc = newDocument("XmlMap");
		Node rootNode = doc.getFirstChild();

		for (Map.Entry<String, V> mapEntry : map.entrySet())
		{
			Element xmlEntry = doc.createElement("entry");
			rootNode.appendChild(xmlEntry);

			Element xmlKey = doc.createElement("key");
			xmlKey.setTextContent(mapEntry.getKey());
			xmlEntry.appendChild(xmlKey);

			Element xmlValue = doc.createElement("value");
			xmlValue.setTextContent(mapEntry.getValue().toString());
			xmlEntry.appendChild(xmlValue);

			if (!String.class.equals(mapEntry.getValue().getClass()))
			{
				Element xmlValueType = doc.createElement("valueType");
				xmlValueType.setTextContent(mapEntry.getValue().getClass().getName());
				xmlEntry.appendChild(xmlValueType);
			}
		}

		return (doc);
	}

	/**
	 * This will get the childNodes of the rootNode as a map.
	 * The purpose of this is to allow fast access to the xml elements with out 
	 * having to iterate through the xml over and over for every child node.
	 * 
	 * If the element only contains text, the value will be the text as a String,
	 * otherwise it will return the Node with the key= that name.
	 * 
	 * So as an example :
	 * <foo>
	 *     <first>one</first>
	 *     <second>two</second>
	 *     <three>
	 *         <three.1>three point 1</three.1>
	 *         <three.2>three point 2</three.2>
	 *     </three>
	 * </foo>
	 * 
	 * will return a map = {second=two, three=[three node], first=one}, 
	 * where [three node] = 
	 * <three>
	 *     <three.1>three point 1</three.1>
	 *     <three.2>three point 2</three.2>
	 * </three> 
	 */
	public static HashMap<String, Object> getFirstLevelEntriesAsMap(Document doc)
	{
		HashMap<String, Object> result = new HashMap<String, Object>();

		Node rootNode = doc.getFirstChild();
		NodeList nodeList = rootNode.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++)
		{
			Node temp = nodeList.item(i);
			if (temp.getNodeType() != Node.ELEMENT_NODE) continue;

			NodeList tempChildNodes = temp.getChildNodes();
			// this will only add the node if there is 1 child and it's a text entry
			if (tempChildNodes.getLength() == 1)
			{
				Node firstTempChild = tempChildNodes.item(0);
				if (firstTempChild.getNodeType() == Node.TEXT_NODE)
				{
					result.put(temp.getNodeName(), firstTempChild.getTextContent());
					continue;
				}
			}

			// this will catch if it's either a non text child or if there's multiple childs 
			if (tempChildNodes.getLength() >= 1)
			{
				result.put(temp.getNodeName(), temp);
			}
		}

		return (result);
	}

	public static HashMap<String, Object> toMap(Document doc)
	{
		HashMap<String, Object> result = new HashMap<String, Object>();

		copyToMap(doc, result);

		return (result);
	}

	public static void copyToMap(Document doc, Map<String, Object> map)
	{
		Node rootNode = doc.getFirstChild();
		ArrayList<Node> entries = getChildNodes(rootNode, "entry");
		for (Node node : entries)
		{
			String key = getChildNodeTextContents(node, "key");
			String value = getChildNodeTextContents(node, "value");
			String valueType = getChildNodeTextContents(node, "valueType");

			if (valueType == null) map.put(key, value);
			else if (Boolean.class.getName().equals(valueType)) map.put(key, Boolean.valueOf(value));
			else if (Integer.class.getName().equals(valueType)) map.put(key, Integer.valueOf(value));
			else if (Long.class.getName().equals(valueType)) map.put(key, Long.valueOf(value));
			else if (Float.class.getName().equals(valueType)) map.put(key, Float.valueOf(value));
			else logger.error("Missed type key/value/type=" + key + '/' + value + '/' + valueType);
		}
	}
}
