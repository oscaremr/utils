/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util.db;

import java.util.Properties;

import org.apache.derby.authentication.UserAuthenticator;
import org.apache.log4j.Logger;
import org.oscarehr.util.ConfigXmlUtils;
import org.oscarehr.util.MiscUtils;

/**
 * This class uses ConfigXmlUtils to controls some configuration parameters, they are all defaulted if not specified.
 * category=database, property=derby_server_user
 * category=database, property=derby_server_password
 * 
 * To use this class you should add the following to your derby.properties
 * derby.connection.requireAuthentication=true
 * derby.authentication.provider=org.oscarehr.util.db.DerbySimpleAuthenticator
 */
public class DerbySimpleAuthenticator implements UserAuthenticator
{
	private static Logger logger = MiscUtils.getLogger();

	private String requiredUserName = ConfigXmlUtils.getPropertyString("database", "derby_server_user", "dbuser");
	private String requiredPassword = ConfigXmlUtils.getPropertyString("database", "derby_server_password", "dbpassword");

	@Override
	public boolean authenticateUser(String userName, String userPassword, String databaseName, Properties info)
	{
		boolean result = requiredUserName.equals(userName) && requiredPassword.equals(userPassword);

		if (logger.isDebugEnabled())
		{
			logger.debug("req:" + requiredUserName + '/' + requiredPassword + ", provided:" + userName + '/' + userPassword + ", result=" + result);
			if (result == false) logger.debug("Call Location", new Exception());
		}

		return(result);
	}
}
