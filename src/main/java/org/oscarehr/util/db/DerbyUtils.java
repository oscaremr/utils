/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util.db;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.derby.drda.NetworkServerControl;

public final class DerbyUtils
{
	/**
	 * Assume use of a directory called "derby" off the jvm current working directory.
	 */
	public static final String DERBY_SYSTEM_HOME = "derby";

	public static void setDerbyHome()
	{
		setDerbyHome(DERBY_SYSTEM_HOME);
	}

	public static void setDerbyHome(String derbyHome)
	{
		Properties properties = System.getProperties();
		properties.put("derby.system.home", derbyHome);
	}

	public static void createDatabase(String dbName, String user, String password) throws SQLException
	{
		try (Connection c = DriverManager.getConnection("jdbc:derby:" + dbName + ";create=true", user, password))
		{
			// nothing to do
		}
	}

	public static void dropDatabase(String dbName, String user, String password) throws IOException
	{
		// derby has no real drop database commmand, just stop the db, then remove the files.
		try
		{
			stopDerby(dbName, user, password);
		}
		catch (SQLException e)
		{
			// that's okay if it doesn't exist
		}

		File file = new File(DERBY_SYSTEM_HOME + '/' + dbName);
		if (file.exists())
		{
			FileUtils.deleteDirectory(file);
		}
	}

	/**
	 * Used for when upgrading derby versions.
	 */
	public static void upgradeDatabase(String dbName, String user, String password) throws SQLException
	{
		try (Connection c = DriverManager.getConnection("jdbc:derby:" + dbName + ";upgrade=true", user, password))
		{
			// nothing to do
		}

	}

	public static void startDerby(boolean enableNetworkServer, String schema, String user, String password, boolean quietOnNonExistantSchema) throws Exception
	{
		NetworkServerControl server = null;
		if (enableNetworkServer)
		{
			server = new NetworkServerControl(InetAddress.getByName("127.0.0.1"), 1527, user, password);
			server.start(null);
		}

		try (Connection c = getEmbeddedConnection(schema, user, password))
		{
			// nothing to do
		}
		catch (SQLException e)
		{
			if (!quietOnNonExistantSchema) throw(e);
		}
	}

	/**
	 * server can be null to stop embedded server only. 
	 */
	public static void stopDerby(boolean networkServer, String user, String password) throws Exception
	{
		stopEmbeddedDerby(user, password);

		if (networkServer)
		{
			NetworkServerControl server = new NetworkServerControl(InetAddress.getByName("127.0.0.1"), 1527, user, password);
			server.shutdown();
		}
	}

	/**
	 * Will stop all derby db's
	 */
	public static void stopEmbeddedDerby(String user, String password) throws SQLException
	{
		stopDerby("", user, password);
	}

	/**
	 * Will stop a single derby db instance
	 */
	public static void stopDerby(String dbName, String user, String password) throws SQLException
	{
		try (Connection c = DriverManager.getConnection("jdbc:derby:" + dbName + ";shutdown=true", user, password))
		{
			// nothing to do
		}
		catch (SQLException e)
		{
			if (e.getSQLState().equals("XJ015"))
			{
				// that's okay, that means success
			}
			else
			{
				throw(e);
			}
		}
	}

	/**
	 * make sure you finally-close this connection, this is a non-pooled call directory to DriverManager.
	 */
	public static Connection getEmbeddedConnection(String dbName, String user, String password) throws SQLException
	{
		return(DriverManager.getConnection("jdbc:derby:" + dbName, user, password));
	}

	public static void runSqlScript(Connection c, String fileName) throws IOException, SQLException
	{
		ClassLoader classLoader = DerbyUtils.class.getClassLoader();
		BufferedReader br = null;
		try
		{
			Statement s = c.createStatement();

			br = new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(fileName)));

			String temp = null;
			while ((temp = br.readLine()) != null)
			{
				temp = StringUtils.trimToNull(temp);
				if (temp == null) continue;

				// create tables adds ; to end of lines, make sure we don't have those when calling the command in jdbc
				if (temp.charAt(temp.length() - 1) == ';') temp = temp.substring(0, temp.length() - 1);

				s.executeUpdate(temp);
			}
		}
		finally
		{
			if (br != null) br.close();
		}
	}
}
