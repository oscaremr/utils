/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util.db;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotEnabledException;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.FilterList.Operator;
import org.apache.hadoop.hbase.filter.FirstKeyOnlyFilter;
import org.apache.hadoop.hbase.filter.KeyOnlyFilter;
import org.apache.hadoop.hbase.io.compress.Compression.Algorithm;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.oscarehr.util.ConfigXmlUtils;

/**
 * This class represents a connection to 1 HBase instance. It's a singleton class.
 * To use this class, call initialise from your startup listeners contextInitialized and call close from your contextDestroyed
 * 
 * This class uses ConfigXmlUtils to controls some configuration parameters, they are all defaulted if not specified.
 * category=hbase, property=hbase.zookeeper.quorum
 * category=hbase, property=connectionPoolSize
 * category=hbase, property=connectionTimeOutMs
 * category=hbase, property=retryPauseMs
 * category=hbase, property=retryCount
 */
public final class HBaseUtils
{
	private static Logger logger = LogManager.getLogger(HBaseUtils.class);

	public static final byte[] COLUMN_FAMILY_NAME_BYTES = Bytes.toBytes("cf");
	public static final char COLUMN_GROUP_SEPARATOR = ':';
	public static final byte[] SEQUENCE_COUNTER_COLUMN_NAME_BYTES = Bytes.toBytes("Counter");
	public static final int LONG_BYTE_LENGTH = Bytes.toBytes(Long.MAX_VALUE).length;

	public static final String ID_COLUMN_NAME = "id";
	public static final byte[] ID_COLUMN_NAME_BYTES = Bytes.toBytes(ID_COLUMN_NAME);
	public static final String ACTIVE_COLUMN_NAME = "active";
	public static final byte[] ACTIVE_COLUMN_NAME_BYTES = Bytes.toBytes(ACTIVE_COLUMN_NAME);
	public static final byte[] TRUE_BYTES = Bytes.toBytes(true);
	public static final byte[] FALSE_BYTES = Bytes.toBytes(false);
	public static final byte[] ZERO_BYTE = { 0 };

	private static Configuration configuration = null;
	private static ExecutorService connectionFactoryExecutor = null;
	private static Connection connection = null;

	public static synchronized void initialise() throws IOException
	{
		if (connection != null) throw (new IllegalStateException("already initalised"));

		String zookeeperQuorumServersHostCsv = ConfigXmlUtils.getPropertyString("hbase", "hbase.zookeeper.quorum", "127.0.0.1");
		int poolSize = ConfigXmlUtils.getPropertyInt("hbase", "connectionPoolSize", 4);
		int connectionTimeOutMs = ConfigXmlUtils.getPropertyInt("hbase", "connectionTimeOutMs", 5000);
		int hBaseRetryPauseMs = ConfigXmlUtils.getPropertyInt("hbase", "hBaseRetryPauseMs", 200);
		int hBaseRetryCount = ConfigXmlUtils.getPropertyInt("hbase", "hBaseRetryCount", 10);
		int maxKeyValueSize = ConfigXmlUtils.getPropertyInt("hbase", "maxKeyValueSize", 1024 * 1024 * 50);
		int zooKeeperRetryPauseMs = ConfigXmlUtils.getPropertyInt("hbase", "hBaseRetryPauseMs", 200);
		int zooKeeperRetryCount = ConfigXmlUtils.getPropertyInt("hbase", "hBaseRetryCount", 5);

		logger.info("hbase.poolSize=" + poolSize);
		logger.info("hbase.connectionTimeOutMs=" + connectionTimeOutMs);
		logger.info("hbase.hBaseRetryPauseMs=" + hBaseRetryPauseMs);
		logger.info("hbase.hBaseRetryCount=" + hBaseRetryCount);
		logger.info("hbase.maxKeyValueSize=" + maxKeyValueSize);
		logger.info("hbase.zooKeeperRetryPauseMs=" + zooKeeperRetryPauseMs);
		logger.info("hbase.zooKeeperRetryCount=" + zooKeeperRetryCount);

		configuration = HBaseConfiguration.create();
		configuration.set("hbase.zookeeper.quorum", zookeeperQuorumServersHostCsv);
		configuration.set("hbase.zookeeper.watcher.sync.connected.wait", String.valueOf(connectionTimeOutMs));
		configuration.set("hbase.client.pause", String.valueOf(hBaseRetryPauseMs));
		configuration.set("hbase.client.retries.number", String.valueOf(hBaseRetryCount));
		configuration.set("hbase.client.keyvalue.maxsize", String.valueOf(maxKeyValueSize));
		configuration.set("zookeeper.recovery.retry.intervalmill", String.valueOf(zooKeeperRetryPauseMs));
		configuration.set("zookeeper.recovery.retry", String.valueOf(zooKeeperRetryCount));

		connectionFactoryExecutor = Executors.newFixedThreadPool(poolSize);

		connection = ConnectionFactory.createConnection(configuration, connectionFactoryExecutor);
	}

	public static synchronized void close() throws IOException
	{
		if (connection != null)
		{
			connection.close();
			connection = null;
		}

		if (connectionFactoryExecutor!=null && !connectionFactoryExecutor.isShutdown())
		{
			connectionFactoryExecutor.shutdown();
			connectionFactoryExecutor = null;
		}

		configuration = null;
	}

	public static Configuration getConfiguration()
	{
		return (configuration);
	}

	public static Admin getHBaseAdmin() throws IOException
	{
		if (connection == null) initialise();

		return (connection.getAdmin());
	}

	/**
	 * @param compressionAlgorithm can be null for no compression
	 * @return true if table was created, false otherwise
	 */
	public static boolean createTable(Admin admin, byte[] tableName, boolean useRowKeyBloomFilters, Algorithm compressionAlgorithm, Integer ttlSec, boolean ifNotExists)
	{
		HColumnDescriptor family = makeDefaultColumnDescriptor(useRowKeyBloomFilters, compressionAlgorithm, ttlSec);

		return (createTable(admin, family, tableName, ifNotExists));
	}

	public static boolean createTable(Admin admin, HColumnDescriptor family, byte[] tableName, boolean ifNotExists)
	{
		try
		{
			TableName tableNameObject=TableName.valueOf(tableName);
			
			if (ifNotExists && admin.tableExists(tableNameObject)) return (false);

			HTableDescriptor hTableDescriptor = new HTableDescriptor(tableNameObject);
			hTableDescriptor.addFamily(family);

			admin.createTable(hTableDescriptor);

			return (true);
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static HColumnDescriptor makeDefaultColumnDescriptor(boolean useRowKeyBloomFilters, Algorithm compressionAlgorithm, Integer ttlSeconds)
	{
		HColumnDescriptor family = new HColumnDescriptor(COLUMN_FAMILY_NAME_BYTES);
		family.setCacheDataOnWrite(true);

		if (ttlSeconds != null) family.setTimeToLive(ttlSeconds);

		if (useRowKeyBloomFilters)
		{
			family.setCacheBloomsOnWrite(true);
			family.setBloomFilterType(BloomType.ROW);
		}

		if (compressionAlgorithm != null) family.setCompressionType(compressionAlgorithm);

		return (family);
	}

	/**
	 * @param quiet means no exception is thrown if table does not exist.
	 */
	public static void disableAndDropTable(Admin admin, byte[] tableName, boolean quiet) throws TableNotFoundException, TableNotEnabledException
	{
		try
		{
			admin.disableTable(TableName.valueOf(tableName));
		}
		catch (TableNotFoundException e)
		{
			if (!quiet)
			{
				throw (e);
			}
		}
		catch (TableNotEnabledException e)
		{
			if (!quiet)
			{
				throw (e);
			}
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}

		try
		{
			admin.deleteTable(TableName.valueOf(tableName));
		}
		catch (TableNotFoundException e)
		{
			if (!quiet)
			{
				throw (e);
			}
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	/**
	 * You need to finally / close() this HTable instance like you would a jdbc connection.
	 */
	public static Table getTableFromPool(byte[] tableName) throws IOException
	{
		if (connection == null) initialise();

		try
		{
			Table table = connection.getTable(TableName.valueOf(tableName));
			return (table);
		}
		catch (Exception e)
		{
			logger.error("Connection to hbase lost? removing old hconnection", e);
			close();
		}
		return (null);
	}

	public static void put(byte[] tableName, Put put)
	{
		try (Table table = getTableFromPool(tableName))
		{
			table.put(put);
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static boolean checkAndPut(byte[] tableName, byte[] keyBytes, byte[] column, byte[] expectedValue, Put put)
	{
		try (Table table = getTableFromPool(tableName))
		{
			return (table.checkAndPut(keyBytes, COLUMN_FAMILY_NAME_BYTES, column, expectedValue, put));
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static void put(byte[] tableName, List<Put> puts)
	{
		try (Table table = getTableFromPool(tableName))
		{
			table.put(puts);
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static Put makePut(byte[] keyBytes, byte[] columnName, byte[] value)
	{
		Put put = new Put(keyBytes);
		put.addColumn(COLUMN_FAMILY_NAME_BYTES, columnName, value);
		return (put);
	}

	/**
	 * Just a convenience method because scan(start,stop) doesn't work with null entries, this one will.
	 * This will also auto-append zero byte to make start exclusive if you pass in true for that parameter.
	 */
	public static Scan makeScan(byte[] startKey, byte[] endKey, boolean excludeStartKey)
	{
		Scan scan = new Scan();
		if (startKey != null)
		{
			if (excludeStartKey) startKey = lexicographicallyIncrement(startKey);
			scan.setStartRow(startKey);
		}

		if (endKey != null) scan.setStopRow(endKey);

		return (scan);
	}

	/**
	 * return null if no data found
	 */
	public static byte[] get(byte[] tableName, byte[] key, byte[] columnName)
	{
		try (Table table = getTableFromPool(tableName))
		{
			Get get = new Get(key);
			get.addColumn(COLUMN_FAMILY_NAME_BYTES, columnName);

			Result result = table.get(get);
			byte[] b = result.getValue(COLUMN_FAMILY_NAME_BYTES, columnName);
			return (b);
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static long incrementSequenceCounter(byte[] sequenceCounterTableName, byte[] counterName)
	{
		return (incrementColumnValue(sequenceCounterTableName, counterName, SEQUENCE_COUNTER_COLUMN_NAME_BYTES, 1));
	}

	public static long incrementColumnValue(byte[] tableName, byte[] row, byte[] columnName, long amount)
	{
		try (Table table = getTableFromPool(tableName))
		{
			long result = table.incrementColumnValue(row, COLUMN_FAMILY_NAME_BYTES, columnName, amount);
			return (result);
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static void delete(byte[] tableName, Delete delete)
	{
		try (Table table = getTableFromPool(tableName))
		{
			table.delete(delete);
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static long getRowCount(byte[] tableName)
	{
		Scan scan = new Scan();
		scan.setCacheBlocks(false);
		scan.setFilter(getKeyOnlyFilter());

		return (getRowCount(tableName, scan));
	}

	/**
	 * You should pass in a scan that's restricted to the columns you need or it'll be inefficient
	 */
	public static long getRowCount(byte[] tableName, Scan scan)
	{
		try (Table table = getTableFromPool(tableName); ResultScanner rs = table.getScanner(scan))
		{
			long counter = 0;
			while (rs.next() != null)
			{
				counter++;
			}

			return (counter);
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static void deleteEntireRow(byte[] tableName, byte[] key)
	{
		delete(tableName, new Delete(key));
	}

	public static long getReverseTime()
	{
		return (Long.MAX_VALUE - System.currentTimeMillis());
	}

	public static long getReverseTime(long time)
	{
		return (Long.MAX_VALUE - time);
	}

	public static long getUnReverseTime(long reversedTime)
	{
		return (Long.MAX_VALUE - reversedTime);
	}

	public static byte[] appendReverseTime(byte[] originalKey)
	{
		return (Bytes.add(originalKey, Bytes.toBytes(getReverseTime())));
	}

	/**
	 * Also known as get all bytes excluding last 8.
	 */
	public static byte[] getKeyExcludingRT(byte[] key)
	{
		return (Bytes.head(key, key.length - LONG_BYTE_LENGTH));
	}

	/**
	 * Also known as get last 8 bytes
	 */
	public static byte[] getRTFromKey(byte[] key)
	{
		return (Bytes.tail(key, LONG_BYTE_LENGTH));
	}

	public static long getFirstBytesAsLong(byte[] key)
	{
		return (Bytes.toLong(key, 0, LONG_BYTE_LENGTH));
	}

	public static long getLastBytesAsLong(byte[] key)
	{
		return (Bytes.toLong(key, key.length - LONG_BYTE_LENGTH, LONG_BYTE_LENGTH));
	}

	/**
	 * This method will assume a RT was appended to the byte key.
	 * This will take the last bytes as a RT, then un-reverse it and give you the original time as a calendar.
	 */
	public static GregorianCalendar getLastBytesAsRTCalendar(byte[] key)
	{
		long temp = Bytes.toLong(key, key.length - LONG_BYTE_LENGTH, LONG_BYTE_LENGTH);
		temp = getUnReverseTime(temp);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeInMillis(temp);
		return (cal);
	}

	/**
	 * This method appends a byte to the passed in array. This will guarantee that this is 
	 * the immediately next "possible" key regardless of the length of the key. So as an example this maybe
	 * suitable to pagination on something where the key is of variable size like userName or something
	 * and you're trying to find the next starting point.
	 *
	 * This will not modify the passed in array. The returned array will be larger than the passed in one.
	 */
	public static byte[] lexicographicallyIncrement(byte[] key)
	{
		return (Bytes.add(key, ZERO_BYTE));
	}

	/**
	 * The input should be the byte[] from a long value, this will increment the long byte value by 1 byte value. 
	 * i.e. it will not make the array longer, it will use the Bytes.incrementBytes but will make a clone of the array first.
	 * This method will not modify the passed in value. Note that this can be used to get the next key in pagination ONLY
	 * if a long key is used.
	 * 
	 * This is different than Bytes.increment in that it does not alter the passed in value.
	 */
	public static byte[] incrementLongByte(byte[] key)
	{
		if (key.length != LONG_BYTE_LENGTH) throw (new IllegalArgumentException("The passed in value must be a long byte[]"));

		byte[] b = Arrays.copyOf(key, key.length);
		Bytes.incrementBytes(b, 1);
		return (b);
	}

	/**
	 * This method will increment the byte[] by 1 byte value.
	 * This will not alter the passed in value.
	 * This method is suitable for finding the end point for a scan for fixed length composite keys, i.e. countryCode+userId, this will find
	 * the next immediately possible countryCode bytes, so you can limit the scan to just this one country code.
	 * 
	 * This method will not grow the length of the array, if it is already at the max value it will return unchanged.
	 */
	public static byte[] incrementFixedLengthKey(byte[] key)
	{
		if (allMaxValue(key)) return (key);

		byte[] result = Arrays.copyOf(key, key.length);

		for (int i = result.length - 1; i >= 0; i--)
		{
			byte b = result[i];
			// -1 is 0xff is 255, Byte.MAX_VALUE is only 0x7f i.e. 127 
			if (b == -1)
			{
				result[i] = 0;
			}
			else
			{
				result[i] = (byte) (b + 1);
				return (result);
			}
		}

		return (result);
	}

	private static boolean allMaxValue(byte[] key)
	{
		for (byte b : key)
		{
			if (b != Byte.MAX_VALUE) return (false);
		}

		return (true);
	}

	/**
	 * if object is null return a byte[0]
	 */
	public static byte[] getBytes(Object o)
	{
		if (o == null) return (new byte[0]);

		if (o instanceof Boolean) return (Bytes.toBytes((Boolean) o));
		else if (o instanceof byte[]) return ((byte[]) o);
		else if (o instanceof Double) return (Bytes.toBytes((Double) o));
		else if (o instanceof Float) return (Bytes.toBytes((Float) o));
		else if (o instanceof Integer) return (Bytes.toBytes((Integer) o));
		else if (o instanceof Long) return (Bytes.toBytes((Long) o));
		else if (o instanceof Short) return (Bytes.toBytes((Short) o));
		else if (o instanceof Byte) return (Bytes.toBytes((Byte) o));
		else if (o instanceof String) return (Bytes.toBytes((String) o));
		else if (o instanceof Calendar) return (Bytes.toBytes(((Calendar) o).getTimeInMillis()));
		else if (o instanceof Enum) return (Bytes.toBytes(((Enum<?>) o).name()));
		else throw (new IllegalArgumentException("unsupported type : " + o.getClass().getName()));
	}

	public static void logInfo(String label, Result result)
	{
		List<Cell> cells = result.listCells();
		for (Cell cell : cells)
		{
			logger.info(label + " : key=" + new String(result.getRow()) + ", col=" + new String(cell.getFamilyArray(), cell.getFamilyOffset(), cell.getFamilyLength()) + "." + new String(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength()) + ", val=" + new String(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength()));
		}
	}

	/**
	 * Add this filter for scans which you only want the key
	 */
	public static FilterList getKeyOnlyFilter()
	{
		FilterList filterList = new FilterList(Operator.MUST_PASS_ALL);
		filterList.addFilter(new FirstKeyOnlyFilter());
		filterList.addFilter(new KeyOnlyFilter());
		return (filterList);
	}

	/**
	 * This method is only really useful for junit testing.
	 * It manually iterates through the rows deleting all entries.
	 * This is faster than dropping and creating a table
	 * if you have few rows (like in most junit tests)
	 */
	public static void deleteAllRowsQuietly(byte[] tableName)
	{
		Scan scan = new Scan();
		scan.setFilter(getKeyOnlyFilter());

		try (Table table = HBaseUtils.getTableFromPool(tableName); ResultScanner rs = table.getScanner(scan))
		{
			Result result;
			while ((result = rs.next()) != null)
			{
				deleteEntireRow(tableName, result.getRow());
			}
		}
		catch (Exception e)
		{
			logger.debug("Error, or might just be table doesn't exist", e);
		}
	}

	/**
	 * Null safe, will return null. Bytes should represent a long representing time in ms.
	 */
	public static GregorianCalendar materialiseCalender(byte[] bytes)
	{
		if (bytes == null) return (null);

		long ms = Bytes.toLong(bytes);
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeInMillis(ms);
		return (cal);
	}
}