/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * A unique index is expected to have a main table which would be like any normal table, along with an Index table which should be a single column table where the index value is the key and the ID pointing back to the main table is the value.
 * An example is an Account table where one of the columns is userName and you want to index on userName. The expectation is that another index table exists
 * where the key=userName and value=accountId.
 * <br /><br />
 * A non-unique index is expected to have a main table along with an Index table but in the index table they key will be a composite key of the value+id and the columns and column values are not significant.
 * An example is an Account table where country is one of the columns and you want to be able to retrieve users by country.
 * There would be an index table, with key=countryCode+accountId. A scan on the country code will provide you with all accountIds. 
 * <br /><br />
 * An association is when you have 2 normal tables both of which have their own primary keys and you want to associated 
 * items from one table to another. 
 * An example is Account and Groups, an account may belong to many groups and a group may have many accounts. Basically
 * this is just creating and maintaining 2 non-unique indexes in both directions.
 * <br /><br />
 * You should not mix uses of index tables, each index table should represent one usage pattern only. Bad things will happen otherwise, you've been warned.
 * <br /><br />
 * Your keys for non-unique entries must be fixed length or bad things will happen.
 */
public final class HBaseIndexUtils
{
	public static final String INDEX_TABLE_COLUMN_NAME = "i";
	public static final byte[] INDEX_TABLE_COLUMN_NAME_BYTES = Bytes.toBytes(INDEX_TABLE_COLUMN_NAME);
	public static final byte[] NOT_SIGNIFICANT_DATA_PLACE_HOLDER = new byte[0];

	public static void addUniqueIndexEntry(byte[] indexTableName, byte[] indexKey, byte[] columnValue) throws DuplicateUniqueKeyException
	{
		if (columnValue == null) return;

		Put put = new Put(indexKey);
		put.addColumn(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, INDEX_TABLE_COLUMN_NAME_BYTES, columnValue);
		boolean added = HBaseUtils.checkAndPut(indexTableName, indexKey, INDEX_TABLE_COLUMN_NAME_BYTES, null, put);
		if (!added) throw (new DuplicateUniqueKeyException("duplicate entry : " + Bytes.toStringBinary(indexKey)));
	}

	/**
	 * There are 2 parts to resynching indexes, you must make sure entries in the index exists for entries in
	 * the main table, and you must make sure entires in the index don't exist if there are no entries
	 * in the main table. This method only takes care of the first part.
	 * 
	 * Most people should not be calling this method, this method only adds index entries for the table entries.
	 * This method assumes unique values in the table.
	 * 
	 * @param mainTableStartKey is where to start scanning from in case you left off some where, can be null for beginning.
	 * @param sleepAndRecheckMs should be <=0 for no sleep/recheck
	 */
	public static void syncIndexFromTable(byte[] mainTableName, byte[] mainColumnName, byte[] mainTableStartKey, byte[] indexTableName, long sleepAndRecheckMs) throws IOException, InterruptedException
	{
		Scan scan = null;
		if (mainTableStartKey == null) scan = new Scan();
		else scan = new Scan(mainTableStartKey);

		scan.addColumn(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, mainColumnName);

		try (Table mainTable = HBaseUtils.getTableFromPool(mainTableName); ResultScanner rs = mainTable.getScanner(scan))
		{
			for (Result result : rs)
			{
				byte[] mainKey = result.getRow();
				byte[] mainValue = result.getValue(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, mainColumnName);

				syncSingleIndexValue(indexTableName, mainValue, mainKey, sleepAndRecheckMs);
			}
		}
	}

	/**
	 * @param sleepAndRecheckMs should be <=0 for no sleep/recheck
	 */
	private static void syncSingleIndexValue(byte[] indexTableName, byte[] indexKey, byte[] indexValue, long sleepAndRecheckMs) throws IOException, InterruptedException
	{
		byte[] currentIndexValue = HBaseUtils.get(indexTableName, indexKey, INDEX_TABLE_COLUMN_NAME_BYTES);

		// check if things are in sync
		if (Bytes.equals(indexValue, currentIndexValue)) return;

		// so at this point we know they're out of sync but we don't know in what manner.

		// sleep and retry all to mitigate collisions
		if (sleepAndRecheckMs > 0)
		{
			Thread.sleep(sleepAndRecheckMs);
			syncSingleIndexValue(indexTableName, indexKey, currentIndexValue, 0);
			return;
		}

		// at this point we have to deal with it

		// if it should be null then delete entry
		if (indexValue == null)
		{
			HBaseUtils.deleteEntireRow(indexTableName, indexKey);
			return;
		}

		// if it shouldn't be null then we can just set it, for both change and create - don't care which case
		Put put = HBaseUtils.makePut(indexKey, INDEX_TABLE_COLUMN_NAME_BYTES, indexValue);
		HBaseUtils.put(indexTableName, put);
	}

	/**
	 * There are 2 parts to resynching indexes, you must make sure entries in the index exists for entries in
	 * the main table, and you must make sure entires in the index don't exist if there are no entries
	 * in the main table. This method only takes care of the second part.
	 * 
	 * Most people should not be calling this method, this method only removes index entries
	 * where there is no entry in the main table. This method assumes unique values in the table.
	 * 
	 * @param indexStartKey is where to start scanning from in case you left off some where, can be null for beginning.
	 * @param sleepAndRecheckMs should be <=0 for no sleep/recheck
	 */
	public static void cleanIndexFromTable(byte[] mainTableName, byte[] mainColumnName, byte[] indexTableName, byte[] indexStartKey, long sleepAndRecheckMs) throws IOException, InterruptedException
	{
		Scan scan = null;
		if (indexStartKey == null) scan = new Scan();
		else scan = new Scan(indexStartKey);

		scan.addColumn(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, INDEX_TABLE_COLUMN_NAME_BYTES);

		try (Table indexTable = HBaseUtils.getTableFromPool(indexTableName); ResultScanner rs = indexTable.getScanner(scan))
		{
			for (Result result : rs)
			{
				byte[] indexKey = result.getRow();
				byte[] indexValue = result.getValue(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, mainColumnName);

				cleanSingleIndexValue(mainTableName, mainColumnName, indexTableName, indexKey, indexValue, sleepAndRecheckMs);
			}
		}
	}

	/**
	 * @param sleepAndRecheckMs should be <=0 for no sleep/recheck 
	 */
	private static void cleanSingleIndexValue(byte[] mainTableName, byte[] mainColumnName, byte[] indexTableName, byte[] indexKey, byte[] indexValue, long sleepAndRecheckMs) throws InterruptedException
	{
		byte[] currentMainValue = HBaseUtils.get(mainTableName, indexValue, mainColumnName);

		// both are equal, then it's ok
		if (Bytes.equals(indexValue, currentMainValue)) return;

		// so at this point we know they're out of sync but we don't know in what manner.

		// sleep and retry all to mitigate collisions
		if (sleepAndRecheckMs > 0)
		{
			Thread.sleep(sleepAndRecheckMs);
			cleanSingleIndexValue(mainTableName, mainColumnName, indexTableName, indexKey, indexValue, 0);
			return;
		}

		// at this point we have to deal with it

		// if this index points to the wrong entry delete it
		if (currentMainValue == null)
		{
			HBaseUtils.deleteEntireRow(indexTableName, indexKey);
			return;
		}
	}

	/**
	 * @param fixedLengthSearchKey in our example of Accounts and country, country would be the search criteria, and accountId would be the associatedId
	 */
	public static void addNonUniqueIndexEntry(byte[] indexTableName, byte[] fixedLengthSearchKey, byte[] associatedId)
	{
		byte[] compositeKeyBytes = Bytes.add(fixedLengthSearchKey, associatedId);
		Put put = HBaseUtils.makePut(compositeKeyBytes, INDEX_TABLE_COLUMN_NAME_BYTES, NOT_SIGNIFICANT_DATA_PLACE_HOLDER);
		HBaseUtils.put(indexTableName, put);
	}

	/**
	 * @param fixedLengthSearchKey in our example of Accounts and country, country would be the fixedLengthSearchKey, and accountId would be the associatedId
	 */
	public static void deleteNonUniqueIndexEntry(byte[] indexTableName, byte[] fixedLengthSearchKey, byte[] associatedId)
	{
		byte[] compositeKeyBytes = Bytes.add(fixedLengthSearchKey, associatedId);
		HBaseUtils.deleteEntireRow(indexTableName, compositeKeyBytes);
	}

	public static boolean existsNonUniqueIndexEntry(byte[] indexTableName, byte[] fixedLengthSearchKey, byte[] associatedId)
	{
		byte[] compositeKeyBytes = Bytes.add(fixedLengthSearchKey, associatedId);
		byte[] result = HBaseUtils.get(indexTableName, compositeKeyBytes, INDEX_TABLE_COLUMN_NAME_BYTES);
		return (result != null);
	}

	public static void addAssociationEntry(byte[] indexTableName1, byte[] indexTableName2, byte[] key1, byte[] key2)
	{
		addNonUniqueIndexEntry(indexTableName1, key1, key2);
		addNonUniqueIndexEntry(indexTableName2, key2, key1);
	}

	public static void deleteAssociationEntry(byte[] indexTableName1, byte[] indexTableName2, byte[] key1, byte[] key2)
	{
		deleteNonUniqueIndexEntry(indexTableName1, key1, key2);
		deleteNonUniqueIndexEntry(indexTableName2, key2, key1);
	}

	/**
	 * @param startResultKey can be null to start from the beginning
	 * 
	 * @return a list of associated keys. So in our Account/Country example the fixedLengthSearchKey=au, and the results=List of accountIds.
	 */
	public static List<byte[]> scanNonUniqueIndex(byte[] indexTableName, byte[] fixedLengthSearchKey, byte[] startResultKey, int itemsToReturn, boolean excludeStartKey)
	{
		ArrayList<byte[]> results = new ArrayList<byte[]>();

		byte[] startKey = fixedLengthSearchKey;
		if (startResultKey != null) startKey = Bytes.add(startKey, startResultKey);
		byte[] endKey = HBaseUtils.incrementFixedLengthKey(fixedLengthSearchKey);

		Scan scan = HBaseUtils.makeScan(startKey, endKey, excludeStartKey);

		int keyLength = fixedLengthSearchKey.length;

		try (Table table = HBaseUtils.getTableFromPool(indexTableName); ResultScanner rs = table.getScanner(scan))
		{
			for (Result result : rs)
			{
				byte[] rowKeyBytes = result.getRow();
				byte[] resultId = Bytes.tail(rowKeyBytes, rowKeyBytes.length - keyLength);

				results.add(resultId);

				if (results.size() >= itemsToReturn) break;
			}
		}
		catch (IOException e)
		{
			throw (new HBaseUtilsException(e));
		}

		return (results);
	}

}
