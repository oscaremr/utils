/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util.db;

import java.io.Serializable;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;

public abstract class AbstractHBaseModel implements Serializable
{
	/**
	 * This logger should never be anything but private, sub classes should make their own logger or items will be logged to the wrong logger. 
	 */
	private static Logger logger = MiscUtils.getLogger();

	protected static final String OBJECT_NOT_YET_PERISTED = "The object is not persisted yet, this operation requires the object to already be persisted.";

	private byte[] id;
	private Calendar createDate=new GregorianCalendar();
	private Calendar updateDate=createDate;

	public String getIdAsHexString()
	{
		return(Bytes.toStringBinary(getId()));
	}

	public byte[] getId()
	{
		return(id);
	}

	public void setId(byte[] id)
	{
		this.id = id;
	}

	public Calendar getCreateDate()
	{
		return(createDate);
	}

	public void setCreateDate(Calendar createDate)
	{
		this.createDate = createDate;
	}

	public Calendar getUpdateDate()
	{
		return(updateDate);
	}

	public void setUpdateDate(Calendar updateDate)
	{
		this.updateDate = updateDate;
	}

	@Override
	public String toString()
	{
		try
		{
			Method[] methods = getClass().getDeclaredMethods();
			AccessibleObject.setAccessible(methods, true);

			StringBuilder sb = new StringBuilder();

			for (Method method : methods)
			{
				String methodName = method.getName();

				if (method.getParameterTypes().length == 0 && methodName.startsWith("get"))
				{
					Object o = method.invoke(this);
					if (sb.length() > 0) sb.append(", ");
					sb.append(methodName);
					sb.append('=');

					if (o instanceof Calendar) sb.append(((Calendar) o).getTime());
					if (o instanceof byte[]) sb.append(Bytes.toStringBinary((byte[]) o));
					else sb.append(o);
				}
			}

			return(sb.toString());
		}
		catch (Exception e)
		{
			logger.error("Unexpected Error", e);
			return(null);
		}
	}

	@Override
	public int hashCode()
	{
		if (getId() == null)
		{
			logger.warn(OBJECT_NOT_YET_PERISTED, new Exception());
			return(super.hashCode());
		}

		return(getId().hashCode());
	}

	@Override
	public boolean equals(Object o)
	{
		if (getClass() != o.getClass()) return(false);

		AbstractHBaseModel abstractModel = (AbstractHBaseModel) o;
		if (getId() == null)
		{
			logger.warn(OBJECT_NOT_YET_PERISTED, new Exception());
		}

		return(getId().equals(abstractModel.getId()));
	}
	
	public static boolean isDataFieldSame(Object o1, Object o2)
	{
		if (o1==null && o2==null) return(true);
		if (o1==null || o2==null) return(false);
		return(o1.equals(o2));
	}
	
	public static boolean isDataFieldSame(byte[] b1, byte[] b2)
	{
		return(Bytes.equals(b1, b2));
	}

	public static boolean isDataFieldSame(Calendar c1, Calendar c2)
	{
		if (c1==null && c2==null) return(true);
		if (c1==null || c2==null) return(false);		
		return(c1.getTimeInMillis()==c2.getTimeInMillis());
	}

	public static boolean isDataFieldSame(Enum<?> e1, Enum<?> e2)
	{
		if (e1==null && e2==null) return(true);
		if (e1==null || e2==null) return(false);		
		return(e1==e2);
	}
}