/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util.db;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.RetriesExhaustedException;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;

/**
 * All these utilities assume the id and active columns match the defined constants in HBaseUtils. It also assume the default column family. 
 */
public final class HBaseModelMapper
{
	private static Logger logger = MiscUtils.getLogger();

	private static HashMap<Class<?>, ArrayList<Field>> fieldsToMapCache = new HashMap<Class<?>, ArrayList<Field>>();

	public static ArrayList<Field> getFieldsToMap(Class<?> c)
	{
		ArrayList<Field> results = fieldsToMapCache.get(c);

		if (results == null)
		{
			results = new ArrayList<Field>();

			Class<?> tempClass = c;
			do
			{
				addFields(tempClass, results);
				tempClass = tempClass.getSuperclass();
			}
			while (!Object.class.equals(tempClass));

			// we don't want to synchronise the map so we'll swap it
			synchronized (HBaseModelMapper.class)
			{
				HashMap<Class<?>, ArrayList<Field>> newMap = new HashMap<Class<?>, ArrayList<Field>>();
				newMap.putAll(fieldsToMapCache);
				newMap.put(c, results);
				fieldsToMapCache = newMap;
			}
		}

		return (results);
	}

	/**
	 * add to the put fields which are not final/static/transient, null is mapped to an empty array.
	 */
	private static void addFields(Class<?> c, ArrayList<Field> results)
	{
		Field[] fields = c.getDeclaredFields();
		AccessibleObject.setAccessible(fields, true);

		for (Field field : fields)
		{
			int modifiers = field.getModifiers();
			if (Modifier.isFinal(modifiers) || Modifier.isStatic(modifiers) || Modifier.isTransient(modifiers))
			{
				logger.debug("skip (modifier) : " + field.getType() + " : " + field.getName());
				continue;
			}

			if (HBaseUtils.ID_COLUMN_NAME.equals(field.getName()))
			{
				logger.debug("skip put (id) : " + field.getType() + " : " + field.getName());
				continue;
			}

			results.add(field);
		}
	}

	/**
	 * Populate a put and delete based on if there's values or nulls in the model object fields.
	 * Put and Delete can be null to ignore those operations.
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static void populatePutDelete(Put put, Delete delete, AbstractHBaseModel model) throws IllegalAccessException
	{
		ArrayList<Field> fields = getFieldsToMap(model.getClass());
		for (Field field : fields)
		{
			String fieldName = field.getName();
			byte[] fieldNameBytes = Bytes.toBytes(fieldName);

			Object value = field.get(model);
			if (value == null)
			{
				if (delete != null)
				{
					delete.addColumn(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, fieldNameBytes);
				}
			}
			else
			{
				if (put != null)
				{
					byte[] valueBytes = HBaseUtils.getBytes(value);
					put.addColumn(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, fieldNameBytes, valueBytes);
				}
			}
		}
	}

	public static void persist(AbstractHBaseModel model, byte[] tableName, byte[] rowKey) throws IllegalAccessException
	{
		try
		{
			Put put = new Put(rowKey);
			populatePutDelete(put, null, model);
			HBaseUtils.put(tableName, put);
		}
		catch (IllegalArgumentException e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	public static void put(AbstractHBaseModel model, byte[] tableName, byte[] rowKey, boolean setUpdateTime) throws IllegalAccessException
	{
		try
		{
			if (setUpdateTime) model.setUpdateDate(new GregorianCalendar());

			Put put = new Put(rowKey);
			Delete delete = new Delete(rowKey);
			populatePutDelete(put, delete, model);
			if (delete.size() > 0) HBaseUtils.delete(tableName, delete);
			HBaseUtils.put(tableName, put);
		}
		catch (IllegalArgumentException e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	/**
	 * Get and Scan can be null if you don't need it
	 */
	public static void populateGetScan(Get get, Scan scan, Class<?> c)
	{
		ArrayList<Field> fields = getFieldsToMap(c);
		for (Field field : fields)
		{
			String fieldName = field.getName();
			byte[] fieldNameBytes = Bytes.toBytes(fieldName);

			if (logger.isDebugEnabled())
			{
				logger.debug("fieldName=" + fieldName);
			}

			if (get != null) get.addColumn(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, fieldNameBytes);
			if (scan != null) scan.addColumn(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, fieldNameBytes);
		}
	}

	private static void materialiseFields(AbstractHBaseModel model, Result result) throws IllegalAccessException
	{
		if (result.isEmpty()) throw (new IllegalArgumentException("result is empty"));

		ArrayList<Field> fields = getFieldsToMap(model.getClass());

		for (Field field : fields)
		{
			String fieldName = field.getName();
			byte[] fieldNameBytes = Bytes.toBytes(fieldName);
			byte[] valueBytes = result.getValue(HBaseUtils.COLUMN_FAMILY_NAME_BYTES, fieldNameBytes);

			Class<?> fieldClass = field.getType();

			// column never existed
			if (valueBytes == null)
			{
				if (!fieldClass.isPrimitive()) field.set(model, null);
			}
			else
			{
				if (boolean.class.equals(fieldClass)) field.setBoolean(model, Bytes.toBoolean(valueBytes));
				else if (Boolean.class.equals(fieldClass)) field.set(model, Bytes.toBoolean(valueBytes));
				else if (byte[].class.equals(fieldClass)) field.set(model, valueBytes);
				else if (ByteBuffer.class.equals(fieldClass)) field.set(model, valueBytes);
				else if (double.class.equals(fieldClass)) field.setDouble(model, Bytes.toDouble(valueBytes));
				else if (Double.class.equals(fieldClass)) field.set(model, Bytes.toDouble(valueBytes));
				else if (float.class.equals(fieldClass)) field.setFloat(model, Bytes.toFloat(valueBytes));
				else if (Float.class.equals(fieldClass)) field.set(model, Bytes.toFloat(valueBytes));
				else if (int.class.equals(fieldClass)) field.setInt(model, Bytes.toInt(valueBytes));
				else if (Integer.class.equals(fieldClass)) field.set(model, Bytes.toInt(valueBytes));
				else if (long.class.equals(fieldClass)) field.setLong(model, Bytes.toLong(valueBytes));
				else if (Long.class.equals(fieldClass)) field.set(model, Bytes.toLong(valueBytes));
				else if (short.class.equals(fieldClass)) field.setShort(model, Bytes.toShort(valueBytes));
				else if (Short.class.equals(fieldClass)) field.set(model, Bytes.toShort(valueBytes));
				else if (byte.class.equals(fieldClass)) field.setByte(model, valueBytes[0]);
				else if (Byte.class.equals(fieldClass)) field.set(model, valueBytes[0]);
				else if (String.class.equals(fieldClass)) field.set(model, Bytes.toString(valueBytes));
				else if (Calendar.class.isAssignableFrom(fieldClass)) field.set(model, HBaseUtils.materialiseCalender(valueBytes));
				else if (Enum.class.isAssignableFrom(fieldClass))
				{
					@SuppressWarnings({ "rawtypes", "unchecked" })
					Enum e = Enum.valueOf((Class<Enum>) fieldClass, Bytes.toString(valueBytes));
					field.set(model, e);
				}
				else throw (new IllegalArgumentException("unsupported type : " + fieldClass.getName()));
			}
		}
	}

	/**
	 * @return null on no such item
	 */
	public static <T extends AbstractHBaseModel> T find(Class<T> c, byte[] tableName, byte[] rowKey)
	{
		if (rowKey == null) return (null);

		try (Table table = HBaseUtils.getTableFromPool(tableName);)
		{
			Get get = new Get(rowKey);
			populateGetScan(get, null, c);

			Result result = table.get(get);
			if (result.isEmpty()) return (null);

			T instance = c.newInstance();
			materialiseFields(instance, result);
			instance.setId(rowKey);
			return (instance);
		}
		catch (RetriesExhaustedException e)
		{
			if (logger.isDebugEnabled()) logger.debug("No such item? tableName:" + Bytes.toString(tableName) + ", key=" + Bytes.toStringBinary(rowKey), e);
			return (null);
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}
	}

	/**
	 * intended to get multiple rows adverse to just a single row.
	 * 
	 * typical usage would be 
	 * 
	 * Scan scan = new Scan(startKey);
	 *	HBaseModelMapper.populateGetScan(null, scan, MyClass.class);
	 * List<MyClass> results = HBaseModelMapper.findMultiResults(MyClass.class, MYCLASS_TABLE_NAME_BYTES, scan, itemsToReturn);
	 */
	public static <T extends AbstractHBaseModel> ArrayList<T> findMultiResults(Class<T> c, byte[] tableName, Scan scan, int itemsToReturn)
	{
		ArrayList<T> results = new ArrayList<T>();

		try (Table table = HBaseUtils.getTableFromPool(tableName); ResultScanner rs = table.getScanner(scan))
		{
			for (Result result : rs)
			{
				if (result.isEmpty()) continue;

				byte[] rowKeyBytes = result.getRow();

				T instance = c.newInstance();
				materialiseFields(instance, result);
				instance.setId(rowKeyBytes);
				results.add(instance);

				if (results.size() >= itemsToReturn) break;
			}
		}
		catch (Exception e)
		{
			throw (new HBaseUtilsException(e));
		}

		return (results);
	}
}
