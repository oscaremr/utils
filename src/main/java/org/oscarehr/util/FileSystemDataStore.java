/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

/**
 * This is a utility to help you store data onto the files system locally or on to an HDFS file system.
 * The original use case is when you have a database but you're storing large files into it like 20mb video clips etc.
 * You want to move the blob from the DB to the file system. The problem is if you write the file to a directory, you will
 * eventually run out of inodes and other file system limitations, i.e. 65535 files, or impossibly long request times for directory
 * listing or file searches etc. 
 *
 * The presumption is that the file will be written with the name of the database row ID, i.e. a number, helps ensure uniqueness etc.
 *
 * The solution we will use will be to limit the number of entries in a given directory by forming subdirectory tree structure.
 * The problem is how to produce a relatively even distribution between directories and entries.
 * If the tree was only 1 level deep it would be easy - Mod 1024 would break it down into 1024 entries, this means
 * we can support up to 65535*1024 = 67,107,840 ~= 67 million files. That's actually not that many, all things considered. 
 * 
 * If we Mod the number twice say Mod-N and Mod-M, then use that as the first and second directory levels it would give us a greater number.
 * The problem is the distribution will not be even. As an example if N=256 and M=128, we will only end up with 256 buckets as
 * the second mod will always produce the same number as the first mod, i.e. if the first bucket = 232, then the second bucket will always=104, you will
 * never get first bucket=232 and second bucket=1 or 2 or 3 etc... 
 * 
 * The solution is to pick N and M such that they are prime with respect to each other. The easiest way is to do this is to
 * just pick prime numbers out right. If as an example we pick 257 and 251. This means we have a total tree structure of 
 * 257*251=64507 directories, and presuming 65535 entries per directory, 65535*64507=4,227,466,245 ~= 4 billion entries.
 * 
 * So, I'm not happy with 4 billion entries anymore, I'm also not entirely happy with 65535 files in a given
 * directory as the file system really grinds even at 10,000 entries in a directory. My updated solution
 * will be to increase the depth of the tree as well as increasing the actual prime number values. If I pick 503/509/521 then 
 * 503*509*521=133,390,067 directories, if we assume 10,000 files per directory, then 503*509*521*10000=1,333,900,670,000 ~= 1.3 trillion files.
 * I shouldn't have to update this again for a while now :)
 * 
 * It should also be noted that for HDFS we should have fewer files which are larger than a large number of tiny files. As such 
 * we will support sequence files for HDFS. (eventually, and maybe in a separate class since it wouldn't work for normal file systems)
 */
public final class FileSystemDataStore
{
	private static final int FIRST_MOD = 503;
	private static final int SECOND_MOD = 509;
	private static final int THIRD_MOD = 521;

	/**
	 * @return something like /187/53
	 */
	public static StringBuilder getDirectory(long dataId)
	{
		StringBuilder sb = new StringBuilder();
		sb.append('/');
		sb.append(dataId % FIRST_MOD);
		sb.append('/');
		sb.append(dataId % SECOND_MOD);
		sb.append('/');
		sb.append(dataId % THIRD_MOD);
		return(sb);
	}

	/**
	 * @param qualifier can be used like a column name, i.e. if you have a row id=765 and 3 columns "video" and "photo" and "metadata"
	 * @return something like 765.video.data
	 */
	public static StringBuilder getFilename(long dataId, String qualifier)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(dataId);
		sb.append('.');
		sb.append(qualifier);
		sb.append(".data");
		return(sb);
	}

	/**
	 * @return something like /262/256/244/765.video.data
	 */
	public static StringBuilder getDirectoryAndFilename(long dataId, String qualifier)
	{
		StringBuilder sb = getDirectory(dataId);
		sb.append('/');
		sb.append(getFilename(dataId, qualifier));
		return(sb);
	}

	/**
	 * @return full path like /data/mystuff/data/262/256/244 if dataRoot was /data/mystuff/data 
	 */
	public static String ensureDirectoriesExist(String dataRoot, long dataId)
	{
		String fullDirectoryPath = dataRoot + getDirectory(dataId);
		File file = new File(fullDirectoryPath);
		if (!file.exists()) file.mkdirs();
		return(fullDirectoryPath);
	}

	/**
	 * Note in hdfs you don't need to create directories before making a file, this call can often be omitted unless you actually want to make directories.
	 */
	public static String ensureDirectoriesExist(FileSystem fs, String dataRoot, long dataId) throws IOException
	{
		String fullPath = dataRoot + getDirectory(dataId);
		Path path = new Path(fullPath);
		fs.mkdirs(path);
		return(fullPath);
	}

	public static void writeData(String dataRoot, long dataId, String qualifier, byte[] b) throws IOException
	{
		String fullDirectoryPath = ensureDirectoriesExist(dataRoot, dataId);
		File outputFile = new File(fullDirectoryPath + '/' + getFilename(dataId, qualifier));
		FileUtils.writeByteArrayToFile(outputFile, b);
	}

	public static void writeData(FileSystem fs, String dataRoot, long dataId, String qualifier, byte[] b) throws IOException
	{
		String fullPath = dataRoot + getDirectoryAndFilename(dataId, qualifier);
		Path outputPath = new Path(fullPath);
		
		try (FSDataOutputStream outputStream = fs.create(outputPath);)
		{
			IOUtils.write(b, outputStream);
			outputStream.flush();
		}
	}

	public static void writeData(String dataRoot, long dataId, String qualifier, InputStream is) throws IOException
	{
		String fullDirectoryPath = ensureDirectoriesExist(dataRoot, dataId);
		File outputFile = new File(fullDirectoryPath + '/' + getFilename(dataId, qualifier));
		FileUtils.copyInputStreamToFile(is, outputFile);
	}

	public static void writeData(FileSystem fs, String dataRoot, long dataId, String qualifier, InputStream is) throws IOException
	{
		String fullPath = dataRoot + getDirectoryAndFilename(dataId, qualifier);
		Path outputPath = new Path(fullPath);

		try (FSDataOutputStream outputStream = fs.create(outputPath))
		{
			IOUtils.copy(is, outputStream);
			outputStream.flush();
		}
	}

	public static byte[] readData(String dataRoot, long dataId, String qualifier) throws IOException
	{
		String fullPath = dataRoot + getDirectoryAndFilename(dataId, qualifier);
		File inputFile = new File(fullPath);
		return(FileUtils.readFileToByteArray(inputFile));
	}

	public static byte[] readData(FileSystem fs, String dataRoot, long dataId, String qualifier) throws IOException
	{
		String fullPath = dataRoot + getDirectoryAndFilename(dataId, qualifier);
		Path inputPath = new Path(fullPath);

		try (FSDataInputStream inputStream = fs.open(inputPath))
		{
			return(IOUtils.toByteArray(inputStream));
		}
	}

	public static void readData(String dataRoot, long dataId, String qualifier, OutputStream os) throws IOException
	{
		String fullPath = dataRoot + getDirectoryAndFilename(dataId, qualifier);

		try (FileInputStream fis = new FileInputStream(fullPath))
		{
			IOUtils.copy(fis, os);
			os.flush();
		}
	}

	public static void readData(FileSystem fs, String dataRoot, long dataId, String qualifier, OutputStream os) throws IOException
	{
		String fullPath = dataRoot + getDirectoryAndFilename(dataId, qualifier);
		Path inputPath = new Path(fullPath);

		try (FSDataInputStream inputStream = fs.open(inputPath))
		{
			IOUtils.copy(inputStream, os);
			os.flush();
		}
	}

	/**
	 * Allegedly the returned FileSystem object is thread safe.
	 * @param hdfsUri is typically like hdfs://127.0.0.1:9000, it's what ever is in the core-site.xml, fs.default.name entry
	 * @param username is the username to create/read files as on hdfs
	 */
	public static FileSystem getHdfsFileSystem(String hdfsUri, String username) throws IOException, InterruptedException, URISyntaxException
	{
		FileSystem fs = FileSystem.get(new URI(hdfsUri), new Configuration(), username);
		return(fs);
	}

	private static void showProof()
	{
		Logger logger = MiscUtils.getLogger();

		AccumulatorMap<String> accumulator = new AccumulatorMap<String>();
		int multiplicity = 5;
		int totalSamples = FIRST_MOD * SECOND_MOD * THIRD_MOD * multiplicity;

		for (int i = 0; i < totalSamples; i++)
		{
			int firstResult = i % FIRST_MOD;
			int secondResult = i % SECOND_MOD;
			int thirdResult = i % THIRD_MOD;

			accumulator.increment("" + firstResult + '/' + secondResult + '/' + thirdResult);

			if (i % 100000 == 0) logger.error("bucketing : " + i + " / " + totalSamples);
		}

		int counter = 0;
		for (Map.Entry<String, Integer> entry : accumulator.entrySet())
		{
			if (entry.getValue() != multiplicity) logger.error(entry.getKey() + "	: " + entry.getValue());

			counter++;
			if (counter % 100000 == 0) logger.error("checking : " + counter);
		}
		logger.error("total buckets:" + accumulator.size());
	}

	public static void main(String... argv)
	{
		showProof();
	}
}