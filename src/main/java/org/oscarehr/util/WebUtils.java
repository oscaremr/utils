/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public final class WebUtils
{
	private static Logger logger = MiscUtils.getLogger();
	public static final String ERROR_MESSAGE_SESSION_KEY = WebUtils.class.getName() + ".ERROR_MESSAGE_SESSION_KEY";
	public static final String INFO_MESSAGE_SESSION_KEY = WebUtils.class.getName() + ".INFO_MESSAGE_SESSION_KEY";

	public static void dumpParameters(HttpServletRequest request)
	{
		logger.error("--- Dump Request Parameters Start for " + request.getRequestURI() + " Start ---");

		Enumeration<String> e = request.getParameterNames();
		while (e.hasMoreElements())
		{
			String key = e.nextElement();
			logger.error(key + '=' + request.getParameter(key));
		}

		logger.error("--- Dump Request Parameters End ---");
	}

	/**
	 * Be warned, this is a debugging method, it should not be called in production.
	 * Calling this method will flush the input stream as servlet streams are not markable.
	 * @param request
	 * @throws IOException
	 */
	public static void dumpRequest(HttpServletRequest request) throws IOException
	{
		logger.error("--- Dump Request Start ---");

		InputStream is = request.getInputStream();

		StringBuilder sb = new StringBuilder();
		int x = 0;
		while ((x = is.read()) != -1)
		{
			sb.append((char) x);
		}
		logger.error(sb.toString());

		logger.error("--- Dump Request End ---");
	}

	/**
	 * This method is intended to be used to see if a check box was checked on a form submit
	 */
	public static boolean isChecked(HttpServletRequest request, String parameter)
	{
		String temp = request.getParameter(parameter);
		return(temp != null && (temp.equalsIgnoreCase("on") || temp.equalsIgnoreCase("true") || temp.equalsIgnoreCase("checked")));
	}

	public static String renderMessagesAsHtml(HttpSession session, String messageKey, String styleClass, String style, String id, String name)
	{
		ArrayList<String> messages = popMessages(session, messageKey);

		StringBuilder sb = new StringBuilder();

		if (messages != null && messages.size() > 0)
		{
			sb.append(getTagString("ul", style, styleClass, id, name, false));

			for (String s : messages)
			{
				sb.append("<li>");
				sb.append(s);
				sb.append("</il>");
			}

			sb.append("</ul>");
		}

		return(sb.toString());
	}

	public static String popErrorMessagesAsHtml(HttpSession session, String styleClass, String style, String id, String name)
	{
		return(renderMessagesAsHtml(session, ERROR_MESSAGE_SESSION_KEY, styleClass, style, id, name));
	}

	public static String popInfoMessagesAsHtml(HttpSession session, String styleClass, String style, String id, String name)
	{
		return(renderMessagesAsHtml(session, INFO_MESSAGE_SESSION_KEY, styleClass, style, id, name));
	}

	public static void addErrorMessage(HttpSession session, String message)
	{
		addMessage(session, ERROR_MESSAGE_SESSION_KEY, message);
	}

	public static void addLocalisedErrorMessage(HttpServletRequest request, String messageKey)
	{
		addLocalisedMessage(request, ERROR_MESSAGE_SESSION_KEY, messageKey);
	}

	public static void addLocalisedErrorMessage(HttpServletRequest request, String messageKey, String additionalText)
	{
		addLocalisedMessage(request, ERROR_MESSAGE_SESSION_KEY, messageKey, additionalText);
	}

	public static void addInfoMessage(HttpSession session, String message)
	{
		addMessage(session, INFO_MESSAGE_SESSION_KEY, message);
	}

	public static void addLocalisedInfoMessage(HttpServletRequest request, String messageKey)
	{
		addLocalisedMessage(request, INFO_MESSAGE_SESSION_KEY, messageKey);
	}

	public static void addLocalisedInfoMessage(HttpServletRequest request, String messageKey, String additionalText)
	{
		addLocalisedMessage(request, INFO_MESSAGE_SESSION_KEY, messageKey, additionalText);
	}

	private static String popMessagesAsBootstrapHtml(HttpSession session, String messageKey, String cssClass)
	{
		ArrayList<String> messages = popMessages(session, messageKey);

		StringBuilder sb = new StringBuilder();

		if (messages != null && messages.size() > 0)
		{
			sb.append(getTagString("div", null, cssClass, null, null, false));

			for (int i = 0; i < messages.size(); i++)
			{
				if (i > 1) sb.append("<br />");
				sb.append(messages.get(i));
			}

			sb.append("</div>");
		}

		return(sb.toString());
	}

	public static String popInfoErrorMessageAsBootstrapHtml(HttpSession session)
	{
		StringBuilder sb = new StringBuilder();

		sb.append(popMessagesAsBootstrapHtml(session, INFO_MESSAGE_SESSION_KEY, "alert alert-success"));
		sb.append(popMessagesAsBootstrapHtml(session, ERROR_MESSAGE_SESSION_KEY, "alert alert-danger"));

		return(sb.toString());
	}
	
	/**
	 * @return an arrayList of error message or null if no messages, the messages are then removed from the session upon return from this call.
	 */
	public static ArrayList<String> popMessages(HttpSession session, String type)
	{
		if (session==null) return(new ArrayList<String>());
		
		synchronized (session)
		{
			@SuppressWarnings("unchecked")
			ArrayList<String> errors = (ArrayList<String>) (session.getAttribute(type));
			session.removeAttribute(type);
			return(errors);
		}
	}

	public static void addLocalisedMessage(HttpServletRequest request, String type, String messageKey)
	{
		addMessage(request.getSession(), type, LocaleUtils.getMessage(request.getLocale(), messageKey));
	}

	public static void addLocalisedMessage(HttpServletRequest request, String type, String messageKey, String additionalText)
	{
		addMessage(request.getSession(), type, LocaleUtils.getMessage(request.getLocale(), messageKey) + ' ' + additionalText);
	}

	public static void addMessage(HttpSession session, String type, String message)
	{
		synchronized (session)
		{
			@SuppressWarnings("unchecked")
			ArrayList<String> messages = (ArrayList<String>) (session.getAttribute(type));

			if (messages == null)
			{
				messages = new ArrayList<String>();
				session.setAttribute(type, messages);
			}

			messages.add(message);
		}
	}

	/**
	 * This method will return a string like "?foo=bar&asdf=zxcv"
	 * based on the contents of the map. Note that the first item is a ?
	 * if an empty map is passed in it will return ""
	 * 
	 * Note that the results are not html escaped.
	 * 
	 * Null or blank values will not be added as query parameters.
	 */
	public static String buildQueryString(Map<String, Object> map)
	{
		StringBuilder sb = new StringBuilder();

		for (Map.Entry<String, Object> entry : map.entrySet())
		{
			if (entry.getValue() == null) continue;

			if (sb.length() == 0) sb.append('?');
			else sb.append('&');

			sb.append(entry.getKey());
			sb.append('=');
			sb.append(entry.getValue());
		}

		return(sb.toString());
	}

	public static String getCheckedString(boolean b)
	{
		if (b) return("checked=\"checked\"");
		else return("");
	}

	public static String getSelectedString(boolean b)
	{
		if (b) return("selected=\"selected\"");
		else return("");
	}

	/**
	 * The purpose of this method is to build a start tag with optional style and class parameters.
	 * i.e. <table style="color:red" style="myTable">
	 * 
	 * This is particularly useful in building JSF bean components in java code, not so useful in jsps etc...
	 */
	public static String getTagString(String tagName, String cssStyle, String styleClass, String id, String name, boolean closeTag)
	{
		StringBuilder sb = new StringBuilder();

		sb.append('<');
		sb.append(tagName);

		if (id != null)
		{
			sb.append(" id=\"");
			sb.append(id);
			sb.append('"');
		}

		if (name != null)
		{
			sb.append(" name=\"");
			sb.append(name);
			sb.append('"');
		}

		if (styleClass != null)
		{
			sb.append(" class=\"");
			sb.append(styleClass);
			sb.append('"');
		}

		if (cssStyle != null)
		{
			sb.append(" style=\"");
			sb.append(cssStyle);
			sb.append('"');
		}

		if (closeTag) sb.append('/');

		sb.append('>');

		return(sb.toString());
	}

	public static String trimToEmptyEscapeHtml(String s)
	{
		s = StringUtils.trimToEmpty(s);
		s = StringEscapeUtils.escapeHtml(s);
		return(s);
	}

	/**
	 * @return the html string to disable a button or component if !enabled
	 */
	public static String getDisabledString(boolean enabled)
	{
		if (!enabled) return("disabled=\"disabled\"");
		else return("");
	}

	/**
	 * Generally speaking your characters can be double the em length, i.e. 10 characters will fit in 5em.
	 */
	public static String limitStringLength(String s, int length)
	{
		if (s == null) return(null);
		if (s.length() <= length) return(s);
		else return(s.substring(0, length - 3) + "...");
	}

	/**
	 * This converts a javascript Date.getTimezoneOffset value to a java TimeZone.customId.
	 * i.e. "420" -> "GMT-0700"
	 */
	public static String convertTimeZoneOffsetToTimeZoneCustomId(String timezoneOffset)
	{
		int offsetMinutes=Integer.parseInt(timezoneOffset);
		
		char sign='-';
		
		if (offsetMinutes<0)
		{
			sign='+';
			offsetMinutes=-offsetMinutes;
		}
		
		int customIdMinutes=offsetMinutes%60;
		int customIdHours=offsetMinutes/60;
		
		StringBuilder sb=new StringBuilder();
		
		sb.append("GMT");
		sb.append(sign);
		
		if (customIdHours<10) sb.append('0');
		sb.append(customIdHours);
		
		if (customIdMinutes<10) sb.append('0');
		sb.append(customIdMinutes);
		
		return(sb.toString());
	}
}