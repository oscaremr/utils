/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.ThreadMXBean;
import java.util.List;
import java.util.Timer;

public final class VmStat
{
	private static List<MemoryPoolMXBean> memoryPools = ManagementFactory.getMemoryPoolMXBeans();
	private static ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
	private static OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
	private static List<GarbageCollectorMXBean> garbageCollectorMXBeans = ManagementFactory.getGarbageCollectorMXBeans();

	private static Timer timer = new Timer(VmStat.class.getName(), true);
	private static VmStatTimerTask timerTask = null;

	public static synchronized void startContinuousLogging(long logPeriodMilliseconds, VmStatDataLoggerInterface vmStatDataLoggerInterface)
	{
		if (timerTask != null) throw(new IllegalStateException("ContinuousLogging already started."));

		timerTask = new VmStatTimerTask(vmStatDataLoggerInterface);
		timer.schedule(timerTask, 0, logPeriodMilliseconds);
	}

	public static synchronized void stopContinuousLogging()
	{
		if (timerTask != null)
		{
			timerTask.cancel();
			timerTask = null;
		}
	}

	public static void logAll(VmStatDataLoggerInterface vmStatDataLoggerInterface)
	{
		logMemoryInfo(vmStatDataLoggerInterface);
		getGCInfo(vmStatDataLoggerInterface);
		getThreadInfo(vmStatDataLoggerInterface);
		getCpuInfo(vmStatDataLoggerInterface);
	}

	public static void logMemoryInfo(VmStatDataLoggerInterface vmStatDataLoggerInterface)
	{
		for (MemoryPoolMXBean memoryPool : memoryPools)
		{
			MemoryUsage memoryUsage = memoryPool.getUsage();
			vmStatDataLoggerInterface.logMemInfo(memoryPool.getName(), memoryUsage.getMax(), memoryUsage.getUsed());
		}
	}

	public static void getThreadInfo(VmStatDataLoggerInterface vmStatDataLoggerInterface)
	{
		vmStatDataLoggerInterface.logThreadCounts(threadMXBean.getThreadCount(), threadMXBean.getDaemonThreadCount(), threadMXBean.getPeakThreadCount());
	}

	public static void getCpuInfo(VmStatDataLoggerInterface vmStatDataLoggerInterface)
	{
		vmStatDataLoggerInterface.logCpuUsage(operatingSystemMXBean.getAvailableProcessors(), operatingSystemMXBean.getSystemLoadAverage());
	}

	public static void getGCInfo(VmStatDataLoggerInterface vmStatDataLoggerInterface)
	{
		for (GarbageCollectorMXBean garbageCollectorMXBean : garbageCollectorMXBeans)
		{
			vmStatDataLoggerInterface.logGcInfo(garbageCollectorMXBean.getName(), garbageCollectorMXBean.getCollectionCount(), garbageCollectorMXBean.getCollectionTime());
		}
	}

	public static void main(String... argv) throws Exception
	{
		startContinuousLogging(5000, new VmStatLog4JLogger());

		System.gc();

		Thread.sleep(60000);
	}
}
