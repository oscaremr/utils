/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.util.Comparator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public final class PerformanceTrackerEntry
{
	private static final String ACTION_REQUEST_COUNT_ATTRIBUTE_NAME = "requestCount";
	private static final String ACTION_REQUEST_MAX_TIME_ATTRIBUTE_NAME = "maxTime";
	private static final String ACTION_REQUEST_TOTAL_TIME_ATTRIBUTE_NAME = "totalTime";
	private static final String ACTION_REQUEST_MAX_DATA_SIZE_ATTRIBUTE_NAME = "maxDataSize";
	private static final String ACTION_REQUEST_TOTAL_DATA_SIZE_ATTRIBUTE_NAME = "totalDataSize";

	public static final Comparator<PerformanceTrackerEntry> ACTION_COMPARATOR = new Comparator<PerformanceTrackerEntry>()
	{
		@Override
		public int compare(PerformanceTrackerEntry o1, PerformanceTrackerEntry o2)
		{
			return(o1.action.compareTo(o2.action));
		}
	};

	public static final Comparator<PerformanceTrackerEntry> REQUEST_COUNT_COMPARATOR = new Comparator<PerformanceTrackerEntry>()
	{
		@Override
		public int compare(PerformanceTrackerEntry o1, PerformanceTrackerEntry o2)
		{
			return(o1.requestCount - o2.requestCount);
		}
	};

	public static final Comparator<PerformanceTrackerEntry> MAX_TIME_COMPARATOR = new Comparator<PerformanceTrackerEntry>()
	{
		@Override
		public int compare(PerformanceTrackerEntry o1, PerformanceTrackerEntry o2)
		{
			return(o1.maxTimeMs - o2.maxTimeMs);
		}
	};

	public static final Comparator<PerformanceTrackerEntry> TOTAL_TIME_COMPARATOR = new Comparator<PerformanceTrackerEntry>()
	{
		@Override
		public int compare(PerformanceTrackerEntry o1, PerformanceTrackerEntry o2)
		{
			return(o1.totalTimeMs - o2.totalTimeMs);
		}
	};

	public static final Comparator<PerformanceTrackerEntry> AVERAGE_TIME_COMPARATOR = new Comparator<PerformanceTrackerEntry>()
	{
		@Override
		public int compare(PerformanceTrackerEntry o1, PerformanceTrackerEntry o2)
		{
			return((o1.totalTimeMs / o1.requestCount) - (o2.totalTimeMs / o2.requestCount));
		}
	};

	public static final Comparator<PerformanceTrackerEntry> MAX_DATA_SIZE_COMPARATOR = new Comparator<PerformanceTrackerEntry>()
	{
		@Override
		public int compare(PerformanceTrackerEntry o1, PerformanceTrackerEntry o2)
		{
			long compareResult = o1.maxDataSize - o2.maxDataSize;
			if (compareResult > 0) return(1);
			else if (compareResult < 0) return(-1);
			return(0);
		}
	};

	public static final Comparator<PerformanceTrackerEntry> TOTAL_DATA_SIZE_COMPARATOR = new Comparator<PerformanceTrackerEntry>()
	{
		@Override
		public int compare(PerformanceTrackerEntry o1, PerformanceTrackerEntry o2)
		{
			long compareResult = o1.totalDataSize - o2.totalDataSize;
			if (compareResult > 0) return(1);
			else if (compareResult < 0) return(-1);
			return(0);
		}
	};

	public static final Comparator<PerformanceTrackerEntry> AVERAGE_DATA_SIZE_COMPARATOR = new Comparator<PerformanceTrackerEntry>()
	{
		@Override
		public int compare(PerformanceTrackerEntry o1, PerformanceTrackerEntry o2)
		{
			float compareResult = (o1.totalDataSize / o1.requestCount) - (o2.totalDataSize / o2.requestCount);
			if (compareResult > 0) return(1);
			else if (compareResult < 0) return(-1);
			return(0);
		}
	};

	private String action;
	private int requestCount;
	private int maxTimeMs;
	private int totalTimeMs;
	private long maxDataSize;
	private long totalDataSize;

	public PerformanceTrackerEntry(String action, int timeForAction, long dataSize)
	{
		this.action = action;
		requestCount = 1;
		maxTimeMs = timeForAction;
		totalTimeMs = timeForAction;
		maxDataSize = dataSize;
		totalDataSize = dataSize;
	}

	public PerformanceTrackerEntry(Node node)
	{
		action = node.getTextContent();
		requestCount = Integer.parseInt(XmlUtils.getAttributeValue(node, ACTION_REQUEST_COUNT_ATTRIBUTE_NAME));
		maxTimeMs = Integer.parseInt(XmlUtils.getAttributeValue(node, ACTION_REQUEST_MAX_TIME_ATTRIBUTE_NAME));
		totalTimeMs = Integer.parseInt(XmlUtils.getAttributeValue(node, ACTION_REQUEST_TOTAL_TIME_ATTRIBUTE_NAME));
		maxDataSize = Integer.parseInt(XmlUtils.getAttributeValue(node, ACTION_REQUEST_MAX_DATA_SIZE_ATTRIBUTE_NAME));
		totalDataSize = Integer.parseInt(XmlUtils.getAttributeValue(node, ACTION_REQUEST_TOTAL_DATA_SIZE_ATTRIBUTE_NAME));
	}

	public void addRequest(int timeForAction, long dataSize)
	{
		synchronized (this)
		{
			requestCount++;
			maxTimeMs = Math.max(maxTimeMs, timeForAction);
			totalTimeMs = totalTimeMs + timeForAction;
			maxDataSize = Math.max(maxDataSize, dataSize);
			totalDataSize = totalDataSize + dataSize;
		}
	}

	public void mergeEntries(PerformanceTrackerEntry otherEntry)
	{
		synchronized (this)
		{
			requestCount = requestCount + otherEntry.requestCount;
			maxTimeMs = Math.max(maxTimeMs, otherEntry.maxTimeMs);
			totalTimeMs = totalTimeMs + otherEntry.totalTimeMs;
			maxDataSize = Math.max(maxDataSize, otherEntry.maxDataSize);
			totalDataSize = totalDataSize + otherEntry.totalDataSize;
		}
	}

	public String getAction()
	{
		return(action);
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public int getRequestCount()
	{
		return(requestCount);
	}

	public void setRequestCount(int requestCount)
	{
		this.requestCount = requestCount;
	}

	public int getMaxTimeMs()
	{
		return(maxTimeMs);
	}

	public void setMaxTimeMs(int maxTimeMs)
	{
		this.maxTimeMs = maxTimeMs;
	}

	public int getTotalTimeMs()
	{
		return(totalTimeMs);
	}

	public void setTotalTimeMs(int totalTimeMs)
	{
		this.totalTimeMs = totalTimeMs;
	}

	public long getMaxDataSize()
	{
		return(maxDataSize);
	}

	public void setMaxDataSize(long maxDataSize)
	{
		this.maxDataSize = maxDataSize;
	}

	public long getTotalDataSize()
	{
		return(totalDataSize);
	}

	public void setTotalDataSize(long totalDataSize)
	{
		this.totalDataSize = totalDataSize;
	}

	public Element toXmlElement(Document doc)
	{
		Element element = doc.createElement(PerformanceTrackerUtils.ACTION_XML_TAG_NAME);
		element.setTextContent(action);
		element.setAttribute(ACTION_REQUEST_COUNT_ATTRIBUTE_NAME, String.valueOf(requestCount));
		element.setAttribute(ACTION_REQUEST_MAX_TIME_ATTRIBUTE_NAME, String.valueOf(maxTimeMs));
		element.setAttribute(ACTION_REQUEST_TOTAL_TIME_ATTRIBUTE_NAME, String.valueOf(totalTimeMs));
		element.setAttribute(ACTION_REQUEST_MAX_DATA_SIZE_ATTRIBUTE_NAME, String.valueOf(maxDataSize));
		element.setAttribute(ACTION_REQUEST_TOTAL_DATA_SIZE_ATTRIBUTE_NAME, String.valueOf(totalDataSize));
		return(element);
	}
}
