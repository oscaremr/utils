/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

public final class DateUtils
{
	private static final TimeZone GMT_TIME_ZONE = TimeZone.getTimeZone("GMT");

	public static final String JS_ISO_DATE_FORMAT = "yy-mm-dd";

	/**
	 * Null Safe
	 */
	public static String getIsoDateTimeNoTNoSeconds(Calendar cal)
	{
		if (cal == null) return ("");

		String s = getIsoDateTimeNoT(cal);
		return (s.substring(0, s.length() - 3));
	}

	/**
	 * Null safe
	 */
	public static String getIsoDateTimeNoT(Calendar cal)
	{
		if (cal == null) return ("");

		return (DateFormatUtils.ISO_DATETIME_FORMAT.format(cal).replace('T', ' '));
	}

	/**
	 * Null safe
	 */
	public static String getIsoDateTime(Calendar cal)
	{
		if (cal == null) return ("");

		return (DateFormatUtils.ISO_DATETIME_FORMAT.format(cal));
	}

	/**
	 * Null safe
	 */
	public static String getIsoDate(Calendar cal)
	{
		if (cal == null) return ("");

		return (DateFormatUtils.ISO_DATE_FORMAT.format(cal));
	}

	/**
	 * @return null upon null input
	 */
	public static Date parseDate(String s, String pattern, TimeZone timeZone) throws ParseException
	{
		s = StringUtils.trimToNull(s);
		if (s == null) return (null);

		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		Date date = dateFormat.parse(s);

		long offset = timeZone.getOffset(date.getTime());
		date.setTime(date.getTime() - offset);

		return (date);
	}

	/**
	 * @return null upon null input
	 */
	public static Date parseIsoDate(String s, String pattern, TimeZone timeZone) throws ParseException
	{
		s = StringUtils.trimToNull(s);
		if (s == null) return (null);

		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		Date date = dateFormat.parse(s);

		if (timeZone != null)
		{
			long offset = timeZone.getOffset(date.getTime());
			date.setTime(date.getTime() - offset);
		}

		return (date);
	}

	/**
	 * @return null upon null input
	 */
	public static Date parseIsoDate(String s) throws ParseException
	{
		return (parseIsoDate(s, DateFormatUtils.ISO_DATE_FORMAT.getPattern(), null));
	}

	/**
	 * @return null upon null input
	 */
	public static GregorianCalendar parseIsoDateAsCalendar(String s) throws ParseException
	{
		Date date = parseIsoDate(s);
		if (date == null) return (null);
		else
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);
			cal.getTimeInMillis();
			return (cal);
		}
	}

	/**
	 * @return null upon null input
	 */
	public static Date parseIsoDateTime(String s) throws ParseException
	{
		s = StringUtils.trimToNull(s);
		if (s == null) return (null);

		SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatUtils.ISO_DATETIME_FORMAT.getPattern());
		Date date = dateFormat.parse(s);
		return (date);
	}

	/**
	 * Null Safe
	 */
	public static GregorianCalendar parseIsoDateTimeAsCalendar(String s) throws ParseException
	{
		Date date = parseIsoDateTime(s);
		if (date == null) return (null);
		else
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(date);
			cal.getTimeInMillis();
			return (cal);
		}
	}

	/**
	 * date2-date1
	 * 
	 * if either are null, it returns null.
	 */
	public static Integer yearDifference(Calendar date1, Calendar date2)
	{
		if (date1 == null || date2 == null) return (null);

		int yearDiff = date2.get(Calendar.YEAR) - date1.get(Calendar.YEAR);

		if (date2.get(Calendar.DAY_OF_YEAR) > date1.get(Calendar.DAY_OF_YEAR)) yearDiff--;

		return (yearDiff);
	}

	/**
	 * This method will calculate the age of the person on the given date.
	 */
	public static Integer getAge(Calendar dateOfBirth, Calendar onThisDay)
	{
		return (yearDifference(dateOfBirth, onThisDay));
	}

	/**
	 * This method will set the calenders hour/min/sec//milliseconds all to 0.
	 */
	public static Calendar setToBeginningOfDay(Calendar cal)
	{
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		// force calculation / materialisation of actual time.
		cal.getTimeInMillis();

		return (cal);
	}

	/**
	 * Null Safe
	 */
	public static Date parseJsIsoDateTimeNoTNoSeconds(String s) throws ParseException
	{
		s = StringUtils.trimToNull(s);
		if (s == null) return (null);

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = dateFormat.parse(s);
		return (date);
	}

	public static Calendar setToGMT(Calendar cal)
	{
		// must materialise time before setting zone or it thinks you're setting that time in that zone.
		cal.getTimeInMillis();

		cal.setTimeZone(GMT_TIME_ZONE);

		// materialise value again so results are as expected.
		cal.getTimeInMillis();

		return (cal);
	}

	public static boolean isSameDay(Calendar cal1, Calendar cal2)
	{
		return (org.apache.commons.lang.time.DateUtils.isSameDay(cal1, cal2));
	}

	/**
	 * Same week in the same year 
	 */
	public static boolean isSameWeek(Calendar cal1, Calendar cal2)
	{
		if (!isSameYear(cal1, cal2)) return (false);

		int time1 = cal1.get(Calendar.WEEK_OF_YEAR);
		int time2 = cal2.get(Calendar.WEEK_OF_YEAR);
		return (time1 == time2);
	}

	/**
	 * Same month in the same year 
	 */
	public static boolean isSameMonth(Calendar cal1, Calendar cal2)
	{
		if (!isSameYear(cal1, cal2)) return (false);

		int time1 = cal1.get(Calendar.MONTH);
		int time2 = cal2.get(Calendar.MONTH);
		return (time1 == time2);
	}

	public static boolean isSameYear(Calendar cal1, Calendar cal2)
	{
		return (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR));
	}

	public static GregorianCalendar getDateAsCalendar(Date d)
	{
		if (d == null) return (null);

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);
		cal.getTimeInMillis();
		return (cal);
	}

	/**
	 * @return null upon null input
	 */
	public static Date parseIsoDate(String s, TimeZone timeZone) throws ParseException
	{
		return (parseIsoDate(s, DateFormatUtils.ISO_DATE_FORMAT.getPattern(), timeZone));
	}

	/**
	 * @return null upon null input
	 */
	public static Date parseIsoDateTime(String s, TimeZone timeZone) throws ParseException
	{
		return (parseIsoDate(s, DateFormatUtils.ISO_DATETIME_FORMAT.getPattern(), timeZone));
	}

	/**
	 * @return null upon null input
	 */
	public static Date parseIsoDateTimeNoT(String s, TimeZone timeZone) throws ParseException
	{
		return (parseIsoDate(s, "yyyy-MM-dd HH:mm:ss", timeZone));
	}

	/**
	 * @return null upon null input
	 */
	public static Date parseIsoDateTimeNoSecondsNoT(String s, TimeZone timeZone) throws ParseException
	{
		return (parseIsoDate(s, "yyyy-MM-dd HH:mm", timeZone));
	}

	/**
	 * Null Safe
	 */
	public static GregorianCalendar parseIsoDateAsCalendar(String s, TimeZone timeZone) throws ParseException
	{
		Date d = parseIsoDate(s, timeZone);
		return (getDateAsCalendar(d));
	}

	/**
	 * Null Safe
	 */
	public static GregorianCalendar parseIsoDateTimeAsCalendar(String s, TimeZone timeZone) throws ParseException
	{
		Date d = parseIsoDateTime(s, timeZone);
		return (getDateAsCalendar(d));
	}

	/**
	 * Null Safe
	 */
	public static GregorianCalendar parseIsoDateTimeNoTAsCalendar(String s, TimeZone timeZone) throws ParseException
	{
		Date d = parseIsoDateTimeNoT(s, timeZone);
		return (getDateAsCalendar(d));
	}

	/**
	 * Null Safe
	 */
	public static GregorianCalendar parseIsoDateTimeNoSecondsNoTAsCalendar(String s, TimeZone timeZone) throws ParseException
	{
		Date d = parseIsoDateTimeNoSecondsNoT(s, timeZone);
		return (getDateAsCalendar(d));
	}
}
