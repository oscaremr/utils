/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This is a very simple single item cache.
 * Put an item in, and it will keep returning it till the max time is reached at which point it'll return null.
 * 
 * cache time should be relatively large, like 1000ms or more probably most effective for 1+ minute.
 */
public final class SimpleSingleItemCache<T>
{
	private static Timer timer = new Timer(SimpleSingleItemCache.class.getName(), true);

	private class ClearTimerTask extends TimerTask
	{
		@Override
		public void run()
		{
			clear();
		}
	}

	private T data;
	private long maxTimeToCache;

	public SimpleSingleItemCache(long maxTimeToCache)
	{
		this.maxTimeToCache = maxTimeToCache;
	}

	public void set(T value)
	{
		data = value;
		timer.schedule(new ClearTimerTask(), maxTimeToCache);
	}

	public void clear()
	{
		data = null;
	}

	public T get()
	{
		return(data);
	}

}