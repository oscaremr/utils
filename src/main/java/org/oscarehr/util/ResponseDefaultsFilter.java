/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

// @WebFilter(urlPatterns={"/*"})
public final class ResponseDefaultsFilter implements Filter
{
	private static Logger logger = MiscUtils.getLogger();

	private static final String EXPIRES_KEY = "Expires";
	private static final String CACHE_CONTROL_KEY = "Cache-Control";

	private boolean setEncoding = true;
	private String encoding = MiscUtils.DEFAULT_UTF8_ENCODING;

	private String[] noCacheEndings = { ".jsp", ".json", ".jsf", ".html" };
	private String[] longCacheEndings = { ".js", ".css", ".png", ".jpg", ".ico", ".gif" };

	private boolean forceStrongETag = true;
	private boolean forceNoETags = true;
	private boolean warnCharsetCacheChange = false;

	/**
	 * This variable is set by the timer task for efficiency so we're not making calendars and parsing date formats all the time
	 */
	private String longCacheExpiryDate = generateLongCacheExpiryValue();

	private Timer timer = new Timer(ResponseDefaultsFilter.class.getName(), true);
	private TimerTask timerTask = new TimerTask()
	{
		@Override
		public void run()
		{
			longCacheExpiryDate = generateLongCacheExpiryValue();
		}
	};

	@Override
	public void init(FilterConfig filterConfig)
	{
		logger.info("Initialising " + ResponseDefaultsFilter.class.getSimpleName());

		String temp = filterConfig.getInitParameter("setEncoding");
		if (temp != null)
		{
			setEncoding = Boolean.parseBoolean(temp);
			encoding = filterConfig.getInitParameter("encoding");
		}
		logger.info("setEncoding=" + setEncoding + ", encoding=" + encoding);

		temp = filterConfig.getInitParameter("noCacheEndings");
		if (temp != null) noCacheEndings = temp.split(",");

		temp = filterConfig.getInitParameter("longCacheEndings");
		if (temp != null) longCacheEndings = temp.split(",");

		temp = filterConfig.getInitParameter("forceStrongETag");
		if (temp != null)
		{
			forceStrongETag = Boolean.parseBoolean(temp);
		}
		logger.info("forceStrongETag=" + forceStrongETag);

		temp = filterConfig.getInitParameter("forceNoETags");
		if (temp != null)
		{
			forceNoETags = Boolean.parseBoolean(temp);
		}
		logger.info("forceNoETags=" + forceNoETags);

		temp = filterConfig.getInitParameter("warnCharsetCacheChange");
		if (temp != null)
		{
			warnCharsetCacheChange = Boolean.parseBoolean(temp);
		}
		logger.info("warnCharsetCacheChange=" + warnCharsetCacheChange);

		timer.schedule(timerTask, 0, DateUtils.MILLIS_PER_DAY);
	}

	@Override
	public void destroy()
	{
		timerTask.cancel();

		logger.info("shutdown " + ResponseDefaultsFilter.class.getSimpleName());
	}

	@Override
	public void doFilter(ServletRequest originalRequest, ServletResponse originalResponse, FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest request = (HttpServletRequest) originalRequest;
		HttpServletResponse response = (HttpServletResponse) originalResponse;

		if (!ignored(request))
		{
			if (setEncoding) setEncoding(request, response);
			setCaching(request, response);
	
			if (forceStrongETag || forceNoETags || warnCharsetCacheChange) response = new ResponseDefaultsFilterResponseWrapper(response, forceStrongETag, forceNoETags, warnCharsetCacheChange);
		}
		
		chain.doFilter(request, response);
	}

	private boolean ignored(HttpServletRequest request)
	{
		// https://127.0.0.1:8096/myoscar_client/javax.faces.resource/jquery/jquery.js.jsf?ln=primefaces&v=5.0

		String servletPath=request.getServletPath();
		if (servletPath.startsWith("/javax.faces.resource/")) return(true);
		
		return false;
	}

	private void setCaching(HttpServletRequest request, HttpServletResponse response)
	{
		String requestUri = request.getRequestURI();
		
		for (String noCacheEnding : noCacheEndings)
		{
			if (requestUri.endsWith(noCacheEnding))
			{
				response.setHeader(CACHE_CONTROL_KEY, "no-store, no-cache, must-revalidate");
				return;
			}
		}

		for (String longCacheEnding : longCacheEndings)
		{
			if (requestUri.endsWith(longCacheEnding))
			{
				response.setHeader(EXPIRES_KEY, longCacheExpiryDate);
				return;
			}
		}
	}

	private void setEncoding(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException
	{
		// use the requested encoding, but if none is specified then we use UTF-8
		if (request.getCharacterEncoding() == null) request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);
	}

	private String generateLongCacheExpiryValue()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MONTH, 4);
		String result = sdf.format(cal.getTime());
		return(result);
	}
}