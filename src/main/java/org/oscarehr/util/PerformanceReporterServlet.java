/**
 * Copyright (c) 2001-2013. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import java.io.IOException;
import java.io.Writer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.xml.sax.SAXException;

public abstract class PerformanceReporterServlet extends HttpServlet
{
	private static final String CATEGORY_PARAMETER = "category";
	private static final String TIME_MONTH_PARMETER = "timeMonth";
	private static final String TIME_SERVER_START_PARMETER = "timeServerStart";
	private static final String ORDER_BY_PARMETER = "orderBy";
	private static final String ORDER_BY_TOTAL_TIME = "totalTime";
	private static final String ORDER_BY_AVG_TIME = "avgTime";
	private static final String ORDER_BY_MAX_TIME = "maxTime";
	private static final String ORDER_BY_TOTAL_DATA_SIZE = "totalDataSize";
	private static final String ORDER_BY_AVG_DATA_SIZE = "avgDataSize";
	private static final String ORDER_BY_MAX_DATA_SIZE = "maxDataSize";
	private static final String ORDER_BY_REQUEST_COUNT = "requestCount";

	public abstract String getDataRoot();

	public abstract String getCssPath();

	@Override
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException
	{
		try
		{
			TreeSet<Calendar> startTimes = PerformanceTrackerUtils.getServerStartLog(getDataRoot());

			TreeSet<Calendar> startTimesDayTruncated = new TreeSet<Calendar>();
			for (Calendar startTime : startTimes)
			{
				startTime = DateUtils.truncate(startTime, Calendar.DAY_OF_MONTH);
				startTimesDayTruncated.add(startTime);
			}

			TreeSet<String> allCategories = PerformanceTrackerUtils.getAllCategories(getDataRoot());

			//---

			response.setContentType("text/html");
			Writer writer = response.getWriter();
			writeHtmlTop(writer);

			renderReportCriteria(writer, allCategories, startTimesDayTruncated);

			writer.write("<hr />\n");

			renderReportResults(request, writer, startTimesDayTruncated);

			writeHtmlBottom(writer);
			writer.flush();
		}
		catch (Exception e)
		{
			throw(new ServletException(e));
		}
	}

	private void renderReportResults(HttpServletRequest request, Writer writer, TreeSet<Calendar> startTimesDayTruncated) throws IOException, ParseException, ParserConfigurationException, SAXException
	{
		String category = StringUtils.trimToNull(request.getParameter(CATEGORY_PARAMETER));
		String monthTimeString = StringUtils.trimToNull(request.getParameter(TIME_MONTH_PARMETER));
		String serverStartTimeString = StringUtils.trimToNull(request.getParameter(TIME_SERVER_START_PARMETER));
		String orderBy = StringUtils.trimToNull(request.getParameter(ORDER_BY_PARMETER));

		Calendar startTime = null;
		Calendar endTime = null;

		if (monthTimeString != null)
		{
			startTime = PerformanceTrackerUtils.parseIsoDateToCalendar(monthTimeString);
			endTime = (Calendar) startTime.clone();
			endTime.add(Calendar.MONTH, 1);
			endTime.getTimeInMillis();
		}

		if (serverStartTimeString != null)
		{
			startTime = PerformanceTrackerUtils.parseIsoDateToCalendar(serverStartTimeString);
			endTime = getEndTimeForServerStartPeriod(startTimesDayTruncated, startTime);
		}

		if (category != null && startTime != null && endTime != null)
		{
			int sumCount = 0;
			int sumTotalTime = 0;
			long sumTotalDataSize = 0;

			writer.write("Category : " + category);
			writer.write("<br />\n");
			writer.write("Timeframe : " + DateFormatUtils.ISO_DATE_FORMAT.format(startTime) + " to " + DateFormatUtils.ISO_DATE_FORMAT.format(endTime));
			writer.write("<br />\n");
			writer.write("Order By : " + orderBy);
			writer.write("<br />\n");
			writer.write("<br />\n");

			ArrayList<PerformanceTrackerEntry> sortedResults = getSortedResults(category, startTime, endTime, orderBy);

			writer.write("<table style=\"border:solid black 1px;border-collapse:collapse\">");
			writer.write("<tr>");
			writer.write("<td style=\"border:solid black 1px;background-color:cyan\">Action</td>");
			writer.write("<td style=\"border:solid black 1px;background-color:cyan\">Count</td>");
			writer.write("<td style=\"border:solid black 1px;background-color:cyan\">Total Time (ms)</td>");
			writer.write("<td style=\"border:solid black 1px;background-color:cyan\">Max Time (ms)</td>");
			writer.write("<td style=\"border:solid black 1px;background-color:cyan\">Avg Time (ms)</td>");
			writer.write("<td style=\"border:solid black 1px;background-color:cyan\">Total Data Size (bytes)</td>");
			writer.write("<td style=\"border:solid black 1px;background-color:cyan\">Max Data Size (bytes)</td>");
			writer.write("<td style=\"border:solid black 1px;background-color:cyan\">Avg Data Size (bytes)</td>");
			writer.write("</tr>");

			NumberFormat avgFormat = NumberFormat.getInstance();
			avgFormat.setMaximumFractionDigits(1);
			avgFormat.setMinimumFractionDigits(1);

			NumberFormat numberFormat = NumberFormat.getInstance();
			numberFormat.setMaximumFractionDigits(0);
			numberFormat.setMinimumFractionDigits(0);
			numberFormat.setGroupingUsed(true);

			for (PerformanceTrackerEntry entry : sortedResults)
			{
				sumCount = sumCount + entry.getRequestCount();
				sumTotalTime = sumTotalTime + entry.getTotalTimeMs();
				sumTotalDataSize = sumTotalDataSize + entry.getTotalDataSize();

				writer.write("<tr>");

				writer.write("<td style=\"border:solid black 1px\">");
				writer.write(entry.getAction());
				writer.write("</td>");

				writer.write("<td style=\"border:solid black 1px;text-align:right\">");
				writer.write(numberFormat.format(entry.getRequestCount()));
				writer.write("</td>");

				writer.write("<td style=\"border:solid black 1px;text-align:right\">");
				writer.write(numberFormat.format(entry.getTotalTimeMs()));
				writer.write("</td>");

				writer.write("<td style=\"border:solid black 1px;text-align:right\">");
				writer.write(numberFormat.format(entry.getMaxTimeMs()));
				writer.write("</td>");

				writer.write("<td style=\"border:solid black 1px;text-align:right\">");
				writer.write(avgFormat.format(entry.getTotalTimeMs() / entry.getRequestCount()));
				writer.write("</td>");

				writer.write("<td style=\"border:solid black 1px;text-align:right\">");
				writer.write(numberFormat.format(entry.getTotalDataSize()));
				writer.write("</td>");

				writer.write("<td style=\"border:solid black 1px;text-align:right\">");
				writer.write(numberFormat.format(entry.getMaxDataSize()));
				writer.write("</td>");

				writer.write("<td style=\"border:solid black 1px;text-align:right\">");
				writer.write(numberFormat.format(entry.getMaxDataSize() / entry.getRequestCount()));
				writer.write("</td>");

				writer.write("</tr>");
			}

			writer.write("</table>");

			writer.write("<br />");
			writer.write("<br />");

			writer.write("Sum total count : " + numberFormat.format(sumCount));
			writer.write("<br />");

			if (sumCount != 0)
			{
				writer.write("Sum total time : " + numberFormat.format(sumTotalTime));
				writer.write("<br />");
				writer.write("Sum avg time : " + avgFormat.format(sumTotalTime / sumCount));
				writer.write("<br />");
				writer.write("Sum total data size : " + numberFormat.format(sumTotalDataSize));
				writer.write("<br />");
				writer.write("Sum avg data size : " + numberFormat.format(sumTotalDataSize / sumCount));
				writer.write("<br />");
			}
		}
	}

	private ArrayList<PerformanceTrackerEntry> getSortedResults(String category, Calendar startTime, Calendar endTime, String orderBy) throws ParserConfigurationException, SAXException, IOException
	{
		HashMap<String, PerformanceTrackerEntry> entries = PerformanceReporter.getConsolodatedResults(getDataRoot(), category, startTime, endTime);
		ArrayList<PerformanceTrackerEntry> sortedResults = new ArrayList<PerformanceTrackerEntry>();
		sortedResults.addAll(entries.values());

		if (ORDER_BY_REQUEST_COUNT.equals(orderBy)) Collections.sort(sortedResults, PerformanceTrackerEntry.REQUEST_COUNT_COMPARATOR);
		else if (ORDER_BY_MAX_TIME.equals(orderBy)) Collections.sort(sortedResults, PerformanceTrackerEntry.MAX_TIME_COMPARATOR);
		else if (ORDER_BY_AVG_TIME.equals(orderBy)) Collections.sort(sortedResults, PerformanceTrackerEntry.AVERAGE_TIME_COMPARATOR);
		else if (ORDER_BY_TOTAL_TIME.equals(orderBy)) Collections.sort(sortedResults, PerformanceTrackerEntry.TOTAL_TIME_COMPARATOR);
		else if (ORDER_BY_MAX_DATA_SIZE.equals(orderBy)) Collections.sort(sortedResults, PerformanceTrackerEntry.MAX_DATA_SIZE_COMPARATOR);
		else if (ORDER_BY_AVG_DATA_SIZE.equals(orderBy)) Collections.sort(sortedResults, PerformanceTrackerEntry.AVERAGE_DATA_SIZE_COMPARATOR);
		else if (ORDER_BY_TOTAL_DATA_SIZE.equals(orderBy)) Collections.sort(sortedResults, PerformanceTrackerEntry.TOTAL_DATA_SIZE_COMPARATOR);

		Collections.reverse(sortedResults);
		return(sortedResults);
	}

	private static Calendar getEndTimeForServerStartPeriod(TreeSet<Calendar> startTimesDayTruncated, Calendar startTime)
	{
		boolean useNextValue = false;
		for (Calendar tempCal : startTimesDayTruncated)
		{
			if (useNextValue) return(tempCal);

			if (DateUtils.isSameDay(startTime, tempCal)) useNextValue = true;
		}

		// default it to now in case they're looking at the current reboot, we put it 1 day in the future so if it's today it gives at least todays numbers 
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DAY_OF_YEAR, 1);
		cal.getTimeInMillis();
		return(cal);
	}

	private void renderReportCriteria(Writer writer, TreeSet<String> allCategories, TreeSet<Calendar> startTimesDayTruncated) throws IOException, ClassCastException
	{
		writer.write("<form method=\"post\">\n");

		renderCategorySelector(writer, allCategories);
		renderDateSelection(writer);
		renderServerRestartSelection(writer, startTimesDayTruncated);
		renderOrderBySelection(writer);

		writer.write("<input type=\"submit\" />\n");

		writer.write("</form>\n");
	}

	private void renderOrderBySelection(Writer writer) throws IOException
	{
		writer.write("OrderBy : ");
		writer.write("<select name=\"" + ORDER_BY_PARMETER + "\">");

		writer.write("<option value=\"" + ORDER_BY_REQUEST_COUNT + "\">Request Count</option>");
		writer.write("<option value=\"" + ORDER_BY_AVG_TIME + "\">Avg Time</option>");
		writer.write("<option value=\"" + ORDER_BY_TOTAL_TIME + "\">Total Time</option>");
		writer.write("<option value=\"" + ORDER_BY_MAX_TIME + "\">Max Time</option>");
		writer.write("<option value=\"" + ORDER_BY_AVG_DATA_SIZE + "\">Avg Data Size</option>");
		writer.write("<option value=\"" + ORDER_BY_TOTAL_DATA_SIZE + "\">Total Data Size</option>");
		writer.write("<option value=\"" + ORDER_BY_MAX_DATA_SIZE + "\">Max Data Size</option>");

		writer.write("</select>");
		writer.write("<br />\n");
	}

	private void renderServerRestartSelection(Writer writer, TreeSet<Calendar> startTimesDayTruncated) throws IOException, ClassCastException
	{
		writer.write("Time period server restart : ");
		writer.write("<select name=\"" + TIME_SERVER_START_PARMETER + "\">");
		writer.write("<option value=\"\"> - </option>");

		for (Calendar startTime : startTimesDayTruncated.descendingSet())
		{
			DateUtils.truncate(startTime, Calendar.DAY_OF_MONTH);

			String dateString = DateFormatUtils.ISO_DATE_FORMAT.format(startTime);

			writer.write("<option value=\"" + dateString + "\">");
			writer.write(dateString);
			writer.write("</option>");
		}

		writer.write("</select>");
		writer.write("<br />\n");
	}

	private void renderDateSelection(Writer writer) throws IOException
	{
		writer.write("Time period month : ");
		writer.write("<select name=\"" + TIME_MONTH_PARMETER + "\">");
		writer.write("<option value=\"\"> - </option>");

		for (int i = 0; i < 24; i++)
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.add(Calendar.MONTH, -i);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.getTimeInMillis();

			String dateString = DateFormatUtils.ISO_DATE_FORMAT.format(cal);

			writer.write("<option value=\"" + dateString + "\">");
			writer.write(dateString);
			writer.write("</option>");
		}

		writer.write("</select>");
		writer.write("<br />\n");
	}

	private void renderCategorySelector(Writer writer, TreeSet<String> allCategories) throws IOException
	{
		writer.write("Category : ");
		writer.write("<select name=\"" + CATEGORY_PARAMETER + "\">");

		for (String category : allCategories)
		{
			writer.write("<option value=\"" + category + "\">");
			writer.write(category);
			writer.write("</option>");
		}

		writer.write("</select>");
		writer.write("<br />\n");
	}

	private void writeHtmlBottom(Writer writer) throws IOException
	{
		writer.write("</body>\n");
		writer.write("</html>\n");
	}

	private void writeHtmlTop(Writer writer) throws IOException
	{
		writer.write("<!DOCTYPE HTML>\n");
		writer.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
		writer.write("<head>\n");
		if (getCssPath() != null) writer.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + getServletContext().getContextPath() + getCssPath() + "\" title=\"default\" />\n");
		writer.write("</head>\n");
		writer.write("<body>\n");
	}
}
