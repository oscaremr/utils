import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.io.IOUtils;
import org.oscarehr.util.MiscUtils;

/**
 * Handy class to have around just to check if an SSL connection works or not even with no certificate checking.
 * This is just a class for debugging I want kept around. This is not actually a junit test.
 */
public class GetSslContents
{
	public static void main(String... argv) throws IOException, KeyManagementException, NoSuchAlgorithmException
	{
		String urlString = "https://localhost:8091/myoscar_server/ws";

		if (argv.length > 0) urlString = argv[0];

		System.err.println("testing url " + urlString);

		MiscUtils.setJvmDefaultSSLSocketFactoryAllowAllCertificates();

		URL url = new URL(urlString);
		InputStream is = (InputStream) url.getContent();

		System.err.println("---------------");
		System.err.println("result");
		System.err.println("---------------");

		System.err.println(IOUtils.toCharArray(is));
	}
}
