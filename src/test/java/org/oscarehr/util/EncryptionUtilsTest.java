/**
 * Copyright (c) 2001-2012. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.junit.Test;
import org.oscarehr.util.EncryptionUtils.RsaKeyData;

public class EncryptionUtilsTest
{
	@Test
	public void testSha() throws UnsupportedEncodingException, NoSuchAlgorithmException
	{
		String data = "asdf";

		byte[] sha = EncryptionUtils.getSha("SHA-256", data);
		assertNotNull(sha);
		assertFalse(Arrays.equals(data.getBytes("UTF-8"), sha));
	}

	@Test
	public void testAesKeyGeneration() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException
	{
		// just checking that the key generated is valid
		SecretKeySpec key = EncryptionUtils.generateAesEncryptionKey("my pw", "SHA-256", 128);
		byte[] results = EncryptionUtils.encryptAes(key, "blue cows");
		assertNotNull(results);
	}

	@Test
	public void testAes() throws NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, UnsupportedEncodingException
	{
		String data = "The brown cow jumped over the moon.";

		SecretKey encryptionKey = EncryptionUtils.generateAesEncryptionKey(128);
		assertNotNull(encryptionKey);

		// make sure the encrypted result is not the same as the original data
		byte[] encryptedBytes = EncryptionUtils.encryptAes(encryptionKey, data);
		assertNotNull(encryptedBytes);
		assertFalse(Arrays.equals(data.getBytes("UTF-8"), encryptedBytes));

		// make sure the decrypted result is the exact same as the encrypted data
		String decryptedString = EncryptionUtils.decryptAesToString(encryptionKey, encryptedBytes);
		assertNotNull(decryptedString);
		assertEquals(data, decryptedString);

		// test no encryption
		encryptedBytes = EncryptionUtils.encryptAes(null, data);
		assertEquals(data, new String(encryptedBytes));

		decryptedString = EncryptionUtils.decryptAesToString(null, data.getBytes("UTF-8"));
		assertEquals(data, decryptedString);
	}

	@Test
	public void testRsa() throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException
	{
		String data = "The blue jelly stung the great white shark.";

		KeyPair keyPair=EncryptionUtils.generateRsaKeyPair(2048);
		
		// test encryption
		byte[] encryptedBytes=EncryptionUtils.encryptRsa(keyPair.getPublic(), data);
		assertFalse(Arrays.equals(data.getBytes("UTF-8"), encryptedBytes));
		
		// test decryption
		String decryptedString=EncryptionUtils.decryptRsaToString(keyPair.getPrivate(), encryptedBytes);
		assertEquals(data, decryptedString);
	}
	
	@Test
	public void reproducableAesKeys() throws UnsupportedEncodingException, NoSuchAlgorithmException, InterruptedException
	{
		// this test is meant to show that a AES key can be deterministically 
		// generateable off of a seed. Basically we want to prove that the key
		// generation isn't randomly salted some where out side of our control.

		String seed = "" + Math.random();

		SecretKeySpec key1 = EncryptionUtils.generateAesEncryptionKey(seed, "SHA-1", 128);
		byte[] key1Bytes = key1.getEncoded();

		Thread.sleep(1000);

		SecretKeySpec key2 = EncryptionUtils.generateAesEncryptionKey(seed, "SHA-1", 128);
		byte[] key2Bytes = key2.getEncoded();

		assertArrayEquals(key1Bytes, key2Bytes);
	}

	@Test
	public void serializableAesKeys() throws NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, UnsupportedEncodingException
	{
		// check to make sure a serialised then deserialised AES key produce the same results.

		String originalData = "candle wax seems to work fine for caking in your board through summer";

		SecretKey originalKey = EncryptionUtils.generateAesEncryptionKey(128);
		byte[] encryptedData = EncryptionUtils.encryptAes(originalKey, originalData);
		assertNotSame(originalData, new String(encryptedData, MiscUtils.DEFAULT_UTF8_ENCODING));

		byte[] keyBytes = originalKey.getEncoded();

		SecretKeySpec deserialisedKey = EncryptionUtils.deserialiseAesEncryptionKey(keyBytes);
		String decryptedData = EncryptionUtils.decryptAesToString(deserialisedKey, encryptedData);
		assertEquals(originalData, decryptedData);
	}

	@Test
	public void serializableRsaKeys() throws NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeySpecException
	{
		String originalData = "UV Resin can be used to fix anything!";

		KeyPair originalKeyPair = EncryptionUtils.generateRsaKeyPair(2048);
		RSAPublicKey originalPublicKey = (RSAPublicKey) originalKeyPair.getPublic();
		RSAPrivateKey originalPrivateKey = (RSAPrivateKey) originalKeyPair.getPrivate();

		RsaKeyData publicKeyData=EncryptionUtils.serialiseRsaPublicKeyData(originalPublicKey);
		RsaKeyData privateKeyData=EncryptionUtils.serialiseRsaPrivateKeyData(originalPrivateKey);
		
		RSAPublicKey deserialisedPublicKey = EncryptionUtils.deserialiseRsaPublicKey(publicKeyData);
		RSAPrivateKey deserialisedPrivateKey = EncryptionUtils.deserialiseRsaPrivateKey(privateKeyData);

		// check both and encrypt and decrypt the same items
		// RSA is non-deterministic encryption so the only way to test is to decrypt the data to make sure decryption works. 
		byte[] originalEncryption = EncryptionUtils.encryptRsa(originalPublicKey, originalData);
		byte[] deserialisedEncryption = EncryptionUtils.encryptRsa(deserialisedPublicKey, originalData);

		String decryptedUsingOriginalKey = EncryptionUtils.decryptRsaToString(originalPrivateKey, deserialisedEncryption);
		String decryptedUsingDeserialisedKey = EncryptionUtils.decryptRsaToString(deserialisedPrivateKey, originalEncryption);

		assertEquals(originalData, decryptedUsingOriginalKey);
		assertEquals(originalData, decryptedUsingDeserialisedKey);
		assertEquals(decryptedUsingOriginalKey, decryptedUsingDeserialisedKey);
	}
}
