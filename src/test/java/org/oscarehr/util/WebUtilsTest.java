package org.oscarehr.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class WebUtilsTest
{
	@Test
	public void testTimeZoneConversion()
	{
		//  420 -> GMT-0700
		// -420 -> GMT+0700
		//  150 -> GMT-0130
		// -665 -> GMT+1105
		
		String result=WebUtils.convertTimeZoneOffsetToTimeZoneCustomId("420");
		assertEquals("GMT-0700", result);
		
		result=WebUtils.convertTimeZoneOffsetToTimeZoneCustomId("-420");
		assertEquals("GMT+0700", result);
		
		result=WebUtils.convertTimeZoneOffsetToTimeZoneCustomId("150");
		assertEquals("GMT-0230", result);
		
		result=WebUtils.convertTimeZoneOffsetToTimeZoneCustomId("-665");
		assertEquals("GMT+1105", result);
	}
}
