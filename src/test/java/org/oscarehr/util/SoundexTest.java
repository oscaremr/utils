package org.oscarehr.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.commons.codec.EncoderException;
import org.junit.Test;

public class SoundexTest
{
	@Test
	public void testSoundex() throws EncoderException
	{
		// soundex algorithm fails, not our code
		// assertTrue(soundex("phish", "fish"));

		assertTrue(soundex("receive", "recieve"));
		assertTrue(soundex("marc", "Mark"));
		assertTrue(soundex("Ana", "anna"));
		assertTrue(soundex("bobby", "bobbie"));

		// soundex algorithm fails, not our code
		// assertTrue(soundex("which", "witch"));

		assertFalse(soundex("foo", "bar"));
		assertFalse(soundex("apple", "orange"));
		assertFalse(soundex("bob", "pat"));
	}

	private boolean soundex(String s1, String s2) throws EncoderException
	{
		System.err.println("" + MiscUtils.soundexScore(s1, s2) + ", " + s1 + ", " + s2);
		return(MiscUtils.soundex(s1, s2));
	}
}
