#!/bin/sh

VERSION=2016.05.11
PROJECT_NAME=util

INSTALL_PATH_PREFIX=~/.m2/repository/org/oscarehr/${PROJECT_NAME}/${VERSION}/${PROJECT_NAME}-${VERSION}
mvn clean install
#mvn install:install-file -Dpackaging=jar -DcreateChecksum=true -DlocalRepositoryPath=../../oscar/oscar_maven_repo/local_repo -DlocalRepositoryId=local_repo -DgroupId=org.oscarehr -DartifactId=${PROJECT_NAME} -Dversion=${VERSION} -Dfile=${INSTALL_PATH_PREFIX}.jar -DpomFile=${INSTALL_PATH_PREFIX}.pom
mvn install:install-file -Dpackaging=jar -DcreateChecksum=true -DlocalRepositoryPath=../local_repo/local_repo -DlocalRepositoryId=local_repo -DgroupId=org.oscarehr -DartifactId=${PROJECT_NAME} -Dversion=${VERSION} -Dfile=${INSTALL_PATH_PREFIX}.jar -DpomFile=${INSTALL_PATH_PREFIX}.pom

